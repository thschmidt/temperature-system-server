
# This is template to create an own service for the temerapture system
# It already includes all necessary functions and imports
# For further information, please read the "How to create an own service" in docs


###############################################################################
# IMPORTS
###############################################################################


####### IMPORT SOMETHING HERE ########


###############################################################################
# CLASS
###############################################################################

class TemplateModule:
    def __init__(self, module_basics):
        # A little helper, including some useful functions and parameters,
        # see docs
        self._mob = module_basics
        
        ####### ADD SOME INITIAL PARAMETER HERE ########
        
   
    """------------------------- INITIAL FUNCTIONS -------------------------""" 
  
    def init_device(self, setup=False):
        """Initialize device parameters"""
        if not setup:
            ####### ADD SOME CODE HERE OR PASS ########
            
            pass

       
    """----------------------- CONNECTION FUNCTIONS -----------------------"""
    
    def open_connection(self, device_connection={}):
        """
        Open Connection to device . 
        'device_connection': the connection info:
            'TYPE': 'Ethernet/USB/Serial,
            'IP/SerialNumber/Port':'XX.XX.XX.XX/XXXXXX/COMX
        """
        
        ####### ADD SOME CODE HERE OR PASS ########
        
        pass

    def close_connection(self, device_connection={}):
        """
        Close Connection to device
        'device_connection': the connection info
            'TYPE': 'Ethernet/USB/Serial,
            'IP/SerialNumber/Port':'XX.XX.XX.XX/XXXXXX/COMX
        """
        
        ####### ADD SOME CODE HERE OR PASS ########
        
        pass
 
    """--------------------- SET AND REQUEST FUNCTIONS ---------------------""" 
    
    # REQUEST, STATUS and SET
    def request_status(self, device_state, log_request=False):
        """
        Request the current status of the device (only during an active campaign)
        and update it in self.body
        """
        
        ####### ADD SOME CODE HERE OR PASS ########
        
        pass 
         
        
    def set_parameter(self, channel, parameter, value):
        """Send value of parameter of selected channel to device"""
        
        ####### ADD SOME CODE HERE OR PASS ########
        
        pass
               
 
    """------------------------- CAMPAIGN FUNCTIONS -------------------------""" 
       
    # START, STOP and INIT campaign
    def start_campaign(self):
        """Start a new campaign with init settings"""
        
        ####### ADD SOME CODE HERE OR PASS ########
        
        pass
  
    def stop_campaign(self):
        """Stop current campaign and reset devices"""
        
        ####### ADD SOME CODE HERE OR PASS ########
        
        pass
                
    def init_campaign(self, campaign_details):
        """Initialize a new campaign and setup up the devices"""
        
        ####### ADD SOME CODE HERE OR PASS ########
        
        pass
    
 

            
