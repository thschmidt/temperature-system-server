 
###############################################################################
# IMPORTS
###############################################################################  

# Some tkinter
import tkinter as tk
from tkinter import ttk

import os
import sys

# Regular expression
import re

# 
from file_and_folder import FileAndFolder as faf
from install_config_subclasses import Defaults, ContentWidget
   

###############################################################################
# MESSENGER CLASS
###############################################################################  

class MessengerConfigPage(tk.Frame, Defaults, ContentWidget):
    
    # CURRENTLY ONLY DESIGNED FOR EMAIL MESSENGER
    def __init__(self, parent, controller, home, queues):
        tk.Frame.__init__(self, parent)
        Defaults.__init__(self, parent, controller, home)
        

        ## Header Label ##
        header = tk.Label(self, text="Messenger Configuration",
                          font=("Courier", 20))
        header.pack(pady=10, padx=10)
              
        # Header Frame
        self.top_frame = tk.Frame(self)
        self.top_frame.pack(fill="x", padx=10, pady=5)
        
        top_label = tk.Label(self.top_frame, text='Messenger Config File:')
        top_label.pack(pady=5, side='left')
        
        self.cfgs = {}
        options = list(self.cfgs.keys())
        self.selected_option = tk.StringVar()
        self.selected_option.trace('w', self.update_content)

        self.dropdown = ttk.OptionMenu(self.top_frame,
                                       self.selected_option,
                                       self.selected_option.get(),
                                       *options)
        self.dropdown.pack(pady=5, side='left')
        
        ## Content Frame ##
        self.content_frame = tk.Frame(self)
        self.content_frame.pack(fill="x", padx=10, pady=5)

        self.save_btn = tk.Button(self.buttons_frame, text='Save',
                                  command=self.save_messenger_config)
        self.save_btn.pack(side="left", padx=10)
        
        self.test_message_btn = tk.Button(self.buttons_frame,
                                          text='Send Test Message',
                                          command=self.send_test_message)
        self.test_message_btn.pack(side="left", padx=10)
        
        self.back_btn.pack(side="right")
        
  
    def reset_page(self):
        """Reset several parameters of the current page"""
        sys.path.insert(1, os.path.join(self.main_directory, 'messengers'))
        import init_messengers as inm
        self.courier_class = inm.Courier()
        
        self.controller.geometry("500x400")
        # Implement any specific reset logic
        self.reload_defaults()
        
        self.input_vars = {}
        
        self.clear_frame(self.content_frame)
        
        # Config dictionary of all messengers
        self.cfgs = self.get_messenger_configs()
        
        menu = self.dropdown['menu']
        menu.delete(0, 'end')
        
        # Add the new options to the menu
        for option in list(self.cfgs.keys()):
            menu.add_command(label=option,
                             command=tk._setit(self.selected_option, option))
        
        if len(list(self.cfgs.keys())) > 0:
            default_option = list(self.cfgs.keys())[0]
            self.selected_option.set(default_option)
        

    def get_messenger_configs(self):
        """
        Get the default and current configuration of the messenger
        CURRENTLY ONLY EMAIL!!!
        """
        # Get module path and the containing filenames
        messenger_config_path = os.path.join(self.configs_directory,
                                             'messengers_yaml')
        config_filenames = faf.get_filenames(messenger_config_path,
                                             ending='yaml', fullpath=False)
        
        ### ADD HERE THE READOUT OF THE OTHER MESSENGER SERVICES
        # # Check which messengers are currently used by the server
        # setup_config = faf.read_yaml(os.path.join(self.modules_directory,
        #                                           'modules_setup.yaml'))

        for messenger_filename in config_filenames:
            if os.path.basename(messenger_filename) == 'email.yaml':
                email_config = faf.read_yaml(os.path.join(messenger_config_path,
                                                          messenger_filename))
                messenger_config = {'email':email_config}

                return messenger_config
  
    def update_content(self, a=0 , b=0, c=0):
        """Update the content dictionary and the frame"""
        self.clear_frame(self.content_frame)
        
        # Update the config content of the current selected messenger
        if a != 0:
            self.config_content = self.cfgs[self.selected_option.get()]
        
        # Arange the config content in the content frame
        self.create_widgets_for_dict(self.content_frame,
                                     self.config_content)


    def save_messenger_config(self):
        """
        Save the messenger config data and check the email setup parameters if active
        """
        
        messenger_filename = os.path.join(self.configs_directory, 
                                          'messengers_yaml',
                                          self.selected_option.get() + '.yaml')
        
        try:
            self.set_messenger_parameter()
        except Exception as err:
            self.error_box(err)
        else:
            self.update_dict_from_inputs(self.config_content)
    
            if 'GENERAL' not in self.config_content:
                comment = "Sending message in case of error"
                self.config_content['GENERAL'] = {'active':{'VALUE':False,
                                                            'TYPE':'Boolean',
                                                            'COMMENT':comment}}
    
            faf.write_yaml(messenger_filename, self.config_content)
            self.reset_page()
        
    def send_test_message(self):
        """Send a test messenger to the first receiver in the list"""
        try:
            test_receiver = self.set_messenger_parameter()
            
            if self.selected_option.get() == 'email':
                self.check_if_email(test_receiver)
            
        except Exception as err:
            self.error_box(err)
        else:
            subject = "Test Message"
            message = "This is a Test Message from the Temperature System"
            self.courier_class.send_message([test_receiver], message, subject)
        
       
        
    def set_messenger_config(self):
        """Fetch the messenger parameters and return the different parameters"""
        parameters = {}
        # 
        for key, value in self.config_content.items():
            parameters[key] = {}
            for k, v in value.items():
                if k == 'receiver_list':
                    skey = key + '.' + k + '[0]'
                    test_receiver = self.input_vars[skey].get()
                else:
                    skey = key + '.' + k
                    parameters[key][k] = self.input_vars[skey].get()
        return parameters, test_receiver

    def set_messenger_parameter(self):
        """Get the input parameters and update the messenger setup"""
        parameters, test_receiver = self.set_messenger_config()
        self.courier_class.setup_messenger('email', parameters)
        return test_receiver
    

    def check_if_email(self, receiver):
        # Simple Regex for syntax checking
        regex = '^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$'
        
        match = re.match(regex, receiver)
        if match == None:
            raise Exception(receiver
                            + ": Please add an existing email address")
