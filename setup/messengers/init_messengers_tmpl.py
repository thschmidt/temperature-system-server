
###############################################################################
# IMPORTS
###############################################################################  

# System and os
import sys
import os

# Connect these directories
current_dir = os.path.dirname(__file__)

###MESSENGER###

###############################################################################
# CLASS
############################################################################### 

class Courier:
    def __init__(self):
        self.couriers = {}
    
    def setup_messenger(self, name, parameters):
        if name == 'email':
            import email_messenger
            self.couriers[name] = email_messenger.EmailBox()
            self.couriers[name].setup_messenger(parameters)
        ###SETUP###
        
    def send_message(self, receiver_list, message_text, message_subject):
        for name in self.couriers:
            self.couriers[name].send_message(receiver_list,
                                             message_text,
                                             message_subject)

