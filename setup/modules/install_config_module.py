 
###############################################################################
# IMPORTS
###############################################################################  

# Some tkinter
import tkinter as tk
from tkinter import scrolledtext
from tkinter import ttk
from tkinter import filedialog


# Multithreading
import threading

import os

#
import time

# 
from file_and_folder import FileAndFolder as faf
from install_config_subclasses import (Defaults, InputPage,
                                       ContentWidget, ConnectionWindow)


###############################################################################
# MODULE CLASSES
###############################################################################  
        
class ModuleInstallPage(tk.Frame, Defaults):
    def __init__(self, parent, controller, home, queues):
        tk.Frame.__init__(self, parent)
        # Initialize some default parameter
        Defaults.__init__(self, parent, controller, home)
        
        ## Header Label ##
        header = tk.Label(self, text="Module Installation",
                          font=("Courier", 20))
        header.pack(pady=10, padx=10)

        # Create a frame for the two columns
        columns_frame = tk.Frame(self)
        columns_frame.pack(fill="both", expand=True)

        # Data Logger Column
        self.data_logger_frame = tk.Frame(columns_frame)
        self.data_logger_frame.pack(side="left", fill="both",
                               expand=True, padx=10)
        
        # Temp Controller Column
        self.temp_controller_frame = tk.Frame(columns_frame)
        self.temp_controller_frame.pack(side="right", fill="both",
                                   expand=True, padx=10)
        

        # Logging textbox #
        self.log_text = scrolledtext.ScrolledText(self, height=10,
                                                  wrap=tk.WORD)
        self.log_text.pack(fill="both", padx=10, pady=10)
        
        # Progress bar #
        self.progress_bar.pack(fill="x", padx=10, pady=10)

        ### Button frame ###
        
        # Install the selected modules
        self.install_btn = tk.Button(self.buttons_frame,
                                     text="Install",
                                     command=self.install_thread)
        self.install_btn.pack(side="left")
        
        # Button to get to the module config page
        module_config_btn = tk.Button(self.buttons_frame,
                                      text="Go to Module Config",
                                      command=lambda: controller.show_frame(ModuleConfigPage))
        module_config_btn.pack(side='left', padx=10)
        
        self.folder_btn = tk.Button(self.buttons_frame, 
                                    text="Import Module",
                                    command=self.import_module)
        self.folder_btn.pack(side='left', padx=10)
        
        # Return back to the Main Menu
        self.back_btn.pack(side="right", padx=10)

    """---------------------- INITIAL PAGE FUNCTIONS ----------------------"""

    def reset_page(self):
        """Reset several parameters of the current page"""
        # Reset the log textbox
        self.reset_log_text()
        # Reload some default paths and filename parameters
        self.reload_defaults()
        
        # Reset Data Logger and Temperature Controller Frame
        for frame in [self.data_logger_frame, self.temp_controller_frame]:
            for widget in frame.winfo_children():
                widget.destroy()
                
        # Create header for Data Logger and Temperature Controller Frame
        data_logger_label = tk.Label(self.data_logger_frame,
                                     text="Data Logger",
                                     font=("Helvetica", 14))
        data_logger_label.pack()
        
        temp_controller_label = tk.Label(self.temp_controller_frame,
                                         text="Temperature Controller",
                                         font=("Helvetica", 14))
        temp_controller_label.pack()
        

        # Dictionaries for Data Logger and Temp Controller
        module_dict = self.get_modules()
        
        # Add Checkboxes to controller and logger
        boxes = []
        self.group_var = {}
        for group in ['data_logger', 'temp_control']:
            self.group_var[group] = {}
            num_boxes = 0
            for key in module_dict[group]:
                var = tk.BooleanVar(value=module_dict[group][key])
                self.group_var[group][key] = var
                if group == 'data_logger':
                    frame = self.data_logger_frame
                else:
                    frame = self.temp_controller_frame
                checkbox = tk.Checkbutton(frame,
                                          text=key, variable=var)
                checkbox.pack(anchor="w")
                num_boxes += 1
            boxes.append(num_boxes)
            
        height = str(350 + max(boxes) * 24)
            
        self.controller.geometry("500x" + height)
        
     
    def get_modules(self):
        """
        Get the available modules and check if already installed/used
        by the current server setup
        """
        modules = {}
        # Get the current setup yaml
        setup_config = faf.read_yaml(os.path.join(self.modules_directory,
                                                  'modules_setup.yaml'))
        for group in ['data_logger', 'temp_control']:
            group_path = os.path.join(self.modules_directory, group)
            # Get the available modules in the subdictionaries
            module_list = faf.get_subdirectories(group_path, 
                                                 fullpath=False)
            series_dict = {}
            for module_path in module_list:
                # Check if modules/series already used by the server
                if setup_config[group] is not None:
                    for series in setup_config[group]:
                        if module_path == setup_config[group][series]['path']:
                            series_dict[module_path] = True
                            break
                        else:
                            series_dict[module_path] = False
                else:
                    series_dict[module_path] = False
                   
            modules[group] = series_dict

        return modules
       
    """----------------------- COPY FOLDER FUNCTIONS -----------------------"""
    
    def import_module(self):
        """Import modules in the module folder"""
        err_msg = "This module cannot be imported."
        try:
            module_folder = (filedialog.askdirectory())
            group = self.check_module_specs(module_folder)
            module_name = os.path.basename(module_folder)
            destination_dir = os.path.join(self.modules_directory, group,
                                           module_name)
            if faf.check_path(destination_dir):
                msg = "Do you want to overwrite existing module?"
                if not self.verify_before_execute(msg, cmd="Yes"):
                    return  
            faf.copy_folder(module_folder, destination_dir)
        except Exception as err:
            self.error_box(err_msg + '\n' + str(err))
        else:
            self.reset_page()
        
    def check_module_specs(self, module_folder):
        """Check if modules contain specific files / configs"""
        # Check existence of config files
        config_folder = os.path.join(module_folder, 'config')
        if faf.check_path(config_folder):
            for cfg_file in ['module', 'setup']:
                if not faf.check_path(os.path.join(config_folder, cfg_file+'.yaml')):
                    raise Exception("Missing config file '"
                                    + cfg_file + ".yaml'")
        else:
            raise Exception("Missing config file folder")
            return False
        
        # Check for requirements file
        if not faf.check_path(os.path.join(module_folder, 'requirements.txt')):
            raise Exception("Missing requirements.txt")
        
        
        module_yaml = faf.read_yaml(os.path.join(config_folder,'module.yaml'))
        group = self.check_module_group(module_yaml)
        
        return group
    
    def check_module_group(self, module_yaml):
        """Check in which group folder the module needs to be put"""
        category = ['Controller','Thermostat','Power_Supply']
        if 'GENERAL' in module_yaml:
            if 'Category' in module_yaml['GENERAL']:
                if module_yaml['GENERAL']['Category'] in category:
                    return 'temp_control'
                else:
                    raise Exception("Wrong category in module.yaml")
            else:
                return 'data_logger'
        else:
            raise Exception("Missing GENERAL key in module.yaml")
        
         
    """---------------------- INSTALLATION FUNCTIONS ----------------------"""            
       
    def install_thread(self):
        """
        To update the log text start a seperate thread
        """
        self.reset_log_text()
        
        # Disable the button to prevent multiple loggers
        self.install_btn.config(state=tk.DISABLED)

        # Start a thread to simulate real-time logging
        threading.Thread(target=self.install_modules, daemon=True).start()
    
  
    def install_modules(self):
        """
        Full procedure of the module installation of the temperature server system
        """
        self.process_amount = 6
        
        try:
            # Check Existing Files
            self.show_log('Check existing Files', header=2)
            self.check_for_config_files()
            self.show_log('Done', msg_type='success')
            
            # Setup changeable modules
            self.setup_yaml_data = {}
            
            no_modules = True
            # Install needed python packages
            self.show_log("Install Module Packages", header=2)
            for group in ['data_logger', 'temp_control']:
                group_path = os.path.join(self.modules_directory, group)
                self.setup_yaml_data[group] = {}
                for slct_mod in self.group_var[group]:
                    # Check which module has been selected
                    if self.group_var[group][slct_mod].get():
                        self.init_module_config(slct_mod,
                                                group_path,
                                                group)
                        try:
                            req_module_file = os.path.join(group_path, 
                                                           slct_mod, 
                                                           'requirements.txt')
                            self.install_python_packages(req_module_file)
                        except Exception:
                            # Just in case
                            self.show_log("A problem occured "
                                          + "installing the package. "
                                          + "Please check manually before "
                                          + "using the software",
                                          msg_type='warning')
                            time.sleep(4)
                        
                        no_modules = False
            self.show_log('Done', msg_type='success')
                
            # Overwrite the old setup yaml file
            self.show_log('Overwrite Setup Yaml', header=2)
            setup_yaml_path = os.path.join(self.modules_directory,
                                           'modules_setup.yaml') 
            faf.write_yaml(setup_yaml_path, self.setup_yaml_data)
            self.show_log('Done', msg_type='success')
            
            # Overwrite the initial module python code
            self.show_log('Overwrite Module Init', header=2)
            if no_modules: # If no modules have been added
                self.process_amount = 5
                faf.overcopy_file(os.path.join(self.setup_directory,'modules'),
                                  self.modules_directory,
                                  'init_modules_tmpl.py',
                                  'init_modules.py')
                self.show_log('Done', msg_type='success')
            else: # If modules have been added
                module_init_file = os.path.join(self.modules_directory,
                                                'init_modules.py')
                # Get default / template code
                module_init_template_file = os.path.join(self.setup_directory,
                                                         'modules',
                                                         'init_modules_tmpl.py')
                self.create_module_init_file(module_init_template_file,
                                             module_init_file,
                                             self.setup_yaml_data)
                self.show_log('Done', msg_type='success')
                
                # Copy all the module config files into the module_yaml folder
                self.show_log('Copy Config Files', header=2)
                modules_yaml_path = os.path.join(self.configs_directory,
                                                 'modules_yaml') 
                module_config_files = faf.get_filenames(modules_yaml_path, 
                                                        ending='yaml', 
                                                        fullpath=False)
                
                self.copy_config_file(module_config_files,
                                      modules_yaml_path)
                self.show_log('Done', msg_type='success')
                self.show_log('+++ Installation successfully +++', 'success')
                
        except Warning as wrn:
            self.process_amount = 0
            self.show_log(str(wrn), msg_type='warning')
        
        except Exception as err:
            self.process_amount = 0
            self.show_log(str(err), msg_type='error')
            
        self.install_btn.config(state='normal')


    """--------------------------- SUB FUNCTIONS ---------------------------""" 


    def init_module_config(self, slct_mod, group_path, group):
        """
        Fetch the path, class and script information of the module the setup.yaml
        """
        module_setup_path = os.path.join(group_path,
                                         (slct_mod
                                          + '/config/setup.yaml'))
        if faf.check_path(module_setup_path):
            slct_mod_setup = faf.read_yaml(module_setup_path)
            try:
                slct_module_key = slct_mod_setup['key']
                di = {'path': slct_mod,
                      'script': slct_mod_setup['script'],
                      'class': slct_mod_setup['class']}
                self.setup_yaml_data[group][slct_module_key] = di
            except KeyError:
                raise Exception("Missing key in setup of " + slct_mod)

        else:
            raise Exception(module_setup_path + " does not exist")
            

    def copy_config_file(self, module_config_files,
                         modules_yaml_path):
        """
        Copy the config file from each selected module into the config path
        and rename it into the abbrev of the module
        """
        for group in self.setup_yaml_data:
            if self.setup_yaml_data[group] is not {}:
                for slct_mod in self.setup_yaml_data[group]:
                    path = self.setup_yaml_data[group][slct_mod]['path']
                    module_path = os.path.join(self.modules_directory,
                                               os.path.join(group,
                                               path))
                    if (slct_mod + '.yaml') not in module_config_files:
                        module_config = faf.read_yaml(os.path.join(module_path, 
                                                                   'config',
                                                                   'module.yaml'))
                        del module_config['ID']['DEVICE-ID']
                        faf.write_yaml(os.path.join(modules_yaml_path,
                                                    slct_mod + '.yaml'),
                                                    module_config)
                    
    def create_module_init_file(self, template_file, module_init_path, data):
        """
        Create the python code from the default structure for the init_modules.py
        in which the the different modules and their classes are imported.
        """
        with open(template_file, 'r') as f:
            line_data = f.read().split('\n')
        
        # Add the module paths in sys
        new_lines = []
        for group in data:
            if data[group] is not None:
                for module in data[group]:
                    module_path = os.path.basename(data[group][module]['path'])
                    new_lines.append("sys.path.insert(1, "
                                     + group
                                     + "_path + '/"
                                     + module_path
                                     + "')")
        
        # Create the FUNCTIONS describtion
        new_lines.append("")
        new_lines.append("#"*79)
        new_lines.append("# FUNCTIONS")
        new_lines.append("#"*79)
        new_lines.append("")
        
        # Start creating the start_module function with its inputs
        first_func = []
        sec_func = [""]
        first_func.append("def start_module(services, queues, " 
                         + "settings, dummymode):")   
        sec_func.append("def start_device(service, connection, mob):")
        
        # Create the different if and elif conditions for the different modules
        first_line = True                                                     
        for group in data:
            if data[group] is not None:
                for module in data[group]:  
                    script_name = data[group][module]['script']
                    class_name = data[group][module]['class']
                    if first_line:
                        first_func.append("\tif services[0] == '" + module + "':")
                        sec_func.append("\tif service == '" + module + "':")
                        first_line = False
                    else:
                        first_func.append("\telif services[0] == '" + module + "':")
                        sec_func.append("\telif service == '" + module + "':")
                    first_func.append("\t\tfrom " + script_name
                                     + " import " + class_name 
                                     + " as module_class")
                    first_func.append("\t\t"
                                     + "module_process.service_process"
                                     + "(module_class, services, "
                                     + "queues, settings, dummymode)")
                    sec_func.append("\t\tfrom " + script_name
                                     + " import " + class_name 
                                     + " as module_class")
                    sec_func.append("\t\tmod = module_class(mob)")
        sec_func.append("\tmod.open_connection(connection)")
        sec_func.append("\treturn mod.init_device(True)")

        # Find the Position where the written code will be inserted ###MODULE###  
        i = 0
        for line in line_data:
            if "###MODULE###" in line:
                break
            i += 1
            
        # Combine the code in the default file and the here created one
        line_string = '\n'.join(line_data[:i] + new_lines + first_func + sec_func)
        
        # Write the full code in the python file
        with open(module_init_path, 'w') as f:
            f.write(line_string) 


class ModuleConfigPage(tk.Frame, Defaults, ContentWidget):
    def __init__(self, parent, controller, home, queues):
        tk.Frame.__init__(self, parent)
        Defaults.__init__(self, parent, controller, home)
        
        self.queues = queues
        
        ## Header Label ##
        header = tk.Label(self, text="Module Configuration",
                          font=("Courier", 20))
        header.pack(pady=10, padx=10)
        
        # Header Frame
        self.top_frame = tk.Frame(self)
        self.top_frame.pack(fill="x", padx=10, pady=5)
        
        top_label = tk.Label(self.top_frame, text='Module Config File:')
        top_label.pack(pady=5, side='left')
        
        self.cfgs = {}
        
        options = list(self.cfgs.keys())
        self.selected_option = tk.StringVar()
        self.selected_option.trace('w', self.update_content)

        self.dropdown = ttk.OptionMenu(self.top_frame,
                                       self.selected_option,
                                       self.selected_option.get(),
                                       *options)
        self.dropdown.pack(pady=5, side='left')
        
        # Dictionary to store the StringVar references
        self.content_frame = tk.Frame(self)
        self.content_frame.pack(fill="x", padx=10, pady=5)


        # Buttons
        self.save_btn = tk.Button(self.buttons_frame,
                                  text="Save Module Config",
                                  command=self.save_config)
        self.save_btn.pack(side="left")
        
        self.back_btn.pack(side="right")
        
    
    """----------------------- INITIAL PAGE FUNCTIONS ----------------------"""  
         
    def reset_page(self):
        """Reset several parameters of the current page"""
        self.controller.geometry("450x700")
        
        # Get available comports
        import serial.tools.list_ports as stl
        comlist = stl.comports()

        self.comports = ['']
        for element in comlist:
            self.comports.append(element.device)
        
        # Implement any specific reset logic
        self.reload_defaults()
        
        self.config_content = {}
        # Dictionary to store the StringVar references
        self.input_vars = {}
        
        self.clear_frame(self.content_frame)
        
        # Config dictionary of all modules used by the server
        self.cfgs = self.get_module_configs()

        menu = self.dropdown['menu']
        menu.delete(0, 'end')
        
        # Add the new options to the menu
        for option in list(self.cfgs.keys()):
            menu.add_command(label=option,
                             command=tk._setit(self.selected_option, option))
        
        if len(list(self.cfgs.keys())) > 0:
            default_option = list(self.cfgs.keys())[0]
            self.selected_option.set(default_option)
        
    def get_module_configs(self):
        """
        Get the default and current configuration of the module used by the server
        """
        # Get module path and the containing filenames
        module_config_path = os.path.join(self.configs_directory,
                                          'modules_yaml')
        config_filenames = faf.get_filenames(module_config_path,
                                             ending='yaml', fullpath=False)
    
        module_config = {}
        # Check which modules/series are currently used by the server
        setup_config = faf.read_yaml(os.path.join(self.modules_directory,
                                                  'modules_setup.yaml'))

        for group in ['data_logger', 'temp_control']:
            group_path = os.path.join(self.modules_directory, group)
            # Check if modules/series used by the server
            if setup_config[group] is not None:
                for series in setup_config[group]:
                    # Get the default configuration of the module
                    module_dir = setup_config[group][series]['path']
                    module_yaml = os.path.join(group_path,
                                               module_dir,
                                               'config',
                                               'module.yaml')
                    mod_def_config = faf.read_yaml(module_yaml)

                    if (series + '.yaml') in config_filenames:
                        # Get the config of the current setup
                        filename = os.path.join(self.configs_directory,
                                                'modules_yaml',
                                                series + '.yaml')
                        mod_config = faf.read_yaml(filename)  
                        
                        module_config[series] = {'Setup': mod_config,
                                                 'Default': mod_def_config}
        return module_config
                    
      
    """---------------------- RESET MODULE FUNCTIONS ----------------------""" 

    def update_content(self, a=0 , b=0, c=0):
        """Update the content dictionary and the frame"""
        self.clear_frame(self.content_frame)
        
        # Reset the buttons
        self.del_btn = {}
        self.con_btn = {}
        
        self.con = {}
        
        # Dictionary of the input variables and channels
        self.input_vars = {}
        self.list_labels = {}
        
        # Update the config content of the current selected module
        if a != 0:
            self.config_content = self.cfgs[self.selected_option.get()]

        # General device content
        self.gen_vars = {}
        if 'Connection' in self.config_content['Default']['GENERAL']:
            self.conn_gen_def = self.config_content['Default']['GENERAL']['Connection']
            series_general = self.config_content['Setup']['GENERAL']['Connection']
            # Create general details frame
            self.create_general_device_frame(self.content_frame, series_general)

        # Get the default device config
        device_default = self.config_content['Default']['ID']['DEVICE-ID']
        self.conn_def = device_default['Connection']

        series = {}
        module_content = self.config_content['Setup']['ID']
        
        
        # Create series dictionary (CONNECTION)
        for dev in module_content.keys():
            device_parameter = module_content[dev]
            
            series[dev] = {'Connection':device_parameter['Connection']}

        
        
        self.create_device_frame(self.content_frame, series)
      
    def create_general_device_frame(self, container, series):
        """"""
        gen_container = ttk.LabelFrame(self.content_frame, text='GENERAL')
        gen_container.grid(row=0, column=0, sticky='ew')
        
        row_i = 0
        if series is not None:
            for key in series:
                if isinstance(series[key], list):
                    value = series[key][0]
                else:
                    value = series[key]   
            
                # Some default connection parameter
                label = ttk.Label(gen_container, text=key)
                label.grid(row=row_i, column=0, padx=5, pady=2, sticky='w')
                
                options = self.conn_gen_def[key]
                
                type_var = tk.StringVar()
                type_var.set(str(value))
                
                type_optionmenu = tk.OptionMenu(gen_container,
                                                type_var,
                                                *options)
                type_optionmenu.grid(row=row_i, column=1, padx=5, pady=2,
                                     sticky='w')
                self.gen_vars[key] = type_var
                row_i += 1
            
    def create_device_frame(self, container, series):
        """
        Create the sub frame for each device of a module/series
        """
        self.row_index = 1
        
        for device in series:
            data = series[device]
            # Create the subframe
            self.add_device(container, self.row_index, device, data)
            self.row_index += 1
            
        # Add more devices
        self.add_button = tk.Button(container,
                                    text="Add new Device",
                                    command=lambda: self.add_device(container,
                                                                    self.row_index))
        self.add_button.grid(row=1000, column=0)   

      
    def add_device(self, container, row_index, device='', data=''):
        """
        Add a new device frame in the config frame with several input variables
        """

        verify = True
        state = 'normal'
        buttons = ''
        
        if device == '':
            verify = False 
            buttons = (list(self.del_btn.values())
                       + list(self.con_btn.values())
                       + [self.add_button, self.save_btn, self.back_btn])
            self.en_disable_buttons(buttons)
            inp_page = InputPage(self.controller, verify)
            self.en_disable_buttons([self.add_button, self.save_btn,
                                     self.back_btn], True)
            
            device, verify = inp_page.get_variables()
            
            if verify:
                # Only update if defining of device name has not been canceled
                data = {'Connection':{'Type':None}}
                self.config_content['Setup']['ID'][device] = data
                state = 'disabled'
                
        if device != '' and verify:
            # Only if a new device has been added and verified update the frame
            device_frame = ttk.Frame(container)
            device_frame.grid(row=row_index, column=0, sticky='ew')
            
            # Label
            device_subframe = ttk.LabelFrame(device_frame, text=device)
            device_subframe.grid(row=row_index, column=0, sticky='ew')
            
            # Create widget of frame (CONNECTION)
            self.create_widgets_for_dict(device_subframe, data, row_index,
                                         parent_key=device + ".")
             
            # Delete button
            button_frame = ttk.Frame(device_frame)
            button_frame.grid(row=row_index, column=1)
            
            self.con_btn[row_index] = tk.Button(button_frame,
                                                text="Connection\nCheck",
                                                command=lambda: self.con_device(device,
                                                                                row_index),
                                                state=state)
            self.con_btn[row_index].pack(fill="x", pady=5)
            
            self.del_btn[row_index] = tk.Button(button_frame,
                                              text="Delete",
                                              command=lambda: self.del_device(device_frame, 
                                                                              device,
                                                                              row_index))
            self.del_btn[row_index].pack(fill="x", pady=5)
            
            self.row_index += 1
        else:
            if buttons != '':
                self.en_disable_buttons(buttons, True)
        

    def del_device(self, device_frame, device_id, row_index=0):
        """
        Delete the config of the selected device and update the config frame
        """
        if device_id in self.config_content['Setup']['ID']:
            del self.config_content['Setup']['ID'][device_id]
            
        self.update_config_content()

        self.update_content()

    def con_device(self, device_id, row_ix):
        """
        Send a connection request to the device and get inital device parameter
        """
        if device_id in self.config_content['Setup']['ID']:
            buttons = (list(self.del_btn.values())
                        + list(self.con_btn.values())
                        + [self.add_button, self.save_btn, self.back_btn])
            self.en_disable_buttons(buttons)
            
            device = self.config_content['Setup']['ID'][device_id]

            service = self.selected_option.get() # service == series
            connection = device['Connection']

            for p in list(self.con[row_ix].keys()):
                connection[p] = self.con[row_ix][p].get()
            general_body = self.config_content['Setup']['GENERAL']

            # Send queue
            self.queues[0].put({'service':service,
                                'connection':connection,
                                'general':general_body})
            
            con_w = ConnectionWindow(self, device_id)
            
            threading.Thread(target=con_w.wait_connection).start()


    def create_widgets_for_dict(self, container, data, row_ix, parent_key=""):
        """
        Create for each key value pair of a dictionary,
        a Label and Input/Selection/... or subframe
        """
        row_index = 0  # Initialize row index for grid placement
        for key, value in data.items():
            if 'Connection' in parent_key:
                # Create the connection frame of the device
                typ_text = 'Type'
                type_val = data['Type']
                
                type_label = ttk.Label(container, text=typ_text, width=20)
                type_label.grid(row=row_index, column=0, sticky='w', padx=5)
    
                options = list(self.conn_def[typ_text].keys())
                
                type_var = tk.StringVar()
                type_var.set(str(type_val))
                
                type_optionmenu = tk.OptionMenu(container,
                                                type_var,
                                                *options)
                type_optionmenu.grid(row=row_index, column=1, padx=5, pady=2,
                                     sticky='w')
                
                parameter_frame = ttk.Frame(container)
                parameter_frame.grid(row=row_index+1, column=0,
                                     columnspan=2,
                                     padx=5, pady=2)
                
                type_var.trace('w',
                               lambda *args: self.change_connection_parameter(parameter_frame,
                                                                              parent_key,
                                                                              data,
                                                                              type_var,
                                                                              row_ix))
                self.input_vars[parent_key + typ_text] = type_var
                for key, value in data.items():
                    if key != 'Type':
                        self.change_connection_parameter(parameter_frame,
                                                         parent_key,
                                                         data,
                                                         type_var,
                                                         row_ix)
                break
            
            elif isinstance(value, dict):
                # Create a sub labelframe
                sub_frame = ttk.LabelFrame(container, text=key)
                sub_frame.grid(row=row_index, column=0, columnspan=3,
                               sticky='ew')
                self.create_widgets_for_dict(sub_frame, value, row_ix,
                                             parent_key + key + ".")
                
            elif isinstance(value, list):
                pkk = parent_key + key
                
                key_label = ttk.Label(container, text=key)
                key_label.grid(row=row_index, column=0, sticky='w', padx=5)
    
                list_frame = ttk.Frame(container)
                list_frame.grid(row=row_index, column=1, padx=5, pady=2)
                
                self.create_list_widgets(list_frame, pkk, value)
                        
            else:
                if value == None:
                    value = ""
                key_label = ttk.Label(container, text=key)
                key_label.grid(row=row_index, column=0, sticky='w', padx=5)
    
                value_var = tk.StringVar(value=str(value))
                value_entry = ttk.Entry(container, textvariable=value_var)
                value_entry.grid(row=row_index, column=1, padx=5, pady=2)
    
                self.input_vars[parent_key + key] = value_var

            row_index += 1

    """-------------------------- TABLE FUNCTIONS --------------------------""" 
        
    def create_list_widgets(self, container, pkk, data):
        """
        Create in a subwidget a list where an element can be added or deleted
        """
        # Container frame for the list of labels
        label_frame = tk.Frame(container)
        label_frame.pack(pady=10)
        
        ch_name = data[0][:-1]
        
        # List of labels
        self.list_labels[pkk] = []
        
        button_frame = ttk.Frame(container)
        button_frame.pack(pady=10)
        
        # Add button
        self.add_button = tk.Button(button_frame,
                                    text="Add",
                                    command=lambda: self.add_item(label_frame,
                                                                  pkk,
                                                                  ch_name))
        self.add_button.grid(row=0, column=0)

        # Delete button
        self.delete_button = tk.Button(button_frame,
                                       text="Delete",
                                       command=lambda:  self.delete_item(pkk))
        self.delete_button.grid(row=0, column=1)
        
        for ch in data:
            self.add_item(label_frame, pkk, ch[:-1])

        return label_frame

    def add_item(self, label_frame, pkk, ch_name):
        """Add a new item as a label to the list."""
        new_item = ch_name + f"{len(self.list_labels[pkk]) + 1}"
        label = tk.Label(label_frame, text=new_item)
        label.pack()
        self.list_labels[pkk].append(label)

    def delete_item(self, pkk):
        """Delete the last label from the list."""
        if self.list_labels[pkk]:
            label = self.list_labels[pkk].pop()
            label.destroy()
            
    def change_connection_parameter(self, container, parent_key,
                                    data, type_var, row_ix):
        """
        Change the content of the connection parameter widget
        """
        self.clear_frame(container)
        
        symbol = "\u274c"
        symbol_bg = 'red'
        if 'valid' in data:
            if type_var.get() in data['valid']:
                symbol = "\u2714"
                symbol_bg = 'green'
        
        parameter = self.conn_def['Type'][type_var.get()]
        
        if parameter in data:
            value = data[parameter]
        else:
            value = ''
        
        value_var = tk.StringVar(value=str(value))
        
        key_label = ttk.Label(container,
                              text=parameter, width=18)
        key_label.grid(row=0, column=0, sticky='w', padx=5)
        
        if type_var.get() == 'Serial':
            value_var = tk.StringVar()
            if value != None:
                value_var.set(str(value))
            else:
                value_var.set('')
            
            value_optionmenu = tk.OptionMenu(container,
                                             value_var,
                                             *self.comports)
            value_optionmenu.config(width=15)
            value_optionmenu.grid(row=0, column=1, padx=5, pady=2, sticky='w')
                
        else:
            value_entry = ttk.Entry(container, textvariable=value_var, width=20)
            value_entry.grid(row=0, column=1, padx=5, pady=2, sticky='w')
        
        valid_label = tk.Label(container, width=2, text=symbol,
                               bg=symbol_bg, fg='white')
        valid_label.grid(row=0, column=2, padx=5, pady=2, sticky='w')
        
        value_var.trace("w", lambda *args: self.on_value_change(valid_label,
                                                                *args))
        
        self.con[row_ix] = {}
        self.con[row_ix]['Type'] = type_var
        self.con[row_ix][parameter] = value_var
        self.input_vars[parent_key + parameter] = value_var
        
      
    def on_value_change(self, valid_label, *args):
        valid_label.config(bg='red', text="\u274c")
      
    def update_config_content(self, add_data=[]):
        """
        Check and update the content of the config variables
        """
        data = self.config_content['Setup']['ID']
        def_device = self.config_content['Default']['ID']['DEVICE-ID']
        
        for key in self.gen_vars:
            # Update general connection parameters
            val = self.gen_vars[key].get()
            self.config_content['Setup']['GENERAL']['Connection'][key] = val
        
        for device_id in data:
            device = data[device_id]
            
            # Add/Update connection parameters
            conn_type = device_id + '.Connection.Type'
            dev_con = {'Type':self.input_vars[conn_type].get()}
            if dev_con['Type'] == 'None':
                raise Exception("For '" + device_id
                                + "': No Connection Type is given")

            for key, val in self.input_vars.items():
                if device_id + '.Connection' in key:
                    if 'Type' not in key:
                        dev_con[key.split('.')[-1]] = val.get()
                        
            if dev_con['Type'] == 'Ethernet':
                if not self.validate_ip(dev_con['IP']):
                    raise Exception("For '" + device_id
                                    + "' the IP adress is not valid")
            
            # Reset or get validation of connection
            if 'valid' not in device['Connection']:
                dev_con['valid'] = []
            else:
                dev_con['valid'] = device['Connection']['valid']
            device['Connection'] = dev_con

            # Add/Update the channel parameter if exists
            if 'Channel' in def_device and 'Channel' not in device:
                device['Channel'] = {}
                for ch_name in def_device['Channel']:
                    if ch_name in def_device['Channel']:
                        if def_device['Channel'][ch_name] != None:
                            device['Channel'][ch_name] = def_device['Channel'][ch_name]
                        else:
                            device['Channel'][ch_name] = {}
                    else:
                        device['Channel'][ch_name] = {}
            
            if add_data != []:
                if device_id == add_data[0]:
                    device = self.merge_dictionaries(device, add_data[1])   
            
            # Add all other default device parameters
            for parameter in def_device:
                if parameter not in device:
                    device[parameter] = def_device[parameter]
                    
            data[device_id] = device

            
    def merge_dictionaries(self, default, individual):
        """
        Merge two dictionaries, individual vales overwrite or added to the default one
        """
        result = default.copy()  # Start with a copy of the default dictionary
        for key, value in individual.items():
            if (key in result and isinstance(result[key], dict)
                and isinstance(value, dict)):
                # If both the current value in result and the new value are 
                # dictionaries, merge them recursively
                result[key] = self.merge_dictionaries(result[key], value)
            else:
                # Otherwise, overwrite the value in the result with the value
                # from the individual dictionary
                result[key] = value

        return result
      
        
    def save_config(self, auto=False, add_data=[]):
        """
        Save the current config content (of the selected module)
        and update the tkinter GUI
        """
        if self.config_content != {}:
            msg = ("Do you really want to overwrite the selected config file?\n"
                   + "Any changes cannot be undone.")

            if auto or self.verify_before_execute(msg):
                try:
                    self.update_config_content(add_data)
                except Exception as err:
                    self.error_box(str(err))
                    
                else:
                    filename = os.path.join(self.configs_directory, 'modules_yaml',
                                            self.selected_option.get() + '.yaml')

                    faf.write_yaml(filename, self.config_content['Setup'])
                    
                    self.cfgs = self.get_module_configs()
                    self.update_content(a=1)
                    
                buttons = [self.save_btn, self.back_btn]
                self.en_disable_buttons(buttons, True)



