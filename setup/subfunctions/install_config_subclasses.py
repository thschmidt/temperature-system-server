
###############################################################################
# IMPORTS
###############################################################################  

# Some tkinter
import tkinter as tk
from tkinter import ttk
from tkinter import messagebox
from tkinter import filedialog

# Multithreading

import os

#
import time

# Regular expression
import re

# 
import version_package_install as vpi
from file_and_folder import FileAndFolder as faf


###############################################################################
# SUB CLASS
###############################################################################  


class Defaults:
    """
    Includes some default parameters and functions
    """
    def __init__(self, parent, controller, home):
        # 
        self.controller = controller
        self.parent = parent

        self.progress_bar = ttk.Progressbar(self,
                                            orient="horizontal",
                                            mode="determinate")
        self.process_amount = 1
        
        # Button Frame at the bottom
        self.buttons_frame = tk.Frame(self)
        self.buttons_frame.pack(fill="x", padx=10, pady=5, side='bottom')
        
        # Return to Menu Button
        self.back_btn = tk.Button(self.buttons_frame, text="Main Menu",
                        command=lambda: controller.show_frame(home))
        
        # Reloading of filenames in the directories
        self.reload_defaults()

    """----------------------- INITIAL PAGE FUNCTIONS ----------------------"""  

    def reload_defaults(self):
        """
        Reload some default parameters, including the filenames of directories
        """
        # Main directory of the temperature system server
        self.main_directory = os.path.dirname(os.getcwd())
        # Config and module directory, created after first initialization
        self.configs_directory = os.path.join(self.main_directory, 'configs')
        self.modules_directory = os.path.join(self.main_directory, 'modules')
        # Sub directory of the main directory,
        # including all the default config files
        self.setup_directory = os.path.join(os.getcwd(), 'setup')
        
        # Folder of the default config files
        self.server_config_folder = os.path.join(self.setup_directory,
                                                'server','configs')

        # Extract the config filenames
        self.server_config_files = faf.get_filenames(self.server_config_folder,
                                                     ending='yaml',
                                                     fullpath=False)
        
        # Set the directory for the config files
        self.program_folders = {'configs':['modules_yaml',
                                           'messengers_yaml'],
                                'modules':['data_logger', 
                                           'temp_control'],
                                'messengers':['couriers']}
        self.program_files = {'configs':self.server_config_files,
                              'modules':['init_modules.py',
                                         'modules_setup.yaml'],
                              'messengers':['init_messengers.py',
                                            'messengers_setup.yaml']}

        # Reset any progress bar
        self.progress_bar['value'] = 0

    """--------------------- LOG AND PROGRESS FUNCTIONS --------------------""" 

    def update_progress_bar(self, value):
        """Update the progress bar by the given value"""
        self.progress_bar['value'] += value

    def show_log(self, message, msg_type='info', header=0):
        """
        Show any log message in the log box,
        change the styling depeding on its tag
        """
        # depending if the message is a header add some symbols
        if msg_type in ['error', 'warning']:
            message = msg_type.capitalize() + ': ' + str(message)
        elif header > 0:
            signs = "="*(40-header*5) 
            message = signs + '\n' + message + '\n' + signs
        
        # Make the textbox editable, enter the new message
        # and close it afterwards
        self.log_text.config(state='normal')
        self.log_text.insert(tk.END, message + '\n', msg_type)
        self.log_text.config(state='disabled')
        
        # Change the styling of the message depending on its tag
        self.log_text.tag_config('error', foreground='red')
        self.log_text.tag_config('warning', foreground='orange')
        self.log_text.tag_config('info', foreground='black')
        self.log_text.tag_config('success', foreground='green')
        
        # Auto-scroll to the bottom
        self.log_text.yview(tk.END)  
        
        # Update the progressbar if successful
        if msg_type == 'success':
            self.update_progress_bar(100/self.process_amount)
            
    def reset_log_text(self):
        """Remove the content in the log textbox"""
        self.log_text.config(state='normal')
        self.log_text.delete('1.0', tk.END)
        self.log_text.config(state='disabled')
     
    """----------------------- INSTALLATION FUNCTIONS ----------------------""" 
            
    def install_python_packages(self, filename):
        """Read ASCII like file with required packages and install them"""
        with open(filename, 'r') as f:
            packages = f.read().split('\n')
        for p in packages:
            if "#" not in p and p != '':
                self.show_log('Install Package: ' + p)
                vpi.pip_install(p)
                self.show_log('Done')
                # Wait after every installed package
                time.sleep(2)

    """---------------------- CHECK / PROOF FUNCTIONS ---------------------""" 

    def check_for_config_files(self):
        """Check if config files and their paths already exist"""
        for file_category in self.program_files:
            for subfl in self.program_files[file_category]:
                if file_category == 'configs':
                    file_path = os.path.join(self.configs_directory, subfl)
                elif file_category == 'modules':
                    file_path = os.path.join(self.modules_directory, subfl)
                if not faf.check_path(file_path):
                    raise Exception(file_path + "does not exist.\n"
                                    + "Please Install the server first")

    def check_first_installation(self):
        """
        Check if the temperature server system has already been installed
        by checking the existence of the different paths (config and module)
        """
        if (faf.check_path(self.modules_directory)
            and faf.check_path(self.configs_directory)):
            return False
        return True
    
    """-------------------------- FRAME FUNCTIONS -------------------------""" 
             
    def clear_frame(self, frame):
        """Remove the content in the given frame"""
        for widget in frame.winfo_children():
            widget.destroy()
    
    """------------------------------- POPUPS ------------------------------""" 
    
    def verify_before_execute(self, msg, cmd="Verify"):
        """
        Open a verification box and return the selected user input (Yes/No)
        """
        response = messagebox.askyesno(cmd, msg)
        return response
    
    def error_box(self, msg):
        """Show an error pop up"""
        messagebox.showerror("Error", msg)
        
    def info_box(self, msg):
        """Show an info pop up"""
        messagebox.showinfo("Error", msg)
    
    """------------------------------ BUTTONS -----------------------------""" 
        
    def en_disable_buttons(self, buttons, enable=False, ignore=[]):
        """En- or disable the given button list"""
        state = 'disabled'
        if enable:
            state = 'normal'
        
        for btn in buttons:
            if btn not in ignore:
                btn.config(state=state)
        
    
class ContentWidget:
    def __init__(self):
        pass
    
    def create_widgets_for_dict(self, container, data, parent_key=""):
        """
        Convert the content of a dictionary into Labels, Textinputs or subframes
        """
        
        # Initialize row index for grid placement
        row_index = 0  
        for key, value in data.items():
            if isinstance(value, dict):
                if 'VALUE' in value:
                    state = 'normal'
                    if 'DISABLED' in value:
                        state = 'disabled'
                    
                    # Get the value from each parameter of the config dict
                    val = value['VALUE']
                    if value['TYPE'] == 'Path':
                        # Create a frame including the path as label
                        # and a button to edit its path
                        value_var = tk.StringVar(value=str(val))
                        
                        value_frame = ttk.Frame(container)
                        value_frame.grid(row=row_index, column=1,
                                         padx=5, pady=2)
                        
                        self.create_path_widgets(value_frame, value_var)
                        
                        # Put the input variable in a key dictionary
                        self.input_vars[parent_key + key] = value_var
                        
                    elif isinstance(val, list):
                        # Create a list frame
                        # with each value of the list as textinput
                        list_frame = ttk.Frame(container)
                        list_frame.grid(row=row_index, column=1,
                                        padx=5, pady=2)
                        
                        self.create_list_widgets(list_frame, val,
                                                 parent_key + key)
                    elif isinstance(val, bool) or 'OPTIONS' in value:
                        # Return the selection options as user input
                        if 'OPTIONS' in value:
                            options = value['OPTIONS']
                        else:
                            options = ['True', 'False']
                        
                        value_var = tk.StringVar()
                        value_var.set(str(val))
                        value_optionmenu = tk.OptionMenu(container,
                                                         value_var,
                                                         *options)
                        value_optionmenu.config(state=state)
                        value_optionmenu.grid(row=row_index, column=1,
                                              padx=5, pady=2)
                        
                        # Put the input variable in a key dictionary
                        self.input_vars[parent_key + key] = value_var
                    else:
                        if val == None:
                            val = ""
                             
                        # Default:
                        # Create for the value ann textinput
                        
                        value_var = tk.StringVar(value=str(val))
                        value_entry = ttk.Entry(container,
                                                textvariable=value_var,
                                                state=state)
                        value_entry.grid(row=row_index, column=1,
                                         padx=5, pady=2)
                        
                        if value['TYPE'] == 'Password':
                            value_entry.config(show='*')
                        
                        # Put the input variable in a key dictionary
                        self.input_vars[parent_key + key] = value_var
                    
                    if 'COMMENT' in value:
                        # Add a comment to the parameter if exists
                        comment_label = ttk.Label(container,
                                                  text=value['COMMENT'])
                        comment_label.grid(row=row_index, column=2,
                                           sticky='w', padx=5)
                        
                    # Add key as Label    
                    key_label = ttk.Label(container, text=key)
                    key_label.grid(row=row_index, column=0,
                                   sticky='w', padx=5)

                else:
                    if isinstance(value, dict):
                        # Create a subframe (as LabelFrame)
                        sub_frame = ttk.LabelFrame(container,
                                                   text=key)
                        sub_frame.grid(row=row_index, column=0,
                                       columnspan=3, sticky='ew')
                        # Create wdigets in subframe
                        self.create_widgets_for_dict(sub_frame, value,
                                                     parent_key + key + ".")

            row_index += 1
    
    def create_list_widgets(self, container, lst, parent_key, ad=''):
        """
        Create for each element in the list a textinput in the container (frame)
        Delete button next to each to enable removing the input
        Add button at the button to add addiotional input values
        """
        for i, item in enumerate(lst):
            # Get item from the input_vars of previous frame
            if ad == 'add' and i != len(lst)-1:
                item = self.input_vars[parent_key + f"[{i}]"].get()
            elif 'del' in ad:
                # ad[3] == index which has been deleted
                if i >= int(ad[3]):
                    j = i+1
                else:
                    j = i
                if parent_key + f"[{j}]" in self.input_vars:
                    item = self.input_vars[parent_key + f"[{j}]"].get()
                    
            value_var = tk.StringVar(value=str(item)) 
            value_entry = ttk.Entry(container, textvariable=value_var)
            value_entry.grid(row=i, column=0, padx=5, pady=2)
            
            # Create a delete button
            del_button = ttk.Button(container,
                                    text="Del",
                                    command=lambda i=i: self.delete_list_item(container,
                                                                              lst,
                                                                              i,
                                                                              parent_key),
                                    width=4)
            del_button.grid(row=i, column=1, padx=5, pady=2)
            
            self.input_vars[parent_key + f"[{i}]"] = value_var
            
        # Add an add button at the end
        add_button = ttk.Button(container,
                                text="Add",
                                command=lambda: self.add_list_item(container,
                                                                   lst,
                                                                   parent_key),
                                width=4)
        add_button.grid(row=len(lst), column=0, columnspan=2, padx=5, pady=2)
    

    ## Subfunction of list widget
    
    def delete_list_item(self, container, lst, index, parent_key):
        """Refresh the list frame and remove the indexed value from the list"""
        del lst[index]
        del self.input_vars[parent_key  + f"[{index}]"]
        
        self.clear_frame(container)
        self.create_list_widgets(container, lst, parent_key, 'del' + str(index))
                
    def add_list_item(self, container, lst, parent_key):
        """Refresh the list frame and add a new value add the end"""
        lst.append("")
        self.clear_frame(container)
        self.create_list_widgets(container, lst, parent_key, 'add')
        
    def create_path_widgets(self, frame, value_var):
        """Add path as label and a button to select the path"""
        value_label = ttk.Label(frame,
                                textvariable=value_var)
        value_label.grid(row=0, column=0, padx=5, pady=2)
        
        value_button = ttk.Button(frame,
                                  text='Open',
                                  command=lambda: self.get_directory(value_var))
        value_button.grid(row=0, column=1, padx=5, pady=2)  

    ## Subfunction of path widget
            
    def get_directory(self, value_var):
        """Open a filedialog and update the path variable"""
        path = filedialog.askdirectory()
        if path:
            value_var.set(path)
    
    """---------------------- CREATE WIDGETS FUNCTIONS ---------------------""" 
    
    def update_dict_from_inputs(self, data, parent_key=""):
        """Update the content of the config files with the input values"""
        for key, value in data.items():
            full_key = parent_key + key
            if isinstance(value, dict):
                if 'VALUE' in value:
                    val = value['VALUE']
                    if isinstance(val, list):
                        new_list = []
                        index = 0
                        while f"{full_key}[{index}]" in self.input_vars:
                            new_val = self.input_vars[f"{full_key}[{index}]"].get()
                            new_list.append(self.check_input(new_val,
                                                             value))
                            index += 1
                        data[key]['VALUE'] = new_list
                    else:
                        if value['TYPE'] == 'Password':
                            data[key]['VALUE'] = '*****'
                        else:
                            new_val = self.input_vars[full_key].get()
                            data[key]['VALUE'] = self.check_input(new_val,
                                                                    value)
                else:
                    self.update_dict_from_inputs(value, full_key + ".")
      
    """--------------------- CHECK USER INPUT FUNCTIONS --------------------"""               
      
    def check_input(self, value, parameter):
        """Check correctness of the user inputs"""
        
        # Check if maximum char length is exceeded
        if 'MAXCHR' in parameter:
            if len(value) > parameter['MAXCHR']:
                raise Exception(str(value) + " is too long, max "
                                + str(parameter['MAXCHR'])
                                + " charcters are allowed")
                
        # Check if input type is correct
        if 'TYPE' in parameter:
            input_type = parameter['TYPE']
            try:
                if input_type == 'Boolean':
                    return value == 'True'
                elif input_type == 'Int':
                    return int(value)
                elif input_type == 'Float':
                    return float(value)
                elif input_type == 'IP':
                    if self.validate_ip(value):
                        return value
                    else:
                        raise Exception(str(value) + " is not a valid ip")
                else:
                    return value
            except:
                raise Exception(str(value)
                                + " is in the wrong input type, it must be "
                                + input_type)
        else:
            return value
        
    def validate_ip(self, ip):
        """Check if the input is a valid ip"""
        a = ip.split('.')
        if len(a) != 4:
            return False
        for x in a:
            if not x.isdigit():
                return False
            i = int(x)
            if i < 0 or i > 255:
                return False
        return True
  
    
  
class InputPage(Defaults):
    def __init__(self, root, verify):
        
        self.window = tk.Toplevel(root)
        
        # 
        self.verify = verify
        
        # Input Box and Label
        self.label = tk.Label(self.window, text="Define a device ID:")
        self.label.pack()
        
        self.user_input = tk.StringVar()
        
        self.device_name_entry = ttk.Entry(self.window,
                                           textvariable=self.user_input)
        self.device_name_entry.pack()
        
        # Error Label (below the input, only show content on error)
        self.error_var = tk.StringVar()
        self.error_label = tk.Label(self.window,
                                    textvariable=self.error_var)
        self.error_label.pack()  
        
        # Button Frame at the bottom
        self.buttons_frame = tk.Frame(self.window)
        self.buttons_frame.pack(fill="x", padx=10, pady=5, side='bottom')
        
        self.acc_btn = tk.Button(self.buttons_frame,
                                 text='Accept',
                                 command=self.accept)
        self.acc_btn.pack(side="left")
        
        self.cancel_btn = tk.Button(self.buttons_frame,
                                    text='Cancel',
                                    command=self.cancel)
        self.cancel_btn.pack(side="right")
    
        self.window.wait_window()
        
    def accept(self):
        """
        Accept the current defined ID and close sub Window
        on nomenclature confirmation
        """
        try:
            self.confirm_nomenclature(self.user_input.get())
        except Exception as err:
            self.error_var.set(err)
        else:
            self.verify = True
            self.window.destroy()
            
    def cancel(self):
        """Cancel defining a device/series ID"""
        self.window.destroy()
            
    def get_variables(self):
        # Return the user input
        return self.user_input.get(), self.verify
    
    
    """--------------------------- NOMENCLATURE ---------------------------"""    
    
    def confirm_nomenclature(self, input_id, id_type="device"):
        """
        Confirm the nomenclature of the device and series ID
        """
        self.check_alphanumeric(input_id, id_type)
        self.check_lowercase(input_id, id_type)
        self.check_string_length(input_id, id_type) 
        
    def check_alphanumeric(self, input_id, id_type):
        """
        Check if the ID of series or device contains only chars like 'a-z'
        or numbers
        """
        if not self.is_alphanumeric(input_id):
            raise Exception("The " + id_type + "-id '" + input_id
                            + "' is not allowed as ID. "
                            + "Only Numbers and characters "
                            + "are allowed.")
    
    def check_lowercase(self, input_id, id_type):
        """Check if the ID of series or device is written in lowercase"""
        for char in input_id:
            if char.isalpha() and not char.islower():
                raise Exception("The " + id_type + "-id '" + input_id
                                + "' must be lowercase")

    def check_string_length(self, input_id, id_type):
        """
        Check if the ID of series or device isn't longer than 7 or
        shorter than 4 chars
        """
        if len(input_id) > 7:
            raise Exception("The " + id_type + "-id '" + input_id
                            + "' is too long (maximum 7 chars)")
        elif len(input_id) < 4:
            raise Exception("The " + id_type + "-id '" + input_id 
                            + "' is too short (minimum 4 chars)")
            
    def is_alphanumeric(self, input_string):
        # Define a regular expression pattern that matches only alphanumeric characters
        pattern = re.compile(r'^[a-zA-Z0-9]+$')
        # Use the search function to find if the pattern matches the input string
        match = re.search(pattern, input_string)
        # If match is found, return True, otherwise return False
        return bool(match)   



class ConnectionWindow(Defaults):
    def __init__(self, instance, device_id):
        self.window = tk.Toplevel(instance.controller)
        self.window.protocol("WM_DELETE_WINDOW", self.disable_event)
        self.queue = instance.queues[1]
        
        self.device_id =  device_id
        self.instance = instance
        
        label = tk.Label(self.window, text="Wait for connection")
        label.pack()
        self.progress = ttk.Progressbar(self.window, length=300, mode='indeterminate')
        self.progress.pack(pady=20)
      
    def disable_event(self):
        return  
      
    def wait_connection(self):
        device = self.instance.config_content['Setup']['ID'][self.device_id]
        connection = device['Connection']
        
        for i in range(100):
            self.progress['value'] = i*10
            self.window.update_idletasks()
            try:
                msg = self.queue.get(timeout=0.1)
            except Exception:
                if i == 60:
                    msg = {'ERROR':'Unknown Error'}
                    break
            else:
                break
            
        self.window.destroy()
        if 'ERROR' in msg:
            if connection['Type'] in device['Connection']['valid']:
                device['Connection']['valid'].remove(connection['Type'])
            self.instance.error_box(msg['ERROR'])
        else:
            if connection['Type'] not in device['Connection']['valid']:
                device['Connection']['valid'].append(connection['Type'])
          
        add_data = []
        if 'DATA' in msg:
            if msg['DATA'] != None:
                add_data = [self.device_id, msg['DATA']]
            
        self.instance.save_config(True, add_data)


    