
###############################################################################
# IMPORTS
###############################################################################  

import sys
import subprocess
import time

###############################################################################
# FUNCTIONS
###############################################################################  


def pip_install(package): 
    """Install defined package"""
    subprocess.check_call([sys.executable,
                           "-m", "pip", "install",
                           package])

def install_python_packages(filename):
    """Read ASCII like file with required packages and install them"""
    with open(filename, 'r') as f:
        packages = f.read().split('\n')
    package_list = []
    for p in packages:
        if "#" not in p and p != '':
            # print('Install Package: ' + p)
            pip_install(p)
            package_list.append(p)
            # print('Installation complete')
            time.sleep(0.1)
    return package_list
   

def check_python_version(min_version, max_version=''):
    """Check if current python version fulfill minimum/maximum requirements"""
    min_version_tuple = tuple(map(int, min_version.split('.')))
    current_version = sys.version_info
    if current_version <= min_version_tuple :
        raise Exception("Your python version is to old."
                        + " Required >= " + min_version)
        
    if max_version != '':
        max_version_tuple = tuple(map(int, max_version.split('.')))
        if current_version > max_version_tuple :
            raise Exception("Python version is not compatible. "
                            + "Compatible versions: " 
                            + min_version + " - " + max_version)
    return current_version

def read_version_file(filename):
    """Get allowed versions (minimum, maximum) from ASCII like file"""
    max_version = ''
    with open(filename, 'r') as f:
        versions = f.read().split('\n')
    min_version = versions[0].split(' ')[1]
    if len(versions) > 1:
        max_version = versions[1].split(' ')[1]
    return min_version, max_version

# Check for pyyaml package and install if not existing
try:
    import yaml
except ModuleNotFoundError:
    pip_install('pyyaml')

