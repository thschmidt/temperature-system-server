
###############################################################################
# IMPORTS
###############################################################################  

import os
import glob
from pathlib import Path

from shutil import copyfile
from shutil import copytree

# Pyyaml
try:
    import yaml
except ImportError:
    # Install pyyaml if not installed
    import version_package_install as vpi
    vpi.pip_install('pyyaml')
    import yaml

###############################################################################
# CLASS
############################################################################### 

class FileAndFolder:
    def create_folder(folder_name, path=''):
        """Check existens of folder and create not existing folders,
        input: folder as raw string"""
        if path != '':
            directory = os.path.join(path, folder_name)
        else:
            directory = folder_name
        if os.path.exists(directory):
            return "Directory " + directory + " already exists"
        Path(directory).mkdir(parents=True, exist_ok=True)
        return "Created " + directory
        
    def get_subdirectory(current_directory, level=0):
        """Get path of path of 'level' above the subdirectory"""
        for i in range(level):
            current_directory = os.path.dirname(current_directory)
        return current_directory
            
    def copy_file(origin_dir, destination_dir,
                  origin_filename, destination_filename=''):
        """Copy file to destination if it exists"""
        if destination_filename == '':
            destination_filename = origin_filename
        destination_file = os.path.join(destination_dir, destination_filename)
        if not os.path.exists(destination_file): 
            copyfile(os.path.join(origin_dir, origin_filename),
                     destination_file)  
            return "Created " + destination_file
        else:
            return "File " + destination_file + " already exists"
     
    def copy_folder(origin_dir, destination_dir):
        """Copy a folder with its content into the destination directory"""
        try:
            # Ensure the destination folder exists
            os.makedirs(destination_dir, exist_ok=True)
            # Copy the folder and its contents
            copytree(origin_dir, destination_dir, dirs_exist_ok=True)
            return ("Copied " + origin_dir + " to " + destination_dir)
        except Exception as err:
            return (f"Error: {err}")
     
        
    def overcopy_file(origin_dir, destination_dir,
                  origin_filename, destination_filename=''):
        """Overwrite the file at the destination"""
        if destination_filename == '':
            destination_filename = origin_filename
        destination_file = os.path.join(destination_dir, destination_filename)
        copyfile(os.path.join(origin_dir, origin_filename),
                 destination_file) 
    
    def get_filenames(folder, ending="*", fullpath=True):
        """Show all filenames in folder,
        input: folder as raw string, ending of files and return as full path 
        return: list of filenames"""
        filenames = glob.glob(os.path.join(folder,'*.' + ending))
        if fullpath:
            return filenames
        else:
            return [os.path.split(str(elem))[1] for elem in filenames]
    
    def get_subdirectories(folder, fullpath=True):
        subfolders = glob.glob(folder + '/*/',recursive = True)
        if fullpath:
            return subfolders
        else:
            return [os.path.split(elem[:-1])[1] for elem in subfolders]
    
    def create_file(filename, content):
        with open(filename, 'w') as f:
            f.write(content)
            
    def read_yaml(filename):
        with open(filename, 'r', encoding='utf-8') as f:
            data = yaml.safe_load(f)
        return data
    
    def write_yaml(filename, data):
        with open(filename, 'w') as f:
            yaml.safe_dump(data, f,
                           default_flow_style=False,
                           indent=4, sort_keys=False)
    
    def check_path(path):
        return os.path.exists(path)
