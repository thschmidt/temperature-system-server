
###############################################################################
# IMPORTS
###############################################################################  

# Some tkinter
import tkinter as tk
from tkinter import scrolledtext
from tkinter import ttk


# Multithreading
import threading

import os

#
import time


import socket

import platform    # For getting the operating system name
import subprocess  # For executing a shell command

# 
import version_package_install as vpi
from file_and_folder import FileAndFolder as faf

from install_config_subclasses import Defaults, ContentWidget


###############################################################################
# SERVER CLASSES
###############################################################################  

class ServerInstallPage(tk.Frame, Defaults):
    def __init__(self, parent, controller, home, queues):
        tk.Frame.__init__(self, parent)
        
        # Initialize some default parameter
        Defaults.__init__(self, parent, controller, home)

        ## Header Label ##
        self.header_label = tk.Label(self,
                                     text='Server Installation',
                                     font=("Courier", 20))
        self.header_label.pack(pady=5)
        
        # Logging textbox #
        self.log_text = scrolledtext.ScrolledText(self, width=50, height=14)
        self.log_text.pack(pady=5)

        # Progress bar #
        self.progress_bar.pack(fill=tk.X, padx=10, pady=5)        

        ### Button frame ###
        self.init_btn = tk.Button(self.buttons_frame,
                                  text="Install",
                                  command=self.install_thread)
        self.init_btn.pack(side='left', padx=10)
        
        # Button to get to the server config page
        self.config_btn = tk.Button(self.buttons_frame,
                                    text="Go to Server Config",
                                    command=lambda: controller.show_frame(ServerConfigPage),
                                    state='disabled')
        self.config_btn.pack(side='left', padx=10)
        
        # Return back to the Main Menu
        self.back_btn.pack(side="right", padx=10)
        
        if not self.check_first_installation():
            self.config_btn.config(state='normal')

    """----------------------- INITIAL PAGE FUNCTIONS ----------------------"""    

    def reset_page(self):
        """Reset several parameters of the current page"""
        # Reset the log textbox
        self.reset_log_text()
        # Reload some default paths and filename parameters
        self.reload_defaults()

    """---------------------- INSTALLATION FUNCTIONS ----------------------"""         

    def install_thread(self):
        """
        Opens an installation thread so the log textbox can be updated in parallel
        """
        # Disable the button to prevent multiple loggers
        self.init_btn.config(state='disabled')

        # Start a thread to simulate real-time logging
        threading.Thread(target=self.install, daemon=True).start()
        
    def install(self):
        self.process_amount = 5
        try:
            ## Check Python version ##
            self.show_log('Check Python Version', header=2)
            min_version, max_version = vpi.read_version_file('required_version.txt')
            curr_version = vpi.check_python_version(min_version, max_version)
            curr_version_str = '{}.{}.{}'.format(*curr_version)
            self.show_log(curr_version_str, 'success')
            
            ## Install some python packages ##
            self.show_log('Install Python Packages', header=2)
            time.sleep(1)
            self.install_python_packages('requirements.txt')
            self.show_log('All packages installed', 'success')
    
            ## Create directories ##
            self.show_log('Create Directories', header=2)
            self.create_dictionaries()
            self.show_log('Created', 'success')
            
            self.show_log('Copy Files', header=2)
            ## Copy init modules file ##
            modules_directory = os.path.join(self.main_directory,
                                             'modules')
            for file in self.program_files['modules']:
                org_file = self.get_org_file(file)
                self.show_log(faf.copy_file(os.path.join(self.setup_directory,
                                                         'modules'),
                                            modules_directory,  org_file,
                                            file))
                
            ## Copy init messenger files ##
            messengers_directory = os.path.join(self.main_directory,
                                                'messengers')

            for file in self.program_files['messengers']:
                org_file = self.get_org_file(file)
                self.show_log(faf.copy_file(os.path.join(self.setup_directory,
                                                         'messengers'), 
                                            messengers_directory, org_file,
                                            file))
            
            ## Copy config files ##
            config_files = faf.get_filenames(self.server_config_folder, 
                                             ending='yaml', 
                                             fullpath=False)
            for cfg_fl in config_files:
                self.show_log(faf.copy_file(self.server_config_folder, 
                                            self.configs_directory, cfg_fl))
                
            # Copy the default email messenger config file in the config folder
            email_config_path = os.path.join(self.setup_directory,
                                             'messengers',
                                             'configs')
            self.show_log(faf.copy_file(email_config_path,
                                        os.path.join(self.configs_directory,
                                                     'messengers_yaml'),
                                        'email.yaml'))
            
            self.show_log('Copied', 'success')
            self.show_log('+++ Installation successfully +++', 'success')
            
            if not self.check_first_installation():
                self.config_btn.config(state='normal')
            
        except Exception as err:
            self.process_amount = 0
            self.show_log(str(err), 'error')
        # Reset install button 
        self.init_btn.config(state='normal')

    def get_org_file(self, file):
        if 'py' in file:
            return file[:-3] + '_tmpl.py'
        return file


    def create_dictionaries(self):
        """Create the different module sub-dictionaries"""
        for fld in self.program_folders:
            if self.program_folders[fld] == []:
                # Create module dictionary
                self.show_log(faf.create_folder(fld, self.main_directory))
            else:
                # Create module subdictionaries
                for subfld in self.program_folders[fld]:
                    sub_dir = os.path.join(fld, subfld)
                    self.show_log(faf.create_folder(sub_dir,
                                                    self.main_directory))
                    
        

class ServerConfigPage(tk.Frame, Defaults, ContentWidget):
    def __init__(self, parent, controller, home, queues):
        tk.Frame.__init__(self, parent)
        
        # Initialize some default parameter
        Defaults.__init__(self, parent, controller, home)
        
        # Save config and current inputs in a variable
        self.input_vars = {}
        self.cfgs = {}
        
        ## Header Label ##
        self.header_label = tk.Label(self,
                                     text='Server Configuration',
                                     font=("Courier", 20))
        self.header_label.pack(pady=5)
        

        ### Header Frame ###
        self.top_frame = tk.Frame(self)
        self.top_frame.pack(fill="x", padx=10, pady=5)
        
        top_label = tk.Label(self.top_frame, text='Config File:')
        top_label.pack(pady=5, side='left')
        
        # Create dropdown for config files
        options = list(self.cfgs.keys())
        self.selected_option = tk.StringVar()
        self.selected_option.trace('w', self.update_content)

        self.dropdown = ttk.OptionMenu(self.top_frame,
                                      self.selected_option,
                                      self.selected_option.get(),
                                      *options)
        self.dropdown.pack(pady=5, side='left')
        
        ### Content frame ###
        self.content_frame = tk.Frame(self)
        self.content_frame.pack(fill="x", padx=10, pady=5)

        ### Button frame ###
        # Button to save current config
        self.save_btn = tk.Button(self.buttons_frame,
                                  text="Save Config",
                                  command=self.save_config)
        self.save_btn.pack(side="left", padx=10)
         
        # Button to reset to default config
        self.reset_btn = tk.Button(self.buttons_frame,
                                   text="Reset Config To Default",
                                   command=self.reset_config)
        self.reset_btn.pack(side="left", padx=10)
        
        # Button to check port and ip
        self.con_btn = tk.Button(self.buttons_frame,
                                 text="Check Connectivity",
                                 command=self.check_server_connectivity)
        self.con_btn.pack(side="left", padx=10)
        
        # Button to return to main menu
        self.back_btn.pack(side="right", padx=10)
    
    """----------------------- INITIAL PAGE FUNCTIONS ----------------------"""  
         
    def reset_page(self):
        """Reset several parameters of the current page"""
        # Define page geometry
        self.controller.geometry("750x500")
        
        # Reload some default paths and filename parameters
        self.reload_defaults()
        
        # Reset the content of the content frame
        self.config_content = {}
        self.clear_frame(self.content_frame)
        
        # Get the content of the config files
        self.cfgs = self.get_configs()
        
        # Reset the options in the config file dropdown menu
        menu = self.dropdown['menu']
        menu.delete(0, 'end')
        
        # Add the new options to the config dropdown menu
        for option in list(self.cfgs.keys()):
            menu.add_command(label=option,
                             command=tk._setit(self.selected_option, option))
        
        default_option = list(self.cfgs.keys())[0]
        self.selected_option.set(default_option)
        

    def get_configs(self):
        """Get the content of the config files and return it as dictionary"""
        config_files = faf.get_filenames(self.configs_directory, ending='yaml')
        
        # Define the dict
        cfgs = {}
        for conf_file in config_files:
            cfg_content = faf.read_yaml(conf_file)
            # Different handling of the paths config
            if os.path.basename(conf_file) == 'paths.yaml':
                cfg_content['LOGS'] = self.set_default_path(cfg_content['LOGS'])
            if os.path.basename(conf_file)[:-5] != 'email':
                cfgs[os.path.basename(conf_file)[:-5]] = cfg_content
        return cfgs
      
    """-------------------------- BUTTON FUNCTIONS -------------------------"""  
    
    def save_config(self):
        """
        Save the current config content (of the selected file)
        and update the tkinter GUI
        """
        if self.config_content != {}:
            # Message to verify overwrite
            msg = ("Do you really want to overwrite the selected config file?\n"
                   + "Any changes cannot be undone.")
            
            if self.verify_before_execute(msg):
                try:
                    self.update_dict_from_inputs(self.config_content, 
                                                 parent_key="")
                except Exception as err:
                    self.error_box(str(err))
                    
                else:
                    # Write content in config file
                    filename = os.path.join(self.configs_directory,
                                            self.selected_option.get() + '.yaml')
                    faf.write_yaml(filename, self.config_content)
                    
                    # Get updated content from the config files
                    self.cfgs = self.get_configs()
                    # Update the tkinter GUI
                    self.update_content()
            
    def reset_config(self):
        """
        Replace the config content with the one from the default config file
        and update the tkinter GUI
        """
        if self.config_content != {}:
            # Message to verify overwrite
            msg = ("Do you really want to reset the selected config file?\n"
                   + "Any reset cannot be undone.")
            
            if self.verify_before_execute(msg):
                # Replace the current config file with the default one
                faf.overcopy_file(self.server_config_folder,
                                  self.configs_directory,
                                  self.selected_option.get() + '.yaml')
                
                # Get updated content from the config files
                self.cfgs = self.get_configs()
                # Update the tkinter GUI
                self.update_content()
                
    def check_server_connectivity(self):
        """
        Check the connectivity of the server,
        if the input port is available and the input IP is a system_ip
        """
        # Get server port and ...
        use_system_ip = self.input_vars['CONNECTION.use_system_ip'].get()
        port = self.input_vars['CONNECTION.server_port'].get()
        
        ip_correct = True
        # Get the hostname of the sysyem
        hostname = socket.gethostname()    
        if use_system_ip == 'True':
            ip = socket.gethostbyname(hostname)
        else:   
            ip = self.input_vars['CONNECTION.server_ip'].get()
            # Get list of system ips
            system_ips = socket.gethostbyname_ex(hostname)[2]
            system_ips.append('127.0.0.1')
            # Check if the input ip belongs to the system
            if ip not in system_ips:
                ip_correct = False
        
        # Check if the input port is open
        port_open = self.is_port_in_use(port) == False
        
        msg_hint = ("Hint: This result will not ensure an accessiblity in your network."
                     + " You may try to ping this PC from a different PC.")
        
        # Return either an info (success) or error popup 
        if ip_correct and port_open:
            msg = ("Connectivity Check successful:\nIP: " 
                   + str(ip) + "\nPort: " + str(port))
            msg += "\n" +  msg_hint
            self.info_box(msg)
        else:
            msg = ("Connectivity Check failed:\nIP: "
                   + str(ip) + "\nPort: " + str(port))
            if ip_correct == False:
                msg += "\nThe IP does not belong to this system."
            else:
                msg += "\nThe port is already used by a different application."
            self.error_box(msg)
 
    """------------------------- CHECKBOX FUNCTIONS ------------------------"""  
    
    def update_content(self, a=0, b=0, c=0):
        """
        Update the content frame of the tkinter GUI
        """
        # a,b,c some parameters given by the tracing of 'self.selected_option'
        
        if self.selected_option.get() == 'config':
            self.con_btn["state"] = "normal"
        else:
            self.con_btn["state"] = "disabled"
          
        # Clear the content frame
        self.clear_frame(self.content_frame)
        
        # Get the content of the selected config file
        self.config_content = self.cfgs[self.selected_option.get()]
        
        # Arrange the config content in the content frame
        self.create_widgets_for_dict(self.content_frame, self.config_content)
      
    """---------------------- CREATE WIDGETS FUNCTIONS ---------------------"""  
    
    def is_port_in_use(self, port):
        """Check if port is used"""
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            return s.connect_ex(('localhost', int(port))) == 0
        
    def ping_ip(self, host):
        """
        Returns True if host (str) responds to a ping request.
        Remember that a host may not respond to a ping (ICMP) request
        even if the host name is valid.
        """
    
        # Option for the number of packets as a function of
        param = '-n' if platform.system().lower()=='windows' else '-c'
    
        # Building the command. Ex: "ping -c 1 google.com"
        command = ['ping', param, '1', host]
    
        return subprocess.call(command) == 0

    """---------------------- ADDITIONAL SUB FUNCTIONS ---------------------"""  
            
    def set_default_path(self, config):
        """Define the default logging path if no path is selected"""
        for parameter in config:
            if config[parameter]['VALUE'] in ['', None]:
                config[parameter]['VALUE'] = (str(os.getcwd()[:3])
                                              + r'LOGS/Temperature_System/'
                                              + parameter + 'log')
        return config

