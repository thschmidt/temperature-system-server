
# System and os
import os
import sys

# Connect other folders / include scripts
main_path = os.path.dirname(os.getcwd())
sys.path.insert(1, 'setup')
sys.path.insert(1, 'setup/server')
sys.path.insert(1, 'setup/messengers')
sys.path.insert(1, 'setup/modules')
sys.path.insert(1, 'setup/subfunctions')

