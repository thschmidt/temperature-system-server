# Temperature System
Version 0.3.0

The Temperature System is a reliable and flexible software solution designed to measure and control a wide variety of sensors. Its modular structure and Python-based framework make it easy to implement new control units or add various temperature logging devices. 

## Documentation
Detailed description can be found in the docs folder.

## Compatible Modules
* [Keysight 34972A](https://gitlab.mpcdf.mpg.de/thschmidt/keysight-34972a-module)
* [TTi PL Series](https://gitlab.mpcdf.mpg.de/thschmidt/tti-pl-series-module)
* [Lakeshore Model 336](https://gitlab.mpcdf.mpg.de/thschmidt/lakeshore-model-336-module)