# Change Log for Temperature-System-Server
All notable changes to `Temperature-System-Server` will be documented in this file.

## 0.3.0 - 2024-11-07

### Added
 - Messenger for Notifications (only via email)
 - Template Module to create own modules
 - Create summary at the end of a campaign (only temperature data)
 - Sensor status, out of limit and faulty notification
 - Tasks-Webpage:
   - More conditions (Temperature Below or Above)
   - AND and OR logic (Using numbers)
   - Copy existing Works
 - Temperature-Webpage:
   - More modbar features
 - Campaign-Webpage:
   - Connect all devices of copied campaign
   - Show available Channels of device
### Updated/Changed
 - Whole setup of the software (using Tkinter GUI instead of console)
 - Whole module backend and structure
 - Campaign config file (example: ReadoutRate -> Interval)
 - Temperature-Webpage:
   - Position of modbar
 - Styles of different webpages
### Removed
 - 'Auto' Readout Mode for all devices
 - Update via git (until it's fixed)
### Fixed
 - Many different type of bugs


## 0.2.3 - 2024-07-10

### Added
 - Temperature-Webpage:
    - Added loading circle
### Updated/Changed
 - Temperature-Webpage:
    - Faster loading of temperature data (only load data files in selected range)
    - Cache the ten last new datasets of an active campaign
 - Controller-Webpage:
    - Allow only four devices or channels in each row of the series container


## 0.2.2 - 2024-05-14

### Added
 - Temperature-Webpage: 
    - Button to reload data of current selected sensors
 - Install python packages during update
 - Install individual python packages of the modules (in requirements.txt of the module)
### Fixed
 - Update via git


## 0.2.1 - 2024-04-29

### Fixed
 - Package installation of lakeshore via pip
 - Stopping the devices upon the end of the campaign
 - Loading the temperature graph of an active campaign
 - Arrangement of the header in the controller data logs (new line)
 - Logger error during creation of the log (verification of the sensor timestamps)
 - Missing of initial sensor data of the campaign


## 0.2.0 - 2024-04-22

### Added
 - Automatically search for updates and install them if selected by user on server shutdown (only if git is installed)
### Updated/Changed
 - Create_Campaign-Webpage: 
    - Accept chars as sensor input name (Example: A, B, C or A1-A7)
 - InputType in configs
 - Module structure (additional subscript to reduce the code in the individual modules)
### Fixed
 - Temperature-Webpage: 
    - Zooming during active campaign
    - Shown range during active campaign
 - Saving sensor data of temperature controller


## 0.1.0 - 2024-03-18
First Early Access Version released