
###############################################################################
# IMPORTS
############################################################################### 

# Tkinter GUI
import tkinter as tk
from tkinter import scrolledtext

# Webpage
import cherrypy

# Time and clock related
import time

# System and os
import os
import sys

# Thread and processes
import multiprocessing as mp
import threading as th

# Import tempserver and webserver
from server import webserver
from server import tempserver

# Scriptlib
import process_thread as pat
import file_functions as ffu
import logging_functions as lof

# Subfunctions
from package_broker import Portier, PackageHandler, Cat
import init_settings as its
import version_update as vru 


###############################################################################
# POPUP
###############################################################################  

class ServerPopup:
    def __init__(self, tk_q, use_terminal):
        self.tk_q = tk_q
        self.use_terminal = use_terminal
    
    def disable_event(self):
        return

    def show_logging(self):
        """Show logging either as tkinter window or in terminal"""
        if self.use_terminal:
            update_parameter = self.show_terminal()
        else:
            update_parameter = self.show_tkinter()
        return update_parameter
       
    def show_terminal(self):
        """Show message in cmd window"""
        while 1:
            try:
                msg = self.tk_q.get(timeout=5)
                if msg == 'CLOSE':
                    input('Press enter to close')
                    break
                elif isinstance(msg, dict):
                    return msg
                else:
                    print(msg)
            except mp.queues.Empty:
                pass     

    def show_tkinter(self):
        """Open a small tkinter Popup to initialization error or server ip/port"""
        root = tk.Tk()
        root.title("Server Status")
        # Disable the cross to close and ALT + F4
        root.protocol("WM_DELETE_WINDOW", self.disable_event)
        # Text
        log_text = scrolledtext.ScrolledText(width=50, height=14)
        log_text.pack(pady=5)
        log_text.config(state='disabled')
        # Close Button
        button = tk.Button(root, text="Close", 
                           command=lambda: self.tk_q.put({}))
        button.pack(pady=10)
        button.config(state='disabled')
        
        def check_message_tk():
            while 1:
                try:
                    msg = self.tk_q.get(timeout=5)
                    if msg == 'CLOSE':
                        # Set close button active
                        button.config(state='normal')
                    elif isinstance(msg, dict):
                        self.tk_q.put(msg)
                        break
                    else:
                        # Update the content of the scrollbox
                        log_text.config(state='normal')
                        log_text.insert(tk.END, str(msg) + '\n')
                        log_text.config(state='disabled')
                except mp.queues.Empty:
                    pass
            # Close the window when the server stops
            root.destroy()  
            
        # Start check for messages thread
        cm_thread = th.Thread(target=check_message_tk, daemon=True)
        cm_thread.start()
        root.mainloop()
        
        # Check if message thread still alive
        if cm_thread.is_alive():
            self.tk_q.put({})
            time.sleep(0.1)
            
        return self.tk_q.get()

###############################################################################
# SERVER
###############################################################################  

class Server:  
    def __init__(self, tkinter_q, srv_pop):
        self.main_path = os.path.dirname(os.getcwd())
            
        # Current Service
        self.service = 'TempSrv'
        
        # tkinter / popup queue
        self.tk_q = tkinter_q
        self.srv_pop = srv_pop
        
        # Check for dummymode
        self.dummymode = self.check_for_dummy_mode()
        
    def init(self):
        """Fetch initial parameters and check config files"""
        # Get setup directory and files
        setup_folder = os.path.join(os.getcwd(), 'setup')
        setup_config_folder = os.path.join(setup_folder, 'init_configs')
        config_file_list = ffu.get_filenames(setup_config_folder, 
                                            ending='yaml', 
                                            fullpath=False)
    
        # Check config files exist
        configs_path = os.path.join(self.main_path, 'configs')
        module_path = os.path.join(self.main_path, 'modules')
        initial = its.InitialSetup(configs_path, module_path)
        initial.check_configs_exists(configs_path, config_file_list)
    
        # Check setup
        self.modules_exist, module_processes = initial.check_setup_yaml(module_path)
    
        # Create Process list
        default_processes = ['Logger', 'TaskEv', 'Mssngr']
        self.process_list = module_processes + default_processes
        
        # Initialize the server by checking all input paramters / settings
        self.settings = initial.load_settings(self.dummymode)
        # UPDATE CURRENTLY OFF
        update_cfg = {'check_for_update':False} #self.settings['config']['UPDATE']
        self.update_allowed = vru.check_git_exists(update_cfg['check_for_update'])
        self.current_versions = vru.get_versions(os.getcwd(),
                                                 os.path.join(module_path,
                                                              'modules_setup.yaml'))
        self.settings['version'] = self.current_versions['Versions']['SERVER']
    
    def run(self):
        """"""
        ## Queues and Paths ##
        send_q = mp.Queue()

        recv_qs = pat.create_queues(self.process_list)
        recv_qs[self.service] = mp.Queue()
        
        queues = [send_q, recv_qs]
        update = {'versions':self.current_versions,
                  'allowed':self.update_allowed}
        
        # Start procedure message
        ph = PackageHandler(self.service, queues, self.settings)
        mtext = '=== Start Server ===' 
        ph.create_event_log_package(self.service, event_message=mtext)
        
        ## Open Portiers ##
        doormen = {'Sevent':Portier('service_event'),
                   'Service':Portier('service'),
                   'Data':Portier('data'),
                   'Setting':Portier('setting'),
                   'Version':Portier('version')}
        cherrypy.engine.publish('service_event', [])
        cherrypy.engine.publish('version',
                                {'Current_Version':self.current_versions['Versions']}) 
        
        ## Cat Process (COMMUNICATION ARCHITECTURE) ##
        pc = mp.Process(target = self.broker_process, args = (queues, 
                                                              self.settings, 
                                                              update))
        pc.start()
        
        ## Temperature main thread ##
        pn = th.Thread(target = tempserver.server_thread,
                        daemon = True,
                        args = (queues,
                                self.process_list,
                                self.settings,
                                self.dummymode)
                        )
        pn.start()
        time.sleep(1)
        
        ## Webserver ##
        webth = th.Thread(target =  webserver.webserver_thread,
                          daemon = True,
                          args = (doormen,
                                  self.tk_q,
                                  queues,
                                  self.process_list, 
                                  self.settings,
                                  self.dummymode,
                                  self.modules_exist))
        webth.start()

        # Tkinter Window
        update_parameters = self.srv_pop.show_logging()
        ## Stop procedure ##
        mtext = '=== Shutdown Server ===' 
        ph.create_event_log_package(self.service, event_message=mtext)
        # Send stop to all processes
        pat.close_process_thread_queues(recv_qs) 
        for d in doormen:
            doormen[d].unsubscribe()

        pc.terminate()
        pc.join()
        
        # # Update if active
        if update_parameters != {}:
            vru.update_software(update_parameters, self.current_versions)
  
    def check_for_dummy_mode(self):
        """Check if server is started in dummy mode"""
        dummymode = False
        try:
            if sys.argv[1] == 'dummy':
                dummymode = True
                self.tk_q.put('=== Start Dummymode ===')
        except:
            pass
        return dummymode
    
    def broker_process(self, queues, logpaths, update):
        broker = Cat(queues, logpaths, update)
        broker.run()
 

###############################################################################
# MAIN
###############################################################################  

if __name__ == '__main__':
    # Create a Queue to update the tkinter GUI
    tkinter_q = mp.Queue()
    
    # Check if this python script is opened in terminal
    try:
        use_terminal = sys.stdin.isatty()
    except AttributeError:
        use_terminal = False

    # Tkinter Window
    srv_pop = ServerPopup(tkinter_q, use_terminal)
    
    # Temperature Server
    srv = Server(tkinter_q, srv_pop)

    try:
        srv.init()
    except Exception as err:
        # Get the full error trace
        full_error = lof.create_error_trace()
        err_text = ("Initialization Problem " + srv.service + ": "
                    + type(err).__name__ + "\t" + str(err) + "\n\n"
                    + "============= Full Error Description =============\n"
                    + full_error
                    + "============= Full Error Description =============\n")
        tkinter_q.put(err_text)
        # Enable the close Button of the Popup
        tkinter_q.put('CLOSE')
        
        # Tkinter Window
        srv_pop.show_logging()
    else:
        srv.run()

        
  

           
