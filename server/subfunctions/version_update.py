
###############################################################################
# IMPORTS
############################################################################### 

# Get access to cmd and paths
import subprocess
import os
import sys

# Time related
import time

# Scriptlib
import file_functions as ffu

###############################################################################
# FUNCTIONS
###############################################################################

"""--------------------------------- GIT ---------------------------------""" 

def check_git_exists(update_allowed):
    """Check if git is installed on the pc"""
    if update_allowed:
        try:
            subprocess.check_output(['git', '--version'])
        except OSError:
            return False
        else:
            return True
    return False

def check_git_subfolder(path):
    """Check current folder if subfolder '.git' exists"""
    git_subfolder_path = os.path.join(path, '.git')
    return os.path.isdir(git_subfolder_path)

def git_fetch_dry_run(path):
    """Check if new branches and tags exist"""
    # Run the git fetch --dry-run command
    branch_tags = subprocess.check_output(
        ['git', 'fetch', '--dry-run'],
        cwd=path,
        stderr=subprocess.STDOUT).decode().strip()
    return branch_tags

def git_fetch_tags(path):
    """Get existing tags of master/main branch locally on pc"""
    tags = subprocess.check_output(
        ['git', 'tag'],
        cwd=path,
        stderr=subprocess.STDOUT).decode()
    return tags.split('\n')[:-1]
    

def check_for_git(path):
    """Ckeck if remote tags (versions) are newer"""
    branch_tags = git_fetch_dry_run(path)
    branch_tags = branch_tags.split('\n')
    versions = {'Local':git_fetch_tags(path), 'Remote':[]}
    # Fetch remote tags
    for line in branch_tags:
        if 'new tag' in line:
            new_tag = line.split(' ')[-1]
            if new_tag not in versions['Remote']:
                versions['Remote'].append(new_tag)
    return versions


"""-------------------------------- VERSION --------------------------------""" 
  
def update_needed(current_version, versions_list):
    """Check if update of module is needed (compare versions)"""
    curr_vers_split = current_version.split('.')
    for version in versions_list:
        vers_split = version[1:].split('.')
        for i in range(len(vers_split)):
            if int(vers_split[i]) > int(curr_vers_split[i]):
                return version
            elif int(vers_split[i]) < int(curr_vers_split[i]):
                # Break if current version is newer?
                break
         
def get_current_version(path):
    """Extract current version from version.txt file"""
    version_file = os.path.join(path, 'version.txt')
    if os.path.exists(version_file):
        return open(version_file, 'r').read().replace('\n','')
         
def get_versions(server_path, setup_file):
    """Get versions and paths of each module + server"""
    versions = {'Versions':{}, 'Paths':{}}
    modules_path = os.path.join(os.path.dirname(server_path), 'modules')
    # Check server version and path
    server_version = get_current_version(server_path)
    if server_version == None:
        raise Exception('No update version')
    versions['Versions']['SERVER'] = server_version
    versions['Paths']['SERVER'] = server_path
    # Check active modules version and path
    setup_config = ffu.config_file_reader(setup_file)
    for group in setup_config:
        for series in setup_config[group]:
            partial_path = setup_config[group][series]['path']
            path = os.path.join(modules_path, group, partial_path)
            module_version = get_current_version(path)
            versions['Paths'][series] = path
            versions['Versions'][series] = module_version
    return versions
    
def check_for_updates(current_versions):
    """Check for each module if git path exisits and need to be updated"""
    new_versions = {}
    for module_id in current_versions['Paths']:
        path = current_versions['Paths'][module_id]
        version = current_versions['Versions'][module_id]
        try:
            if check_git_subfolder(path) and version != None:
                # Check if module has a git path
                versions_list = check_for_git(path)
                # check if update is needed
                new_version = update_needed(version, versions_list['Remote'])
                if new_version != None:
                    new_versions[module_id] = new_version[1:]
        except Exception:
            pass 
    return new_versions    

"""-------------------------------- UPDATE --------------------------------"""

def check_branch(path):
    """Get active branch. Only update if module is in main/master branch"""
    branches = subprocess.check_output(['git', 'branch'], 
                                       cwd=path).decode().split('\n')
    for br in branches:
        if '*' in br:
            branch_name = br.replace('*','').strip()
            # Only master of main branch
            if branch_name in ['master', 'main']:
                return branch_name
            else:
                raise Exception("Wrong branch selected. Cannot update if "
                                + "master/main branch has not been selected.")

def git_pull_branch(path, branch):
    """Pull 'branch' from git"""
    process = subprocess.Popen(['git', 'pull', 'origin', branch], 
                               cwd=path, 
                               stdout=subprocess.PIPE,
                               stderr=subprocess.STDOUT)
    process.wait()

def update_software(to_update, current_versions):
    """Main function to update the modules"""
    old_versions = {}
    
    # Check which module needs to updated
    for module in current_versions['Versions']:
        if module in to_update:
            if to_update[module]:
                # print('Fetch update from git')
                path = current_versions['Paths'][module]
                # Get active branch
                branch = check_branch(path)
                # Pull branch from git
                git_pull_branch(path, branch)
                # print('Check if all packages for this module are installed')
                install_python_packages(path, 'requirements.txt')
                # print('Successfully updated ' + module)
                
        old_versions[module] = current_versions['Versions'][module]
        
    # Save the numbers of the previous used version of each module
    server_path = current_versions['Paths']['SERVER']
    main_path = os.path.dirname(server_path)
    version_file = os.path.join(main_path, 'configs', 'last_versions.yaml')
    ffu.write_yaml_file(version_file, old_versions)
    # print('Check if all packages are installed')
    install_python_packages(main_path, 'requirements.txt')
    # print('Successfully updated')
            
def pip_install(package): 
    """Install defined package"""
    subprocess.check_call([sys.executable,
                        "-m", "pip", "install",
                        package])

def install_python_packages(filename):
    """Read ASCII like file with required packages and install them"""
    with open(filename, 'r') as f:
        packages = f.read().split('\n')
    for p in packages:
        if "#" not in p and p != '':
            # print('Install Package: ' + p)
            pip_install(p)
            # print('Installation of package completed')
            time.sleep(2)
