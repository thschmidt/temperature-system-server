# -*- coding: utf-8 -*-

###############################################################################
# IMPORTS
###############################################################################

# File and Path related
import os

# Regular expression
import re

# For Copying of content of variables
import copy

# scriptlib
import file_functions as ffu

###############################################################################
# CLASSES
###############################################################################

class SetupCheck:
    def __init__(self, config_file_directory, module_file_directory):
        self.config_file_directory = config_file_directory
        self.module_file_directory = module_file_directory
        
        # Save all device ids to check for identical device ids
        self.device_id_dict = {}
    
    """--------------------------- NOMENCLATURE ---------------------------"""    
    
    def confirm_nomenclature(self, input_id, id_type="series"):
        """
        Confirm the nomenclature of the device and series id
        """
        self.check_alphanumeric(input_id, id_type)
        self.check_lowercase(input_id, id_type)
        self.check_string_length(input_id, id_type) 
        
    def check_alphanumeric(self, input_id, id_type):
        """
        Check if the id of series or device contains only chars like 'a-z'
        or numbers
        """
        if not self.is_alphanumeric(input_id):
            raise Exception("The " + id_type + "-id '" + input_id
                            + "' is not allowed as ID. "
                            + "Only Numbers and characters "
                            + "are allowed.")
    
    def check_lowercase(self, input_id, id_type):
        """Check if the if of series or device is written in lowercase"""
        for char in input_id:
            if char.isalpha() and not char.islower():
                raise Exception("The " + id_type + "-id '" + input_id
                                + "' must be lowercase")

    def check_string_length(self, input_id, id_type):
        """
        Check if the id of series or device isn't longer than 7 or
        shorter than 4 chars
        """
        if len(input_id) > 7:
            raise Exception("The " + id_type + "-id '" + input_id
                            + "' is too long (maximum 7 chars)")
        elif len(input_id) < 4:
            raise Exception("The " + id_type + "-id '" + input_id 
                            + "' is too short (minimum 4 chars)")
            
    def is_alphanumeric(self, input_string):
        # Define a regular expression pattern that matches only alphanumeric characters
        pattern = re.compile(r'^[a-zA-Z0-9]+$')
        # Use the search function to find if the pattern matches the input string
        match = re.search(pattern, input_string)
        # If match is found, return True, otherwise return False
        return bool(match)        
    
    """------------------------- MULTIPLE MENTION -------------------------""" 
    
    def check_device_id_exists(self, device_id, series):
        """Check if device id has already been used by a other series"""
        if device_id not in self.device_id_dict.keys():
            self.device_id_dict[device_id] = series
        else:
            raise Exception("The device_id '"  + device_id + 
                            "' of '" + series + "' is already used in "
                            + self.device_id_dict[device_id])
    
    """-------------------------- PATHS AND FILES --------------------------""" 
    
    def check_paths(self, paths_dict):
        """Check if all file / logging etc. paths exist"""
        for k1,v1 in paths_dict.items():
            if v1 == '' or v1 == None:
                v1 = (str(os.getcwd()[:3]) + r'LOGS/Temperature_System/'
                      + k1 + 'log')
                ffu.check_folder(v1)
            if not os.path.exists(v1):
                raise Exception("File or Folder does not exist: " + str(v1))
            paths_dict[k1] = v1
        return paths_dict
    

    def check_configs_exists(self, path, config_file_list):
        """Check if all initial setup configuration files exist"""
        for filename in config_file_list:
            file = os.path.join(path, filename)
            if not os.path.exists(file):
                raise FileNotFoundError(file + " Does Not Exist")
    
    def check_setup_yaml(self, module_path):
        """
        Readout details from setup configuration file and create the process list. 
        Only if other modules exist, set the parameter 'modules_exist' as True
        """
        setup_device_keys = ['path', 'script', 'class']
        
        modules_exist = False
        # Readout setup.yaml
        setup_data = ffu.config_file_reader(os.path.join(module_path,
                                                         'modules_setup.yaml'))
        
        # Create Pocess list
        module_processes = []
        for group in setup_data:
            if setup_data[group] is not None:
                module_processes += list(setup_data[group].keys())
                for series in setup_data[group]:
                    for key in setup_device_keys:
                        if key not in setup_data[group][series]:
                            raise KeyError("In " + series
                                           + " the key '" 
                                           + key 
                                           + "' is missing")
                # Set modules parameter True
                # => Access to controller and work page
                modules_exist = True
                
        return modules_exist, module_processes
    
    
    def convert_config_content(self, config_content):
        converted_config = {}
        for section in config_content:
            converted_config[section] = {}
            if config_content[section] is not None:
                for key in config_content[section]:
                    converted_config[section][key] = config_content[section][key]['VALUE']
        return converted_config
    
    """---------------------------- CONNECTION ----------------------------""" 
    
    def check_connection_valid(self, series_config, device_id):
        """Check if connection is marked as valid"""
        connection = series_config['ID'][device_id]['Connection']
        if connection['Type'] in connection['valid']:
            return True
        return False
        


class InitialSetup(SetupCheck):
    def __init__(self, config_file_directory, module_file_directory): 
        super().__init__(config_file_directory=config_file_directory,
                         module_file_directory=module_file_directory)
         
    def load_settings(self, dummy):
        """
        Load different configuration files and put their data together
        in the settings dictionary. The settings parameter will contain
        all the initial and current data of the different services (series)
        """
        conf_files = ffu.get_filenames(self.config_file_directory)
        mod_files = ffu.get_filenames(self.module_file_directory)
        # Basic Categories
        settings = {'data_logger':{}, 'temp_control':{},
                    'campaign':{}, 'work':{},
                    'config':{}, 'paths':{}, 'website':{},
                    'messenger':{}} 

        group_modules = {}
        # Loop over filenames
        for fn in mod_files:
            header = os.path.basename(fn).split('.')[0]
            
            if header == 'modules_setup':
                setup_data = ffu.config_file_reader(fn)
                for group in setup_data:
                    if setup_data[group] is not None:
                        group_modules[group] = list(setup_data[group].keys())
        
        for fn in conf_files:
            header = os.path.basename(fn).split('.')[0]
            
            # Readout each file in the config folder and add it to the settings dictionary
            if header in settings:
                if header in ['config', 'paths', 'website']:
                    cfg_content = self.convert_config_content(ffu.config_file_reader(fn))
                    if header == 'paths':
                        settings[header] = self.check_paths(cfg_content['LOGS'])
                        settings['campaign'] = self.read_campaign_data(settings[header]['campaign'])
                        settings['work'] = self.read_work_data(settings[header]['work'])
                    else:
                        settings[header] = cfg_content
                elif header in ['campaign', 'work']:
                    pass
                else:
                    settings[header] = ffu.config_file_reader(fn)

        # Fetch the path of the configuration files of the messengers
        # CURRENTLY ONLY EMAIL IS SUPPORTED NO EXTERAL MESSENGER SCRIPTS
        messenger_config_path = os.path.join(self.config_file_directory,
                                             'messengers_yaml')
        
        email_path = os.path.join(messenger_config_path, 'email.yaml')
        email_cfg = ffu.config_file_reader(email_path)
        settings['messenger']['email'] = self.convert_config_content(email_cfg)
        
        # Fetch the path of the configuration files of the modules
        module_config_path = os.path.join(self.config_file_directory,
                                         'modules_yaml')
        for group in group_modules:
            for series in group_modules[group]:
                # Check if series id length and for allowed chars
                self.confirm_nomenclature(series)
                    
                # Fetch the configuration data of the series
                fn = os.path.join(module_config_path, series + '.yaml')
                series_config = ffu.config_file_reader(fn)  
                

                # Loop over every device id of the series
                for device_id in series_config['ID'].copy():
                    if (self.check_connection_valid(series_config, device_id)
                                                    or dummy):
                        # Check if device id length and for allowed chars
                        self.confirm_nomenclature(device_id, 'device')
                        
                        # Check if device id already exist in different series
                        self.check_device_id_exists(device_id, series)
                        
                        # Add general configs to individual 
                        series_config = self.add_general_config(series_config,
                                                                device_id)
                        
                        if group == 'temp_control':
                            series_config = self.add_parameters(device_id, 
                                                                series_config)
                        # Add additional parameters to settings of the module
                        additional_fields = {'LastReadout': '',
                                             'LastConnection':'',
                                             'State': 'Offline'}
                        additional_fields['SensorData'] = {}
                        
                        series_config['ID'][device_id].update(additional_fields)
                    else:
                        del series_config['ID'][device_id]
                settings[group][series] = series_config     
        
        return settings
    
    def read_campaign_data(self, campaign_folder):
        """
        Fetch all campaign names and return the details of the last active campaign
        """
        campaigns_file = os.path.join(campaign_folder, 'campaigns.txt')
        if os.path.exists(campaigns_file):
            campaign_ids = ffu.read_ascii_file(campaigns_file)[:-1] 
            campaign_ids.reverse()
            campaign_names = []
            for campaign in campaign_ids:
                campaign_yaml = os.path.join(campaign_folder, campaign
                                             + '/campaign_details.yaml')
                campaign_details = ffu.config_file_reader(campaign_yaml)
                campaign_names.append(campaign_details['Name'])
            try:
                campaign_data = {'CampaignLists': [campaign_ids, campaign_names],
                                 'CurrentCampaign': campaign_ids[0], 
                                 'Active':False,
                                 'LastReadout':''}
            except IndexError:
                campaign_data = {'CampaignLists':[[],[]], 'CurrentCampaign':'',
                                 'Active':False, 'LastReadout':''}
        else:
            with open(campaigns_file, 'w'):
                pass
            campaign_data = {'CampaignLists':[[],[]], 'CurrentCampaign':'',
                             'Active':False, 'LastReadout':''}
        return campaign_data
        

    def read_work_data(self, work_folder):
        """
        Fetch all work parts and return the details of the last active work
        """
        work_file = os.path.join(work_folder, 'work.txt')
        
        work_data = {'WorkLists':[[],[]], 'CurrentWork':'',
                     'CurrentTask':'', 'Active':False,
                     'StartedTask':'', 'Iteration':'',
                     'ConditionVariable':{}}
        
        if os.path.exists(work_file):
            work_ids = ffu.read_ascii_file(work_file)[:-1] 
            work_ids.reverse()
            work_names = []
            for work_id in work_ids:
                work_yaml = os.path.join(work_folder, work_id + '.yaml')
                work_details = ffu.config_file_reader(work_yaml)
                work_names.append(work_details['Name'])
            try:
                work_data['WorkLists'] =  [work_ids, work_names]
                work_data['CurrentWork'] =  work_ids[0]
            except IndexError:
                pass
                
        else:
            with open(work_file, 'w'):
                pass
        return work_data


    def add_parameters(self, key, settings):
        """
        Add some default paramters and its values for Read and/or Set
        (depending on the InputType, Switch etc.) to the settings parameter. 
        """
        device_data = settings['ID'][key]
        for channel_name in device_data['Channel']:
            channel = device_data['Channel'][channel_name]
            for parameter_name in channel:
                parameter = channel[parameter_name]
                if 'InputType' in parameter:
                    input_type = parameter['InputType']
                    if input_type == 'String':   
                        parameter['Set'] = ''
                        parameter['Read'] = ''
                    elif input_type in ['Float', 'Int']:
                        parameter['Set'] = 0
                        parameter['Read'] = 0
                    elif input_type == 'Switch':   
                        if 'Values' in parameter:
                            parameter['Set'] = parameter['Values'][0]
                        else:
                            parameter['Set'] = 0
                    elif input_type == 'Select':
                        parameter['Set'] = parameter['Values'][0]
                        parameter['Read'] = parameter['Values'][0]
                    elif input_type == 'Button':
                        parameter['Set'] = 0
                    else:
                        raise Exception("InputType is not known in "
                                        + key + ", Channel: "
                                        + channel_name + ", Parameter: "
                                        + parameter_name)
                else:
                    parameter['Read'] = ''
                device_data['Channel'][channel_name][parameter_name] = parameter
        settings['ID'][key] = device_data 
        return settings
    
    def add_general_config(self, series_config, device_id):
        # Define config parameter
        device_config = series_config['ID'][device_id]
        general_config = series_config['GENERAL']
        #
        if 'Channel' in general_config:
            for ch_num in device_config['Channel']:
                # Needs to be copied to prevent "Variable id" conflict
                general_channel_copy = copy.deepcopy(general_config['Channel'])
                if device_config['Channel'][ch_num] in [{}, None]:
                    new_config = general_channel_copy
                else:
                    new_config = self.merge_dictionaries(general_channel_copy,
                                                         device_config['Channel'][ch_num])
                device_config['Channel'][ch_num] = new_config

        if 'Parameter' in general_config:
            # Needs to be copied to prevent "Variable id" conflict
            general_parameter_copy = copy.deepcopy(general_config['Parameter'])
            all_parameter = self.merge_dictionaries(general_parameter_copy, 
                                                    device_config)
            series_config['ID'][device_id] = all_parameter
        return series_config
    
    def merge_dictionaries(self, default, individual):
        """
        Merge two dictionaries, individual vales overwrite or added to the default one
        """
        result = default.copy()  # Start with a copy of the default dictionary
        for key, value in individual.items():
            if (key in result and isinstance(result[key], dict)
                and isinstance(value, dict)):
                # If both the current value in result and the new value are 
                # dictionaries, merge them recursively
                result[key] = self.merge_dictionaries(result[key], value)
            else:
                # Otherwise, overwrite the value in the result with the value
                # from the individual dictionary
                result[key] = value

        return result
                        
