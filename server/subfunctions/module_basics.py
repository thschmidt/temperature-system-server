# -*- coding: utf-8 -*-

from datetime import datetime as dt



class ModuleFunctions:
    def __init__(self, instance='', thread_instance=''):
        self._instance = instance
        if thread_instance != '':
            self._thread_instance = thread_instance
            self._device_id = thread_instance.device_id
         
    def create_timestamp(self, milliseconds=False):
        """
        Return the current time as a formated timestamp
        Default format: yyyy-mm-ddThh:mm:ss
        """
        if milliseconds:
            return dt.utcnow().strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3]
        else:
            return dt.utcnow().strftime('%Y-%m-%dT%H:%M:%S')
              
    def create_event_message(self, msg_text):
        """
        Creates the event message parameters, logs it
        and sends it to the main server.
        The input of msg_text can be a standard string, Exception or Warning
        Example: msg_text = Exception("Some Text")
        """
        if self.instance != '':
            # Does not work during setup !!
            if isinstance(msg_text, Exception) or isinstance(msg_text, Warning):
                self._instance.create_event_log_package(self._instance.service, 
                                                        exception=msg_text,
                                                        device_id=self._device_id)
            elif isinstance(msg_text, str):
                self._instance.create_event_log_package(self._instance.service, 
                                                        event_message=msg_text,
                                                        device_id=self._device_id)
            else:
                raise Exception("The message input must be a String / Exception"
                                + " / Warning, not " + type(msg_text))
            
    def convert_to_string(self, parameter_dict, value):
        """
        Convert a float or integer into a string with the defined decimals.
        parameter_dict looks like: {'Set': ..., 'Read': ..., 'Decimals': ...}
        """
        if 'Decimals' in parameter_dict:
            decimal = parameter_dict['Decimals']
            string_format = '{:.' + str(decimal) + 'f}'
            return string_format.format(value)
        else:
            return str(value)
     
    @property  
    def general_body(self):
        return self._thread_instance.general_body
    
    @property  
    def campaign_sensors(self):
        return self._thread_instance.campaign_sensors
        
