# -*- coding: utf-8 -*-

"""
CONFIG File and Folder - file_functions.py
EDITOR: Thomas Schmidt
EMAIL: tschmidt@mpe.mpg.de
"""

###############################################################################
# IMPORTS
###############################################################################

# Path / File realted
import os
from pathlib import Path
import glob
from shutil import copyfile

# File type related
import yaml
import json
import configparser

###############################################################################
# FUNCTIONS
###############################################################################

"""------------------------------ CONFIG FILES -----------------------------"""
        
def config_file_reader(filename):
    """General file reader, 
    input: filename as raw string, 
    return: file data as dictionary or an exception"""
    if os.path.exists(filename):
        if filename.endswith('.json'):
            data_dict = read_json_file(filename)
        elif filename.endswith('.ini'):
            data_dict = read_ini_file(filename)
        elif filename.endswith('.yaml'):
            data_dict = read_yaml_file(filename)
        else:
            return("Unknown File Format")
        return data_dict
    else:
        raise FileNotFoundError (filename + " Does Not Exist")
   
# Read Functions    
def read_ini_file(config_filename, remove_default=True, case_insensitive=False):
    """Readout ini file, 
    input: ini filename as raw string, remove the automatically created default key,
    case_insensitive == true converts keys to lower case
    return: file data as dictionary"""
    config = configparser.ConfigParser()
    if not case_insensitive:
        config.optionxform = str
    config.read(config_filename)
    config_dict = {}
    for section in config:
        config_dict[section] = {}
        for parameter in config[section]:
            config_dict[section][parameter] = config[section][parameter]
    if remove_default:
        del config_dict['DEFAULT']
    return config_dict

def read_json_file(json_filename):
    """Readout json file, 
    input: raw filename string, 
    return: file data as dictionary"""
    with open(json_filename, 'r') as f:
        json_dict = json.load(f)
    return json_dict

def read_yaml_file(yaml_filename):
    """Readout yaml file, 
    input: raw filename string, 
    return: file data as dictionary"""
    with open(yaml_filename, 'r', encoding='utf-8') as f:
        data = yaml.safe_load(f)
    return data

# Write Functions
def write_ini_file(config_filename, data, case_insensitive=False):
    """Write ini file, 
    input: config filename as raw string, data to save in file as dictionary,
    case_insensitive == true converts keys to lower case"""
    config = configparser.ConfigParser()
    if not case_insensitive:
        config.optionxform = str
    config.read(config_filename)
    config.read_dict(data)
    with open(config_filename , 'w') as f:
        config.write(f)
        
def write_json_file(json_filename, data):
    """Write json file, 
    input: json filename as raw string, data to save in file as dictionary"""
    with open(json_filename , 'w') as f:
        json.dump(data, f, indent=4)
    
def write_yaml_file(yaml_filename, data, sort_keys=False):
    """Write yaml file, 
    input: yaml filename as raw string, data to save in file as dictionary"""
    with open(yaml_filename, 'w') as f:
        yaml.safe_dump(data, f,
                       default_flow_style=False,
                       indent=4, sort_keys=sort_keys)

    
"""---------------------------- RAW ASCII FILES ---------------------------"""

def append_ascii_file(filename, line):
    """APPEND ASCII CHARACTERS TO FILE,
    INPUT: FILENAME AS RAW STRING, DATA AS STRING""" 
    if os.path.exists(filename):   
        with open(filename , 'a+') as f:
            f.write(line)
    else:
        create_ascii_file(filename, line)
    
def create_ascii_file(filename, line):
    """Append ascii characters to file,
    input: filename as raw string, data as string""" 
    with open(filename , 'w') as f:
        f.write(line)
        
def read_ascii_file(filename, newline=True, delimiter='', cols=True, ncols=0):
    """Read raw ascii file,
    input:  filename as raw string, newline seperation boolean,
            line delimiter as string, create row or column lists
            and number of cols (only needed if number of first line of file != cols needed)
    retutrn: data as string or list(s in list)""" 
    with open(filename , 'r') as f:
        string_data = f.read()
        if newline:
            # Split data every new line
            line_data = string_data.split('\n')
            if delimiter != '':
                # Split every line with seperator
                if cols:
                    # Return data as column lists
                    if ncols == 0:
                        # Use first line to determine number of cols needed
                        ncols = len(line_data[0].split(delimiter))
                    col_list = [[] for _ in range(ncols)]
                    for l in line_data:
                        if l != '':
                            lsp = l.split(delimiter)
                            for i in range(ncols):
                                col_list[i].append(lsp[i])   
                    return col_list
                else:
                    # Return data as row lists
                    row_list = []
                    for l in line_data:
                        row_list.append(l.split(delimiter))  
                    return row_list
            else:
                return line_data
        else:
            # Return data as string
            return string_data
    
"""------------------------ CHECK FILES AND FOLDERS -----------------------"""    

def get_filenames(folder, ending="*", fullpath=True):
    """Show all filenames in folder,
    input: folder as raw string, ending of files and return as full path 
    return: list of filenames"""
    filenames = glob.glob(os.path.join(folder,'*.' + ending))
    if fullpath:
        return filenames
    else:
        return [str(elem).split('\\')[-1] for elem in filenames]

def check_folder(folder):
    """Check existens of folder and create not existing folders,
    input: folder as raw string"""
    Path(folder).mkdir(parents=True, exist_ok=True)  

def copy_file(directory, folder, fn):
    """Copy file to destination if it exists"""
    if not os.path.exists(os.path.join(folder, fn)): 
        copyfile(os.path.join(directory, fn) , os.path.join(folder, fn))   
