# -*- coding: utf-8 -*-
"""
logging_functions.py
EDITOR: Thomas Schmidt
EMAIL: tschmidt@mpe.mpg.de
"""

###############################################################################
# IMPORTS
###############################################################################

# time related
from datetime import datetime

# 
import os
import sys, traceback

# scriptlib
import file_functions as ffu

###############################################################################
# FUNCTIONS
###############################################################################

def create_error_trace():
    """Return the full trace information of the error"""
    exc = sys.exc_info()[0]
    # last one would be full_stack()
    stack = traceback.extract_stack()[:-1]  
    if exc is not None:  # i.e. an exception is present
        # remove call of full_stack, the printed exception
        # will contain the caught exception caller instead
        del stack[-1]       
    trc = 'Traceback (most recent call last):\n'
    if exc is not None:
          trc += '  ' + traceback.format_exc().lstrip(trc)
    return trc

def default_logger(logpath, filename, text, module='', tag='',
                  delimiter='\t', ending='.log', show=False, miliseconds=True):
    """Logs text in ascii-like file with chosen filename and path. 
    File ending can be adjusted. Timestamps are given in utc,
    precision can be given in miliseconds or seconds (default).
    The module is an additional information in which script / function / etc.
    the log was created"""
    # Check if dot is missing in the file ending
    if '.' not in ending:
        ending = '.' + ending 
    # Check if log folder exists, if not create folders   
    ffu.check_folder(logpath) 
    filen = os.path.join(logpath, filename) + ending
    # If module is empty only log the text
    date = datetime.utcnow()
    if miliseconds:
        timestamp = date.strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3]
    else:
        timestamp = date.strftime('%Y-%m-%dT%H:%M:%S')
    line = timestamp
    for element in [module, tag, text]:
        if element != '':
            line += delimiter + element
    if show:
        print(line)
    # Append log to file or create a new one
    ffu.append_ascii_file(filen, line + '\n') 
    return line
    
  
