# -*- coding: utf-8 -*-
"""
process_thread.py
EDITOR: Thomas Schmidt
EMAIL: tschmidt@mpe.mpg.de
"""

###############################################################################
# IMPORTS
###############################################################################

import multiprocessing as mp
import queue

###############################################################################
# PROCESS / THREADS
###############################################################################

def close_process_thread_queues(queues, sgl='STOP'):
    """Stops threads or processes using queue signal"""
    for q in queues.values():
        q.put(sgl)    

def start_process_thread(prothr):
    """
    Start threads or processes.
    input either as dictionary or list of threads
    """
    if isinstance(prothr, dict):
        iprothr = prothr.values()
    elif isinstance(prothr, list):
        iprothr= prothr
    else:
        iprothr = []  
    for pt in iprothr:
        pt.start()
       
###############################################################################
# THREADING
###############################################################################   

def close_thread_on_signal(threads, sgl_var='do_run'):
    """
    Stop threads using signal variable, 
    only works if thread includes: while getattr(threading.currentthread, sgl_var, true).
    input either as dictionary or list of threads
    """
    if isinstance(threads, dict):
        ithreads = threads.values()
    elif isinstance(threads, list):
        ithreads = threads
    else:
        ithreads = []  
    for t in ithreads:
        setattr(t, sgl_var, False)
        t.join()
        
 
###############################################################################
# QUEUES FUNCTIONS
###############################################################################  

def clear_queue(queue):
    """Clear queue"""
    while not queue.empty():
        queue.get()
       

def create_queues(queue_list, typ='mp', maxsize=50):
    """Creates a dictionary including queues using multprocessing or queue"""
    queues = {}
    for process in queue_list:
        if typ=='mp':
            queues[process] = mp.Queue(maxsize=maxsize)
        else:
            queues[process] = queue.Queue()
    return queues

        

