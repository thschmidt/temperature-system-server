
###############################################################################
# IMPORTS
############################################################################### 

import keyring
from smtplib import SMTP
from email.mime.text import MIMEText

###############################################################################
# CLASS
###############################################################################

class EmailBox:
    def __init__(self):
        self.email = ''
        self.user = ''
        self.smtp_server = ''
        self.smtp_port = ''
        
        self.checked = False
        
    def set_email_password(self, password):
        keyring.set_password("TeSe","Email", password)
        
    def setup_messenger(self, parameters):
        """
        Save the configuration of the email and
        verify the connection to the smtp server
        """
        sender = parameters['SENDER']
        self.smtp_server = sender['smtp_server']
        self.smtp_port = sender['smtp_port']
        
        self.email = sender['address']
        password = sender['password']

        self.user = self.email.split('@')[0]
        
        if password != '*****':
            self.set_email_password(password)
			
        self.verify_email(password)
			
        self.checked = True
            
    def verify_email(self, password):
        """
        Verify the email and credentials by login on the smtp server
        """
        server = SMTP(self.smtp_server, self.smtp_port)
        
        # Get password
        if password == '*****':
            password = keyring.get_password("TeSe","Email")

        # Try to login with credentials
        server.login(self.user, password)

        server.quit()

    def send_message(self, receiver_list, message, subject='INFO'):
        """
        Sending email to the recipients in the receiver_list. 
        Only works if email has been verified.
        """
        if (receiver_list != [] and self.checked): 
            # Get password
            password = keyring.get_password("TeSe","Email")
            
            # Create subject, shown name and message
            msg = MIMEText(message)
    
            msg['Subject'] = subject
            msg['From'] = "Temperature System"
            
            # Add all recipients
            recipient = ""
            for receiver in receiver_list:
                recipient += receiver + ", "
            msg['To'] = recipient[:-2]
            
            # Send Email
            with SMTP(self.smtp_server, self.smtp_port) as server:     
                server.login(self.user, password)
                server.sendmail(self.email, receiver_list, msg.as_string())

                