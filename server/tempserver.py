
###############################################################################
# IMPORTS
############################################################################### 

# Webpage
import cherrypy

# Time and clock related
from datetime import timedelta
from datetime import datetime as dt
import time

# Processes
import multiprocessing as mp

# Scriptlib
import process_thread as pat

# Subfunctions
import init_modules as inm

# Package
from package_broker import PackageHandler

###############################################################################
# CONSTANTS
###############################################################################

SENDER = 'TempSrv'

###############################################################################
# CLASSES
###############################################################################

class TempServer(PackageHandler):
    def __init__(self, queues, service_list, settings, dummymode):
        super().__init__(service=SENDER, queues=queues, settings=settings)
        
        self.service_list = service_list
        
        self.init_settings = settings.copy()
        
        self.dummymode = dummymode
        
        self.service_events = {}
        self.event_msg_limit = 20
        self.event_spam = False
        
        self.initialize_services()
        

    """-------------------- PROCESS / SERVICE FUNCTIONS --------------------"""    
    
    def create_service(self, service_name):
        """
        Create service (Python Process) with 'service_name'
        and return the process.
        """
        return mp.Process(target=start_service,
                          args=(service_name, self.service_list, 
                                self.send_q, self.recv_qs[service_name],
                                self.init_settings, self.dummymode))
    
    def initialize_services(self):
        """Initialisation and starting of the all services (Python Process)"""
        self.service_dict = {}
        for p in self.service_list:
            self.service_dict[p] = self.create_service(p)
            self.service_dict[p].start()
        
    def change_service_status(self, task):
        """Start, Stop or Restart a selected service (Python Process)"""
        pname = task['Name']       
        if (task['Event'] == 'Start' and
            not self.service_dict[pname].is_alive()):
            # Clear queue
            pat.clear_queue(self.recv_qs[pname])
            self.reset_device_status()
            # Create a new process for the service and start it
            self.service_dict[pname] = self.create_service(pname)
            self.service_dict[pname].start()
        elif task['Event'] == 'Restart':
            # Send and check if service has been stopped
            self.check_stop_procedure(pname)
            self.reset_device_status()
            # Create a new process for the service and start it
            self.service_dict[pname] = self.create_service(pname)
            self.service_dict[pname].start()
        elif task['Event'] == 'Stop' and self.service_dict[pname].is_alive():
            # Send and check if service has been stopped
            self.check_stop_procedure(pname)
    
    def check_stop_procedure(self, pname, shutdown=False, wait=3):
        """Check if service stops within 3 sec, else terminate its process"""
        waited = 0
        # Check if service alive
        while self.service_dict[pname].is_alive():
            if waited == 0:
                self.recv_qs[pname].put('STOP')
            time.sleep(0.05) 
            waited += 0.05
            if waited > wait:
                self.terminate_service(pname, shutdown=shutdown)
                break
        # Set the service status false and send it to the CAT
        self.services[pname]['Status'] = False
        package = self.create_package(SENDER,
                                      receiver='CAT', 
                                      cmdtype='SET',
                                      data={'STATE':False,
                                            'SERVICE':pname})
        self.send_q.put(package)
        time.sleep(0.05)
        pat.clear_queue(self.recv_qs[pname])
      
    def get_service_status(self, services={}):
        """
        Get current status of the service and
        update its status in the 'services' variable and send it to the CAT
        """
        if services == {}:
            for p in self.service_dict:
                services[p] = {'Status':self.service_dict[p].is_alive()}
        else:
            for p in self.service_dict:
                state = self.service_dict[p].is_alive()
                if state != services[p]['Status']:
                    # Send the status update to the CAT
                    package = self.create_package(SENDER,
                                                  receiver='CAT', 
                                                  cmdtype='SET',
                                                  data={'STATE':state,
                                                        'SERVICE':p})
                    self.send_q.put(package)
                services[p]['Status'] = state
        # Publish the status of the 'services' in the cherrypy engine
        cherrypy.engine.publish('service', services)
        return services
            
    def terminate_service(self, pname, shutdown=False):  
        """Terminate the service ('pname')"""
        self.service_dict[pname].terminate()
        # Create an event message if terminated not during shutdown
        if not shutdown:
            mtext = 'Killed Service ' + pname
            self.create_event_log_package(SENDER, event_message=mtext)
            
    def reset_device_status(self):
        """Reset device status on service restart / start"""
        for group in ['data_logger', 'temp_control']:
            for series in self.init_settings[group]:
                for device_id in self.init_settings[group][series]['ID']:
                    device = self.init_settings[group][series]['ID'][device_id]
                    device['State'] = 'Offline'
                    # Not needed
                    self.init_settings[group][series]['ID'][device_id] = device
        
    """-------------------------- DATA FUNCTIONS --------------------------"""
    
    def update_data(self, msg):
        """Compare previous settings and new settings after data update"""
        changes = False
        new_settings = self.settings.copy()
        for group in ['data_logger', 'temp_control']:
            try:
                series = msg['HEADER']['FROM']
                package_data = msg['BODY']['DATA']
                if series in self.settings[group]:
                    clt = package_data[group][series]
                    slt = self.settings[group][series]
                    if clt != slt:
                        new_settings[group][series] = clt
                        changes = True
            except KeyError:
                # Just in case ... old?
                pass
        # No changes -> no updated data is sent to the logger service
        return new_settings, changes
    
    def update_campaign_task(self, package_data):
        """Update work or campaign parameters"""
        for key in ['campaign', 'work']:
            if key in package_data:
                for subkey in self.settings[key]:
                    if subkey in package_data[key]:
                        self.settings[key][subkey] = package_data[key][subkey]
                
    def start_campaign(self, package_data):
        """Update the 'settings' parameter on start of campaign"""
        campaign_id = package_data['ID']
        campaign_name = package_data['NAME']
        self.settings['campaign']['Active'] = True
        self.settings['campaign']['CurrentCampaign'] = campaign_id
        self.settings['campaign']['CampaignLists'][0].insert(0, campaign_id)
        self.settings['campaign']['CampaignLists'][1].insert(0, campaign_name)
    
    def init_work(self, package_data):
        """Update the 'settings' parameter on stop of campaign"""
        work_id = package_data['ID']
        work_name = package_data['NAME']
        if work_id not in self.settings['work']['WorkLists'][0]:
            self.settings['work']['WorkLists'][0].insert(0, work_id)
            self.settings['work']['WorkLists'][1].insert(0, work_name)

        
    """-------------------------- EVENT FUNCTIONS --------------------------"""
    
    def update_service_events(self, msg):
        """
        Prepare the messages shown on the service event page. 
        Saves the individual logs for each service (max. self.event_msg_limit)
        """
        # Abbrevs
        header = msg['HEADER']
        origin = header['FROM']
        package_data = msg['BODY']['DATA']
        
        # Remove INIT key:
        package_data.pop('INIT')
            
        # Add service event to individual list for each service (max. 20)
        if origin in self.service_events:
            self.service_events[origin].append(package_data)
            if len(self.service_events[origin]) > self.event_msg_limit:
                self.service_events[origin].pop(0)
        else:
            self.service_events[origin] = [package_data]
         
        # Check for Event Spam
        if self.event_spam:
            self.event_spam_detection(origin)
            
        # Publish in service event
        cherrypy.engine.publish('service_event', self.service_events)
        
        # 
        packet = self.create_package(SENDER,
                                     receiver='Mssngr',
                                     data=package_data, 
                                     cmdtype='SET')
        self.send_q.put(packet)  
        
        
    def event_spam_detection(self, service):
        """
        Check if the last messages of the service are equal. 
        Stop the service in case of a persistent message sent by it
        """
        # Check the last 1/2 of the event message limit
        check_n_events = int(self.event_msg_limit/2)
        
        event_list = self.service_events[service]
        # Only check for spam if atleast five event messages
        # have been sent by the service
        if len(event_list) >= check_n_events:
            # Convert the string timestamps ([:-4] removes the milliseconds)
            last_timestamp = self.string_to_datetime(event_list[-1]['TIMESTAMP'][:-4])
            fth_timestamp = self.string_to_datetime(event_list[-check_n_events]['TIMESTAMP'][:-4])
            message = []
            for i in range(1, check_n_events+1):
                message.append(event_list[-i]['MESSAGE'])
            # Check if last six event messages are equal (last 5 seconds)
            if all(x == message[-1] for x in message):
                if last_timestamp < fth_timestamp + timedelta(seconds=check_n_events):
                    # Create event log message
                    mtext = ("Detected SPAM from service " + service
                             + ". Initialized shutdown of this service.")
                    self.create_event_log_package(SENDER, event_message=mtext)
                    
                    # Initialize stop of service
                    task = {'Name':service,'Event':'Stop'}
                    self.change_service_status(task)
                    
    
    def string_to_datetime(self, date_string):
        """Convert string timestamp into datetime timestamp"""
        return dt.strptime(date_string, '%Y-%m-%dT%H:%M:%S')


        
    """----------------------- RUN AND ITS FUNCTIONS -----------------------"""
    

class TempServerThread(TempServer) :
    def __init__(self, queues, service_list, settings, dummymode):
        super().__init__(queues, service_list, settings, dummymode)
        
        # Init campaign variables
        self.point = 0
        self.interval = 0
        self.campaign_active = False
        
        self.last_msgrefs = []
        
        # Create some timestamps
        self.date_start = dt.utcnow()
        self.date_last = dt.utcnow()
        self.now = dt.utcnow().strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3]
        self.last_msgrefs.append(self.now)
        
        # Service Variables
        self.services = self.get_service_status()

        for s in self.services:
            self.services[s].update({'Init':False,
                                     'LastMsg':self.now,
                                     'Received':self.now})
   
    def no_message(self):
        """
        Request data every second from the services and check their status
        """
        # Check service status
        self.services = self.get_service_status(self.services)
        
        # Request data from each service every second
        date_now = dt.utcnow()
        
        # readtype:
        # - REQUEST: Requests a readout from the device (temperature data)
        # - STATUS: Check for new data and/or request readout other data
        # - '': No readout requested
        readtype = ''
        
        # Get time difference between different events
        # (last readout or start of campaign)
        diff1 = date_now - self.date_start
        diff2 = date_now - self.date_last
        if (diff1 >= timedelta(seconds=self.interval*self.point)
            and self.campaign_active):
            #
            self.create_log_time('send')
            self.point += 1
            readtype = 'REQUEST'
        elif diff2 >= timedelta(seconds=0.5):
            # Create a readout request after half a second
            self.date_last = dt.utcnow()
            readtype = 'STATUS'
            
        # If a request has been created (a second has passed)
        if readtype != '':
            self.now = date_now.strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3]
            if readtype == 'REQUEST':
                self.settings['campaign']['LastReadout'] = self.now[:-4]
            # Save 20 last message references (timestamps), approx 10 sec
            if len(self.last_msgrefs) == 20:
                self.last_msgrefs.pop(0)
            self.last_msgrefs.append(self.now)
            # Get current process ids
            for serv in self.services:
                # Check if service is alive
                # -> Reset initialization status if not
                if not self.services[serv]['Status']:
                    self.services[serv]['Init'] = False
                # Check if service is alive and intialised
                # before requesting data
                if (self.services[serv]['Status'] and
                    self.services[serv]['Init']):
                    # Request only if old message has been returned
                    if readtype == 'REQUEST':
                        package = self.create_package(SENDER,
                                                      receiver=serv, 
                                                      cmdtype=readtype,
                                                      cmdref = self.now)
                        self.send_q.put(package)
                    elif self.services[serv]['Received'] == self.last_msgrefs[-2]: 
                        package = self.create_package(SENDER,
                                                      receiver=serv, 
                                                      cmdtype=readtype,
                                                      cmdref = self.now)
                        self.send_q.put(package)
        
        # Check if every service has been responed within 10 sec
        for serv in self.services:
            if self.services[serv]['Init']:
                if self.services[serv]['LastMsg'] not in self.last_msgrefs:
                    # If service not available
                    # Terminate and reset the service status 
                    self.create_event_log_package(serv,
                                                 exception=Exception(''
                                                     + 'Service is '
                                                     + 'not available.'))
                    self.services[serv]['Init'] = False
                    self.check_stop_procedure(serv, wait=0.06)
   
    def handle_message(self, msg):      
        # Abbrevs
        header = msg['HEADER']
        origin = header['FROM']
        cmdtype = header['CMDTYPE']
        package_data = msg['BODY']['DATA']
        # Cases
        if (cmdtype == 'SET' and origin == 'CAT'):
            # Termination request or version update from CAT
            if 'Service' in package_data:
                self.check_stop_procedure(package_data['Service'], wait=0.06)
            else:
                cherrypy.engine.publish('version', package_data) 
                
        elif (cmdtype == 'SET' and origin == 'WebSrv'):
            self.change_service_status(package_data)
            
        elif cmdtype == 'EVENT':
            tag = package_data['TAG']
            init = package_data['INIT']
            if init and tag == 'INFO':
                # Set the service initialized (ready for requests)
                self.services[origin].update({'Init':True,
                                              'LastMsg':self.now,
                                              'Received':self.now})
                cherrypy.engine.publish('setting', self.settings) 
            # Send the last X service events to the webserver
            self.update_service_events(msg)
            
        elif cmdtype == 'INIT':
            if 'DEVICES' not in package_data:
                self.init_work( package_data)
                
        elif cmdtype == 'START':
            # Start a new campaign and init some parameters
            if 'DEVICES' in package_data:
                self.interval = int(package_data['INTERVAL'])
                self.point = 0
                self.date_start = package_data['TIMESTAMP']
                self.campaign_active = True
                self.start_campaign(package_data)
                
        elif cmdtype == 'STOP':
            # Stop active campaign
            if 'DEVICES' in package_data:
                self.campaign_active = False      
                self.settings['campaign']['Active'] = False
         
        elif (cmdtype in ['RESPONSE', 'STATUS'] and
              header['CMDREFERENCE'] in self.last_msgrefs):
            # Update reference parameter of 'origin' service
            self.services[origin]['LastMsg'] = header['CMDREFERENCE']
            self.services[origin]['Received'] = self.now

            if package_data != {}: 
                # Handle packages from different 'origins'
                if origin == 'Logger':
                    cherrypy.engine.publish('data', package_data) 
                    packet = self.create_package(SENDER,
                                                 receiver='TaskEv',
                                                 data=package_data, 
                                                 cmdtype='SET')
                    self.send_q.put(packet) 
                    self.create_log_time('publish')
                    self.log_timestamps()
                elif origin == 'TaskEv':
                    self.update_campaign_task(package_data)
                else:
                    new_settings, changes = self.update_data(msg)
                    if changes:
                        self.settings = new_settings
                        # Update device parameters (controllers)
                        cherrypy.engine.publish('setting', self.settings) 
                        if self.services['Logger']['Status']:
                            # Push new data to the logging service
                            packet = self.create_package(SENDER,
                                                         receiver='Logger',
                                                         data=self.settings, 
                                                         cmdtype='SET')
                            self.send_q.put(packet) 

   
    def run(self):
        # Start service loop
        while 1:
            try:
                msg = self.recv_qs[SENDER].get(timeout=0.05)
            except mp.queues.Empty:
                self.no_message()
            else:
                if msg == 'STOP':
                    for serv in self.services:
                        time.sleep(1)
                        self.check_stop_procedure(serv, shutdown=True)
                    break
                else:
                    try:
                        self.handle_message(msg)
                    except Exception as err:
                        self.create_event_log_package(SENDER,
                                                      exception=err)


###############################################################################
# THREADS
###############################################################################
 
def server_thread(queues, service_list, settings, dummymode):
    
    # Start initialisation of the tempserver
    Trs = TempServerThread(queues, service_list, settings, dummymode)
    Trs.run()
    
    
###############################################################################
# FUNCTIONS
###############################################################################  
    
  
# Python function to start the service

def start_service(service, service_list, send_q,
                  recv_q, settings, dummymode):
    """Try to start the selected service and share some parameters"""
    
    queues = [send_q, recv_q]
    
    ph = PackageHandler(queues=queues, settings=settings)
    
    # PROCESS == SERVICE
    try:
        # Sends a message to the service page
        msg_text = "Service " + service + " has been started"
        ph.create_event_log_package(SENDER, event_message=msg_text)
        if service == 'Logger':
            # Create the logging service
            import logger_service
            logger_service.logger_process([service, service_list],
                                          queues, settings,
                                          dummymode)
        elif service == 'TaskEv':
            # Create the task and event service
            import taskevent_service
            taskevent_service.taskevent_process([service, service_list],
                                                queues, settings,
                                                dummymode)
        elif service == 'Mssngr':
            # Create the Messenger service
            import messenger_service
            messenger_service.messenger_process([service, service_list],
                                                queues, settings,
                                                dummymode)     
        else:
            # Create and start the individual service
            inm.start_module([service, service_list], queues,
                              settings, dummymode)
        # As soon as the service (Python Process) has been stopped
        # a message is sent to the service page
        msg_text = "Service " + service + " has been stopped"
        ph.create_event_log_package(SENDER, event_message=msg_text)
    except Exception as err:
        # Sends an error message to the service page in case of an error
        ph.create_event_log_package(service, exception=err)     

                        
