
###############################################################################
# IMPORTS
############################################################################### 

# Webpage
import cherrypy

# Time and clock related
from datetime import datetime as dt

# System and os
import os

# File related
import json

# Get system ip
import socket

# Key related
import random
import string

# Format content
import markdown

# Scriptlib
import file_functions as ffu

# Communitcation Package
from package_broker import PackageHandler

###############################################################################
# LOCAL VARIABLES
###############################################################################

CONF = {
    'global': {
        'server.thread_pool': 5,
        'server.socket_queue_size': 5,
        'engine.autoreload.on': False,
        'tools.sessions.same_site': 'None',
    },
    '/': {
        'tools.sessions.locking': 'explicit',
        'tools.staticdir.root': os.path.abspath(os.getcwd()),
        'tools.sessions.on': True,
        # 'tools.sessions.cookie_name': 'session_id',
        # 'tools.sessions.cookie_path': '/',
        # 'tools.sessions.signed': False,
        # 'tools.sessions.httponly': True,
        # 'tools.sessions.secure': True,  # Ensures cookies are sent only over HTTPS
        # 'tools.sessions.same_site': 'strict',  

    },
    '/webpage': {
        'tools.staticdir.on': True,
        'tools.staticdir.dir': './server/webpages',
        'tools.sessions.on': True
    },
    '/campwork_exchange': {
        'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
        'tools.response_headers.on': True,
        'tools.response_headers.headers': [('Content-Type',
                                            'text/plain')]
    },
    '/data_exchange': {
        'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
        'tools.response_headers.on': True,
        'tools.response_headers.headers': [('Content-Type',
                                            'text/plain')]
    },
    '/service_exchange': {
        'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
        'tools.response_headers.on': True,
        'tools.response_headers.headers': [('Content-Type',
                                            'text/plain')]
    },
    '/setting_exchange': {
        'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
        'tools.response_headers.on': True,
        'tools.response_headers.headers': [('Content-Type',
                                            'text/plain')]
    },
    '/version_exchange': {
        'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
        'tools.response_headers.on': True,
        'tools.response_headers.headers': [('Content-Type',
                                            'text/plain')],
    }
}

SENDER = 'WebSrv'

# WORK AROUND:
Update_Parameters = {}


###############################################################################
# CHERRYPY
###############################################################################
              
 
"""------------------------------- SERVICES -------------------------------""" 
 
class WebServiceParameter(PackageHandler):
    """Create collection of all neeeded paramters for the webserver classes"""
    def __init__(self, doormen=None, queues=None,
                 process_list=None, settings=None):
        super().__init__(self, queues=queues, settings=settings)

        self.doormen = doormen
        self.process_list = process_list
        

@cherrypy.expose  
class TemperatureService(WebServiceParameter):
    """Communication link to the 'Temperature' Page"""
    def __init__(self, doormen, queues, process_list, settings):
        super().__init__(doormen, queues, process_list, settings)
        
        self.last_datasets = {}
        
        # Location of campaign files / folders
        self.folder = settings['paths']['campaign']
        
        # Variable used for cache some data from different campaigns
        self.temp_data = {}
        
        self.last_datasets = {} 
        
    #WEBSERVER   
    @cherrypy.tools.accept(media='text/plain')
    @cherrypy.tools.json_out()
    def GET(self, key=None, period=None, first_date=0):
        """
        Return data of the campaign ('key')
        and time period which should be loaded
        """
        if key is not None and period is not None:
            # Convert the period to int since all data is a string
            pint = int(period)
            if pint != 0:
                try:
                    return self.load_campaign_files(key, pint, first_date)
                except Exception:
                    pass
            else:
                # If period == 0,
                # the website tries to fetch the data points of the last timestamps
                try:
                    last_dataset = self.doormen['Data'].message
                    for key in last_dataset:
                        # Keys: Sensors, Devices
                        if key not in self.last_datasets:
                            self.last_datasets[key] = [last_dataset[key]]
                        else:
                            if last_dataset[key] != self.last_datasets[key][-1]:
                                if len(self.last_datasets[key]) >= 10:
                                    self.last_datasets[key].pop(0)
                                # Save the last 10 timestamp data
                                self.last_datasets[key].append(last_dataset[key])
                except Exception:
                    pass
                return self.last_datasets
        
    def convert_data_to_dict(self, data, header):
        """
        Convert the data array from the temperature file into a dictionary,
        where each key is the sensor-id
        """
        dict_data = {}
        for col in range(len(header)):
            # Get the sensor-id from the header
            key = header[col]
            # Add the sensor-id and its data to the dictionary
            dict_data[key] = data[col]
        return dict_data
    
    def load_campaign_files(self, campaign, period, first_date):
        """
        Load temperature data of campaign for the selected period
        from the campaignlog folder
        """
        # Get all filenames as list of the selected campaign
        campaign_temps = os.path.join(self.folder,
                                      campaign + r'\TemperatureData')
        files = ffu.get_filenames(campaign_temps, ending='txt')
        # Reverse the filename list to start with the oldest
        files.reverse()
        
        # Determine how many day files need to be read
        n_days = 0
        while n_days*3600*24 < period:
            # Add one day file
            n_days += 1
            if n_days == len(files):
                # In case only 'n_days' files are available stop the loop
                n_days = len(files) - 1
                # self.temp_data[campaign] = {}
                break
         
        # Reduce the amount of data sent to the user website
        # by data of files has already been loaded (files are ignored)
        if first_date != '0':  
            files, n_days = self.change_file_list(first_date, files, n_days)
         
        # Create a dictionary containing the data  
        temp_data = {}
        
        str_data = ''
        header_end = 0
        header = []
        for f in files:
            # Read data and convert it to a dictionary
            raw_file_data = ffu.read_ascii_file(f, newline=False)
            
            if header_end == 0:
                # Get end of header (newline)
                for char in raw_file_data:
                    if char == '\n':
                        break
                    header_end += 1  
                    
            header = raw_file_data[:header_end].split('\t')
            str_data = raw_file_data[header_end+1:] + str_data
            
            if f == files[n_days]:
                break
        
        # Convert string into lists
        line_data = str_data.split('\n')

        ncols = len(header)
        col_list = [[] for _ in range(ncols)]
        for l in line_data:
            if l != '':
                lsp = l.split('\t')
                for i in range(ncols):
                    col_list[i].append(lsp[i])     
        
        # Convert lists into dictionary
        conv_data = self.convert_data_to_dict(col_list, header)
        for key in conv_data.keys():
            if '[UTC]' in key:
                # Remove the [UTC] from the key
                new_key = 'Timestamp'
            else:
                new_key = key
            # Combining the data of the different day files
            temp_data[new_key] = conv_data[key]

            
        return temp_data

    def change_file_list(self, first_date, files, n_days):
        """
        Reduces the amount files need to be opened (and readout)
        depending on the parameter 'first_date'
        """
        for f in files.copy():
            n_days -= 1
            files.pop(0)
            if os.path.basename(f).split('_')[0] == first_date:
                break
        return files, n_days
  
        
@cherrypy.expose
class SettingService(WebServiceParameter):
    """Communication link of the status of all devices"""
    def __init__(self, doormen, queues, process_list, settings):
        super().__init__(doormen, queues, process_list, settings)
        
    def prepare_packet(self, body):
        """Prepare and send packets to the different 'receiver'"""
        cmdtype = body['COM']
        if cmdtype in ['SET', 'CONNECT']:
            # 'CONNECT' to (dis-)connect to a device
            # 'SET' to change a parameter of a device
            for receiver in [body['SERIES'], 'TaskEv']:
                packet = self.create_package(SENDER, 
                                             receiver=receiver, 
                                             cmdtype=cmdtype, 
                                             data=body['DATA'])
                self.send_q.put(packet)

    # WEBSERVER   
    @cherrypy.tools.accept(media='text/plain')
    @cherrypy.tools.json_out()
    def GET(self):
        """Return the current settings of the devices, campaigns, ..."""
        self.settings = self.doormen['Setting'].message

        if self.settings != {}:
            # Only send imporant information to the webpage 
            # Removed log path information
            tosend = {'data_logger':self.settings['data_logger'],
                      'temp_control':self.settings['temp_control'],
                      'campaign':self.settings['campaign'],
                      'work':self.settings['work'],
                      'website':self.settings['website']}
            return tosend
        
    def POST(self):
        cl = cherrypy.request.headers['Content-Length']
        raw_body = cherrypy.request.body.read(int(cl))
        
        body = json.loads(raw_body)
        self.prepare_packet(body)
        

@cherrypy.expose  
class CampaignWorkService(WebServiceParameter):
    """Communication link to the 'Campaign' and 'Work' Pages"""
    def __init__(self, doormen, queues, process_list, settings):
        super().__init__(doormen, queues, process_list, settings)
        
        # Get the default paths of campaign and work
        self.campaign_folder = self.settings['paths']['campaign']
        self.work_folder = self.settings['paths']['work']

    def prepare_packet(self, body):   
        """Prepare and send package to the 'receiver' """
        cmdtype = body['COM']
        if cmdtype in ['START', 'STOP']:
            # Start / Stop a current campaign or work
            if 'CAMPAIGN' in body:
                to_list = self.process_list + ['TempSrv']
                for to in to_list:
                    # Send the start signal to the different series services
                    self.send_packet(to, cmdtype, body['CAMPAIGN'])
                # Start timestamp of the campaign is created here 
                body['CAMPAIGN']['TIMESTAMP'] = dt.utcnow()
                # Send start signal to logger and TaskEvent
                self.send_packet('Logger', cmdtype, body['CAMPAIGN'])
                self.send_packet('TaskEv', cmdtype, body['CAMPAIGN'])
            elif 'WORK' in body:
                # Sent task list (Work) to TaskEvent service
                self.send_packet('TaskEv', cmdtype, body['WORK'])

        elif cmdtype == 'SET':
            # Send changes of campaign parameters to the logger
            self.send_packet('Logger', cmdtype, body['CAMPAIGN'])

        elif cmdtype == 'INIT':
            # Initialize different services
            if 'CAMPAIGN' in body:
                self.send_packet('Logger', cmdtype, body['CAMPAIGN'])
                self.send_packet('TaskEv', cmdtype, body['CAMPAIGN'])
            elif 'WORK' in body:
                self.send_packet('TaskEv', cmdtype, body['WORK'])
                self.send_packet('TempSrv', cmdtype, body['WORK'])
            else: 
                self.send_packet(body['SERIES'], cmdtype, body['DEVICES'])

    def send_packet(self, receiver, cmdtype, body_data):
        """Send packet with body_data to the 'receiver'"""
        packet = self.create_package(SENDER, 
                                     receiver=receiver, 
                                     cmdtype=cmdtype, 
                                     data=body_data)
        self.send_q.put(packet)

    # WEBSERVER   
    @cherrypy.tools.accept(media='text/plain')
    @cherrypy.tools.json_out()
    def GET(self, key=None, group='campaign'):
        """
        Return the data of the selected campaign / work ('key'). 
        If work or campaign depends on parameter 'group' 
        """
        if key != None:
            if group == 'campaign':
                current_campaign_folder = os.path.join(self.campaign_folder,
                                                       key)
                campaign_details_filename = os.path.join(current_campaign_folder,
                                                         'campaign_details.yaml')
                tosend = ffu.config_file_reader(campaign_details_filename)
                return tosend
            elif group == 'work':
                current_work_filename = os.path.join(self.work_folder,
                                                     key + '.yaml')
                tosend = ffu.config_file_reader(current_work_filename)
                return tosend

    def POST(self):
        cl = cherrypy.request.headers['Content-Length']
        raw_body = cherrypy.request.body.read(int(cl))
        
        body = json.loads(raw_body)
        self.prepare_packet(body)   


@cherrypy.expose  
class StatusService(WebServiceParameter):
    """Communication link to the 'Service' Page"""
    def __init__(self, doormen, queues, process_list, settings):
        super().__init__(doormen, queues, process_list, settings)
        
        self.tosend = {'Services':{}, 'Events':[]}
     
    def get_status(self):
        """Get the current status of each service and incoming events"""
        self.process_dict = self.doormen['Service'].message
        for p in self.process_dict:
            if p not in self.tosend['Services']:
                self.tosend['Services'][p] = {}
            self.tosend['Services'][p] = self.process_dict[p]
        self.tosend['Events'] = self.doormen['Sevent'].message

    def prepare_packet(self, body):
        """Send service requests to TempSrv"""
        packet = self.create_package(SENDER, data=body, cmdtype='SET')
        self.send_q.put(packet)

    @cherrypy.tools.accept(media='text/plain')
    def GET(self): 
        """Return service status and events"""
        self.get_status()
        return json.dumps(self.tosend)
    
    def POST(self):
        cl = cherrypy.request.headers['Content-Length']
        raw_body = cherrypy.request.body.read(int(cl))
        
        body = json.loads(raw_body)
        self.prepare_packet(body) 


@cherrypy.expose  
class VersionService(WebServiceParameter):
    """Communication link for the server and service versions"""
    def __init__(self, doormen, queues, process_list, settings):
        super().__init__(doormen, queues, process_list, settings)
        
        self.tosend = {}
     
    def get_versions(self):
        """Fetch the current server and service versions"""
        self.tosend = self.doormen['Version'].message

    @cherrypy.tools.accept(media='text/plain')
    def GET(self): 
        """Return the current server and service versions"""
        self.get_versions()
        return json.dumps(self.tosend)
    
    def POST(self):
        cl = cherrypy.request.headers['Content-Length']
        raw_body = cherrypy.request.body.read(int(cl))
        body = json.loads(raw_body)
        
        # WORK AROUND: 
        # Define which services need to be updated after server shutdown
        global Update_Parameters
        Update_Parameters = body

"""--------------------------------- USERS ---------------------------------""" 

class User:
    def __init__(self, username, password):
        self.username = username
        self.password = password
        self.token = None

class Admin(User):
    def generate_token(self):
        """Generate an admin access token"""
        self.token = ''.join(random.choices(string.ascii_letters
                                            + string.digits, k=16))
        return self.token

"""------------------------------- WEBPAGES -------------------------------""" 


class TempControlServer(object):
    def __init__(self, admin_pw, modules_exist=False, admin_ips=[]):
        self.modules_exist = modules_exist
        self.admin_ips = admin_ips
        
        self.users = {
            'admin': Admin('admin', admin_pw)
        }
        self.html_path = 'server/webpages/html/'

  
    def check_permission(self):
        """Return access after login or admin ip"""
        user_ip = cherrypy.request.remote.ip
        # If user_ip is in a list of admin_ips (see config files)
        if user_ip in self.admin_ips:
            return True 
        # Login as user (CURRENTLY ONLY THE USER ADMIN EXISTS!!)
        if 'username' in cherrypy.session:
            username = cherrypy.session['username']
            if username in self.users:
                user = self.users[username]
                if isinstance(user, Admin) and user.token:
                    return True
        return False

    def render_page(self, page, changes={}):
        """Rerender webpage with changed content"""
        with open(self.html_path + page, "r") as file:
            content = file.read()
          
        # Change Login / Logout button depending on permission status
        if self.check_permission():
            changes.update({">Login<":">Logout<",
                            "showLoginModal()":"window.location.href='/logout'"})
        else:
            changes.update({">Logout<":">Login<",
                           "window.location.href='/logout'":"showLoginModal()"})
           
        content = self.change_page_content(content, changes)
        return content

    def change_page_content(self, content, changes):
        """Change content of webpage. Replace key with value."""
        for key, val in changes.items():
            content = content.replace(key, val)
        return content

    #### LOGIN / LOGOUT PAGES ####

    @cherrypy.expose
    def login(self, username=None, password=None):
        if username and password:
            if (username in self.users and
                self.users[username].password == password):
                cherrypy.session['username'] = username
                if isinstance(self.users[username], Admin):
                    self.users[username].generate_token()
                raise cherrypy.HTTPRedirect('/')
        return ("Invalid username or password."
                + """<form method="post" action="/">
                <input type="submit" value="Return to Main Page"></form>""")

    @cherrypy.expose
    def logout(self):
        cherrypy.session.pop('username', None)
        raise cherrypy.HTTPRedirect('/')  

    #### DIFFERENT WEBPAGES ####

    @cherrypy.expose
    def index(self):
        """Load change log and readme and delete the first rows"""
        with open("CHANGELOG.md") as file:
            changelog_content = file.read().split('\n\n', 1)[-1]
        with open("README.md") as file:
            readme_content = file.read()
        
        changes = {} 
        # Will be added in updated with new details (0.3.1)
        # changes = {"<p>CHANGELOG</p>":markdown.markdown(changelog_content)}
        changes.update({"<p>README</p>":markdown.markdown(readme_content)})
        return self.render_page('index.html', changes)
    
    @cherrypy.expose
    def services(self):
        if self.check_permission():
            return self.render_page('services.html')
        return self.noaccess()
    
    @cherrypy.expose
    def campaign(self):
        return self.render_page('campaign.html')
    
    @cherrypy.expose
    def create_campaign(self):
        if self.check_permission() and self.modules_exist:
            return self.render_page('create_campaign.html')
        return self.noaccess()
    
    @cherrypy.expose
    def edit_campaign(self):
        if self.check_permission():
            return self.render_page('edit_campaign.html')
        return self.noaccess()
    
    @cherrypy.expose
    def tasks(self):
        if self.check_permission() and self.modules_exist:
            return self.render_page('tasks.html')
        return self.noaccess()
    
    @cherrypy.expose
    def temperature(self):
        return self.render_page('temperature.html')
    
    @cherrypy.expose
    def controller(self):
        if self.check_permission():
            return self.render_page('controller.html')
        return self.noaccess()
    
    @cherrypy.expose
    def shutdown(self):  
        if self.check_permission():
            cherrypy.engine.exit()
            # Send stop signal to tkinter Popup
            return self.render_page('shutdown.html')
        return self.noaccess()  
        
    def noaccess(self):
        return "No Access." + """<form method="post" action="/">
        <input type="submit" value="Return to Main Page"></form>"""


###############################################################################
# RUN
###############################################################################

class Webserver:  
    def __init__(self, tk_q):
        self.tk_q = tk_q
    
    def init(self, doormen, queues, process_list,
             settings, dummymode, modules_exist):
        con_cfg = settings['config']['CONNECTION']
        
        # Get ip and port for webserver
        if con_cfg['use_system_ip']:
            hostname = socket.gethostname()    
            html_ip = socket.gethostbyname(hostname)
        else:
            if dummymode:
                html_ip = '127.0.0.1'
            else:
                html_ip = con_cfg['server_ip']
        html_port = con_cfg['server_port']
        
        # Get access ips
        login_cfg = settings['config']['LOGIN']
        admin_ips = login_cfg['admin_ips']
        # Add server ip to admin access
        admin_ips.append(html_ip)
        self.webapp = TempControlServer(login_cfg['admin_pw'],
                                        modules_exist=modules_exist,
                                        admin_ips=admin_ips)
        
        # Create the different communication links
        self.webapp.data_exchange = TemperatureService(doormen, queues,
                                                       process_list, settings)
        self.webapp.campwork_exchange = CampaignWorkService(doormen, queues,
                                                            process_list, settings)
        self.webapp.setting_exchange = SettingService(doormen, queues,
                                                      process_list, settings)
        self.webapp.service_exchange = StatusService(doormen, queues,
                                                     process_list, settings)
        self.webapp.version_exchange = VersionService(doormen, queues, 
                                                      process_list, settings)
    
        # Set ip and port of webserver
        cherrypy.server.socket_host = html_ip
        cherrypy.server.socket_port = html_port
        
        # Set Configuration of cherrypy server
        cherrypy.config.update({'log.screen': True,
                                'log.error_file': '','global': {
                                'environment' : 'production',
                                }})
        self.tk_q.put(f"WEBAPP STARTED, CHECK {html_ip}:{html_port}")
        
    def run(self):
        # RUN 
        cherrypy.quickstart(self.webapp, '/', CONF)
    
        
def webserver_thread(doormen, tk_q, queues, process_list,
                     settings, dummymode, modules_exist):
    websrv = Webserver(tk_q)
    websrv.init(doormen, queues, process_list,
                settings, dummymode, modules_exist)
    websrv.run()
    
    # Return services to update on server shutdown (only if selected)
    tk_q.put(Update_Parameters)