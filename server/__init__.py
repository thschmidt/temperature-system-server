
# System and os
import os
import sys

# Connect other folders / include scripts
main_path = os.path.dirname(os.getcwd())
module_init_path = os.path.join(main_path, 'modules')
messenger_init_path = os.path.join(main_path, 'messengers')
sys.path.insert(1, module_init_path)
sys.path.insert(1, messenger_init_path)
sys.path.insert(1, 'server')
sys.path.insert(1, 'server/services')
sys.path.insert(1, 'server/messengers')
sys.path.insert(1, 'server/subfunctions')
sys.path.insert(1, 'server/subfunctions/scriptlib')

__all__ = ['tempserver',
           'webserver']
