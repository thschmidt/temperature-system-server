
###############################################################################
# IMPORTS
###############################################################################

# Time and clock related
from datetime import timedelta
from datetime import datetime as dt
import time

# System and os
import os

# Thread and processes
import multiprocessing as mp


# Scriptlib
import file_functions as ffu

# Package
from package_broker import PackageHandler

###############################################################################
# CLASS
###############################################################################

class TempLogger(PackageHandler):   
    def __init__(self, service, queues, serv_list, settings):
        
        super().__init__(queues=queues, settings=settings)

        self.service = service
        
        self.initialization()
        self.create_event_log_package(self.service,
                                      event_message='Initialized',
                                      init=True)
        
        self._ref = ''
        
    """-------------------------- INITIALIZATION --------------------------""" 
    
    def initialization(self):
        """Call all initial functions"""
        # Get current campaign
        self.campaign = self.settings['campaign']['CurrentCampaign']
        # Create campaign parameters: folders, configs, ...
        self.campaign_folder = os.path.join(self.logpaths['campaign'],
                                            self.campaign)
        self.campaign_config = {}
        self.campaign_sensors = {}
        
        # Parameters saving data of last readout
        self.last_readout_data = {}
        self.cache_controll_data = {'Timestamp':'', 'Data':{}}
        self.last_timestamp = dt.utcnow().strftime('%Y-%m-%dT%H:%M:%S')
        
        # Sensor Errors
        self.sensor_errors = {'1':[], '2':[]}
        self.sensor_limits = {}

    """--------------------- LOG DATALOGGER FUNCTIONS ---------------------""" 
    
    def log_temperature_data(self, data, timestamp=''):
        """Update the temperature logging file with new data"""
        # Save last readout data and timestamp as class variables
        # (later sent to webpage)
        self.last_readout_data['Sensors'] = data
        self.last_timestamp = timestamp
        
        # Prepare header of file
        header = ['TimeStamp [UTC]'] + self.get_sensor_list()
        
        # Log temperature data
        self.log_data(data, header, timestamp, data_type='temperature')
        
    def data_dict_to_txt(self, log_dict, header, logtext=''):
        """Convert sensor data dictionary to text (line)"""
        logtext += log_dict['Timestamp']
        # Loop over all sensors (defined in header)
        for h in header[1:]:
            logtext += '\t' + str(log_dict['Data'][h])
        # Append new line for next entry
        logtext += '\n'     
        return logtext
        
    def prepare_data_log(self, data):
        """Prepare logging of temperature and controller data"""
        # Check if new data is available
        if data != {}:
            timestamp = data['campaign']['LastReadout']
            if timestamp != '':         
                # Check controller data
                controller_check = self.check_timestamp_controller(data,
                                                                   timestamp)
                # Check and get temperature data
                temperature_log_data = self.check_timestamp_sensors(data,
                                                                    timestamp)
                # Only log if all sensor and controller data of the current
                # timestamp had been fetched
                if (temperature_log_data != {} and controller_check and
                    timestamp != self.last_timestamp):
                    # Log temperature and controller data seperately
                    self.create_log_time('st_log')
                    self.log_controller_data()
                    self.log_temperature_data(temperature_log_data, timestamp)
                    self.create_log_time('fi_log')
                    
                    self.respond_status(self._ref)
                    self._ref = ''
                    
                # Just to avoid the logger service getting stuck
                if self._ref != '':
                    ref_time = dt.strptime(self._ref,'%Y-%m-%dT%H:%M:%S.%f')
                    if (dt.utcnow() - ref_time) > timedelta(seconds=4):
                        self.respond_status(self._ref)
                        self._ref = ''

    def check_timestamp_sensors(self, data, timestamp, wait_time = 3.1):
        """Check if all sensor data from all active devices for the last 
        readout timestamp had been collected. 
        Return cache_data of collected data""" 
        cache_data = {'Timestamp':'', 'Data':{}}
        try:
            last_logtime = self.string_to_datetime(timestamp)
            for group in self.campaign_sensors:
                for series in self.campaign_sensors[group]:
                    for device_id in self.campaign_sensors[group][series]:
                        device = data[group][series]['ID'][device_id]
                        # if group == 'data_logger':
                        if device['SensorData'] != {}:
                            # Get first key of SensorData dictionary
                            last_readout = next(iter(device['SensorData']))
                            last_readout_dt = self.string_to_datetime(last_readout)
                            if abs(last_logtime-last_readout_dt) < timedelta(seconds = wait_time): 
                                for sens in self.campaign_sensors[group][series][device_id]:
                                    if sens.split('-')[1] not in device['SensorData'][last_readout]:
                                        return {}
                                    else:
                                        sens_dat = device['SensorData'][last_readout][sens.split('-')[1]]
                                        cache_data['Data'][sens] = sens_dat
                            else:
                                return {}
                            
                        if (device['SensorData'] == {}
                            and self.campaign_sensors[group][series][device_id] != []):
                            return {}
        except ValueError:
            return {}
        else:
            if cache_data['Data'] != {}:
                cache_data['Timestamp'] = timestamp
                return cache_data
            else:
                return {}

    def check_timestamp_controller(self, data, timestamp):
        """Check if all controller data from all active devices for the last 
        readout timestamp had been collected. Return status as boolean""" 
        # Only readout devices in group 'temp_control'
        group = 'temp_control'
        # Get timestamp of last log
        last_logtime = self.string_to_datetime(timestamp)
        
        if 'Devices' in self.campaign_config:
            if group in self.campaign_config['Devices']:
                for series_name in self.campaign_config['Devices'][group]:
                    series = self.campaign_config['Devices'][group][series_name]
                    for device_id in series:
                        device = data[group][series_name]['ID'][device_id]
                        last_readout = device['LastReadout']
                        last_readout_dt = self.string_to_datetime(last_readout)
                        if abs(last_logtime-last_readout_dt) < timedelta(seconds = 1.1):
                            if device_id not in self.cache_controll_data['Data']:
                                self.cache_controll_data['Data'][device_id] = device['Channel']
                
                for series_name in self.campaign_config['Devices'][group]:
                    series = self.campaign_config['Devices'][group][series_name]
                    if self.cache_controll_data['Data'].keys() != series.keys():
                        # If not all controller data has not been collected yet 
                        return False
                # Save current timesamp in controller data cache
                self.cache_controll_data['Timestamp'] = timestamp
        return True
    
    def log_controller_data(self):
        """Update the controller logging file with new data"""
        self.last_readout_data['Devices'] = self.cache_controll_data
        timestamp = self.cache_controll_data['Timestamp']
        if timestamp != '':
            data = self.cache_controll_data['Data']
            header_lines = ['Timestamp [UTC]\t', '\t', '\t'] 
            data_line = [timestamp]
            
            # Create header lines and line data
            for device_id in data:
                for channel_name in data[device_id]:
                    header_lines[0] += device_id + '-' + channel_name
                    for parameter_name in data[device_id][channel_name]:
                        parameter = data[device_id][channel_name][parameter_name]
                        header_lines[1] += parameter_name
                        # Add Unit in Header in Brackets
                        if 'Unit' in parameter:
                            header_lines[1] += ' [' + parameter['Unit'] + ']'
                        for variable in ['Set', 'Read']:
                            if variable in parameter:
                                header_lines[2] += variable + '\t'
                                parameter[variable]
                                header_lines[0] += '\t'
                                header_lines[1] += '\t'
                                value = parameter[variable]
                                data_line.append(str(value))
             
            # Log controller data
            self.log_data(data_line, header_lines,
                          timestamp, data_type='controller')
        # Save last data in the cache variable
        self.cache_controll_data = {'Timestamp':'', 'Data':{}}
        
                    
    """------------------------ CAMPAIGN FUNCTIONS ------------------------""" 
    
    def init_campaign(self, data):
        """Create initial path and configuration files of a new campaign"""
        # Create folder and path/filename
        self.campaign_folder = os.path.join(self.logpaths['campaign'],
                                            data['ID'])
        ffu.check_folder(self.campaign_folder)
        campaign_details_file = os.path.join(self.campaign_folder,
                                             'campaign_details.yaml')
        
        copy_campaign= {'Sensors':{}}
        if data['COPY'] != "":
            campaign_path = os.path.join(self.logpaths['campaign'],
                                         data['COPY'])
            a, copy_campaign = self.read_campaign_config(campaign_path)
        
        # Create config file input as dictionary
        # Default parameters:
        dictionary = {'VERSION':self.settings['version'],
                      'Name':'', 'Start':'', 'Active':False, 'Interval':0}
        for key in dictionary:
            try:
                dictionary[key] = data[key.upper()]
            except KeyError:
                pass
        # Reset sensor limits
        self.sensor_limits = {}
            
        # Add sensor and data of devices
        dictionary['Sensors'] = self.create_sensor_config(data['SENSORS'],
                                                          copy_campaign['Sensors'])
        dictionary['Devices'] = self.create_device_config(data['DEVICES'])
        
        # Create yaml / config gile
        ffu.write_yaml_file(campaign_details_file, dictionary, sort_keys=True)
        
        # Update campaigns file
        # (readout previous file to check if campaign already exists)
        campaigns_file = os.path.join(self.logpaths['campaign'],
                                      'campaigns.txt')
        campaign_list = ffu.read_ascii_file(campaigns_file)[:-1]
        if data['ID'] not in campaign_list:   
            with open(campaigns_file, 'a') as f:
                f.write(data['ID'] + '\n')
                
        # Define campaign class variables
        self.campaign = data['ID']
        self.campaign_config = dictionary
        self.campaign_sensors = self.create_sensor_dict(self.get_sensor_list())
        
        # Create folders for the controller and temperature data
        ffu.check_folder(os.path.join(self.campaign_folder, 'TemperatureData'))
        ffu.check_folder(os.path.join(self.campaign_folder, 'ControllerData'))
        
        # Reset controller cache
        self.cache_controll_data = {'Timestamp':'', 'Data':{}}
        
    def start_campaign(self, start_time):
        """Add start time and change status of current campaign
        in its config file"""
        # Load current campaign config file
        campaign_details_file, self.campaign_config = self.read_campaign_config()
        
        self.campaign_config['Start'] = start_time.strftime('%Y-%m-%dT%H:%M:%S UTC')
        self.campaign_config['Active'] = True
        
        # Reset sensor errors
        self.sensor_errors = {'1':[], '2':[]}
        
        # Overwrite old campaign config file
        ffu.write_yaml_file(campaign_details_file,
                            self.campaign_config,
                            sort_keys=True)
        
    def stop_campaign(self, stop_time):
        """Add stop time and change status of current campaign
        in its config file"""
        # Load current campaign config file
        campaign_details_file, self.campaign_config = self.read_campaign_config()
        
        self.campaign_config['Stop'] = stop_time.strftime('%Y-%m-%dT%H:%M:%S UTC')
        self.campaign_config['Active'] = False
        
        # Overwrite old campaign config file
        ffu.write_yaml_file(campaign_details_file,
                            self.campaign_config,
                            sort_keys=True)
        
        # Create summary file if export
        export_extension = self.settings['config']['DATA']['export_data']
        if export_extension != 'NONE':
            self.create_summary_file()
        
        self.last_readout_data = {}
        
        # Reset some campaign variables
        self.campaign_config = {}
        self.campaign_sensors = {}
        
        # Reset the status message
        if self._ref != '':
            self.respond_status(self._ref)
        
    def update_campaign_config(self, msg):
        """Update the current campaign config file"""
        # Create filename and readout old campaign configuration
        campaign_details_file = os.path.join(self.logpaths['campaign'],
                                             msg['ID'], 
                                             'campaign_details.yaml')
        old_config = ffu.read_yaml_file(campaign_details_file)
        # Extract data from message sent from the webpage, prepare an empty dict
        data = msg['DATA']
        dictionary = {}
        # Update every parameter (key)
        for key in data:
            if key == 'Sensors':
                dictionary[key] = {}
                for group in data[key]:
                    dictionary[key][group] = {}
                    for series in data[key][group]:
                        series_data = data[key][group][series]
                        sensor_data = self.update_sensors_config(series_data)
                        dictionary[key][group][series] = sensor_data
                    if dictionary[key][group] == {}:
                        dictionary[key].pop(group, None)
            elif key == 'Devices':
                # Needs to be changed if device table get editable
                dictionary[key] = old_config[key]
            else:  
                dictionary[key] = self.string_to_boolean(data[key])
        dictionary['Name'] = old_config['Name']
        # Update campaign config variable if changes done for current campaign
        if msg['ID'] == self.campaign:
            self.campaign_config = dictionary
        
        # Overwrite old campaign config file
        ffu.write_yaml_file(campaign_details_file,
                            dictionary,
                            sort_keys=True)
    
    def update_sensors_config(self, data):
        """Update the configuration parameters of each sensor
        using the data from each series"""
        sensor_dict = {}
        # sn => short for number of sensors of the series
        for sn in range(len(data)):
            # Get the device_ids of a series
            ck = data[sn]['ID']
            # Create an entry for the sensor
            # ("ck" = device_id + sensor_id, Example: k20-101)
            sensor_dict[ck] = {}
            # "subkey" = parameters/details of the sensor
            for subkey in data[sn]:
                if 'Error' in subkey or 'Style' in subkey:
                    key_part = subkey.split(' ')
                    if key_part[0] not in sensor_dict[ck]:
                        sensor_dict[ck][key_part[0]] = {}
                    sensor_dict[ck][key_part[0]][key_part[1]] = data[sn][subkey] 
                elif 'Limit' in subkey:
                    key_part = subkey.split(' ')
                    if key_part[1] not in sensor_dict[ck]:
                        sensor_dict[ck][key_part[1]] = {}
                    sensor_dict[ck][key_part[1]][key_part[0]] = data[sn][subkey] 
                    # Update sensor limit variable
                    if self.campaign != '':
                        if 'Upper' in subkey:
                            self.sensor_limits[ck][1] = float(data[sn][subkey])
                        else:
                            self.sensor_limits[ck][0] = float(data[sn][subkey])
                elif subkey == 'Sensor Type':
                    sensor_dict[ck]['SensorType'] = data[sn][subkey] 
                elif subkey == 'ID':
                    pass
                else:
                    sensor_dict[ck][subkey] = self.string_to_boolean(data[sn][subkey])
                
        return sensor_dict

    def read_campaign_config(self, campaign_path=''):
        """Readout campaign config file (default: current campaign)"""
        if campaign_path == '':
            campaign_path = self.campaign_folder
        campaign_details_file = os.path.join(campaign_path,
                                             'campaign_details.yaml')
        campaign_config = ffu.config_file_reader(campaign_details_file)
        # Return current campaign file(name) and its configuration
        return campaign_details_file, campaign_config
        
    def create_device_config(self, device_list):
        """Creat the device details for the campaign"""
        device_dict = {}
        for group in device_list:
            device_dict[group] = {}
            for series in device_list[group]:
                device_dict[group][series] = {}
                for device_id in device_list[group][series]:
                    device_data = device_list[group][series][device_id]
                    device_dict[group][series][device_id] = device_data
            if device_dict[group] == {}:
                device_dict.pop(group, None)
        return device_dict
        
    def create_sensor_config(self, device_list, copy_campaign_sensors={}):
        """Create the sensor details for the campaign"""
        sensor_dict = {}
        for group in device_list:
            sensor_dict[group] = {}
            for series in device_list[group]:
                sensor_dict[group][series] = {}
                for device_id in device_list[group][series]:
                    sensor_data = device_list[group][series][device_id]
                    for sensor_type in sensor_data:
                        for sensor in sensor_data[sensor_type]:
                            sensor_name = str(device_id) + "-" + str(sensor)
                            not_defined = True
                            # Details template for each sensor
                            sensor_template = {'Name':'',
                                               'Description':'',
                                               'Group':'',
                                               'SensorType':'',
                                               'Limit':{'Lower':0,
                                                        'Upper':1000},
                                               'State':'Normal',
                                               'Style':{'Colour':'black',
                                                        'Line':'solid'}} 
                            
                            if copy_campaign_sensors != {}:
                                if sensor_name in copy_campaign_sensors[group][series]:
                                    not_defined = False
                                    sensor_template = copy_campaign_sensors[group][series][sensor_name]
                            if not_defined:
                                sensor_template['Name'] = sensor_name
                                sensor_template['SensorType'] = sensor_type
                            sensor_dict[group][series][sensor_name] = sensor_template
                            self.sensor_limits[sensor_name] = [float(sensor_template['Limit']['Lower']),
                                                               float(sensor_template['Limit']['Upper'])]
            if sensor_dict[group] == {}:
                sensor_dict.pop(group, None)
        return sensor_dict
    

    def create_sensor_dict(self, sensors):
        """Create the campaign sensor dictionary for the current campaign"""
        campaign_sensors = {}
        for group in ['data_logger', 'temp_control']:
            campaign_sensors[group] = {}
            for series in self.settings[group]:
                campaign_sensors[group][series] = {}
                for sens in sensors.copy():
                    device_id = sens.split('-')[0]
                    if device_id in self.settings[group][series]['ID']:
                        if sens.split('-')[0] in campaign_sensors[group][series]:
                            campaign_sensors[group][series][device_id].append(sens)
                        else:
                            campaign_sensors[group][series][device_id] = [sens]
                        sensors.remove(sens)          
        return campaign_sensors

    """----------------------- EXPORT SUMMARY FILE -------------------------""" 

    def create_summary_file(self, extension='txt'):
        """
        Combine all (currently only temperature) data in on file,
        create a bigger header and export it as txt file
        """
        files = ffu.get_filenames(os.path.join(self.campaign_folder,
                                               'TemperatureData'))

        string_data = ''
        i = 0
        header = {}
        for fn in files:
            with open(fn, 'r') as f:
                raw_data = f.read()
            if i == 0:
                header['ID'] = raw_data.split('\n')[0].split('\t')
            pos = raw_data.find('\n')
            string_data += raw_data[pos+1:]
            i += 1
           
        sensor_dict = self.campaign_config['Sensors']
        
        header['Name'] = [''] * len(header['ID'])
        header['Description'] = [''] * len(header['ID'])

        for group in sensor_dict:
            for series in sensor_dict[group]:
                for sensor_id, sensor_data in sensor_dict[group][series].items():
                    index = header['ID'].index(sensor_id)
                    header['Name'][index] = sensor_data['Name']
                    header['Description'][index] = sensor_data['Description']
           
        string_header = ''
        for sub_header in header.values():
            string_header += '\t'.join(sub_header) + '\n'
            
        # Save temperature data as exported file
        exp_filename = self.campaign + '_TemperatureData.' + extension
        with open(os.path.join(self.campaign_folder, exp_filename), 'w') as f:
            f.write(string_header + string_data)

    """--------------------------- SUBFUNCTIONS ----------------------------""" 

    def log_data(self, data, header, timestamp, data_type='temperature'):
        """Log data at each timestamp in temperature or controller data file"""
        if data_type == 'temperature':
            folder_name = 'TemperatureData'
            data_text = self.data_dict_to_txt(data, header) 
            header_text = '\t'.join(header) + '\n'
        else:
            folder_name = 'ControllerData'
            data_text = '\t'.join(data) + '\n'
            header_text = '\n'.join(header) + '\n'
 
        # Define log file
        logfile = os.path.join(self.campaign_folder, folder_name,
                               timestamp.split('T')[0]
                               + '_' + folder_name + '.txt')
        # Check if logfile exists
        # Add header to new log file
        if not os.path.exists(logfile):
            ffu.create_ascii_file(logfile, header_text)
            time.sleep(0.1)

        # Append data_text in logfile
        ffu.append_ascii_file(logfile, data_text)
        

    def string_to_datetime(self, date_string):
        """Convert string timestamp into datetime timestamp"""
        return dt.strptime(date_string, '%Y-%m-%dT%H:%M:%S')
    
    def string_to_boolean(self, string):
        """Convert strings (false, true) into booleans"""
        if string.lower() == 'false':
            return False
        elif string.lower() == 'true':
            return True
        else:
            return string
        
    def get_sensor_list(self):
        """Get list of sensors from config data of the current campaign"""
        sensor_list = []
        for group in self.campaign_config['Sensors']:
            group_sensors = self.campaign_config['Sensors'][group]
            for series in group_sensors:
                series_sensors = list(group_sensors[series].keys())
                sensor_list += series_sensors
        return sensor_list
    
    """---------------------- CHECK / PROOF FUNCTIONS ----------------------""" 
    
    def check_for_errors(self, data):
        """Check sensor data for sensors outside of limit or faulty"""
        # Error Codes: 
        # 1: NaN -> sensors not available / faulty
        # 2: Outside of limit
             
        changed = False
        error_dict = {'1':[],'2':[]}
        for sensor, value in data['Sensors']['Data'].items():
            limit = self.sensor_limits[sensor]
            # Faulty sensors
            if (str(value).lower() == 'nan'
                and sensor not in self.sensor_errors['1']):
                self.sensor_errors['1'].append(sensor)
                error_dict['1'].append(sensor)
                changed = True
            # Limits
            elif (float(value) < limit[0] or float(value) > limit[1]
                  and sensor not in self.sensor_errors['2']):
                self.sensor_errors['2'].append(sensor)
                error_dict['2'].append({'Sensor':sensor, 'Value':value})
                changed = True
            else:
                # Reset error if
                if sensor in self.sensor_errors['1']:
                    if str(value).lower() != 'nan':
                        self.sensor_errors['1'].pop(self.sensor_errors['1'].index(sensor))
                        changed = True
                elif sensor in self.sensor_errors['2']:
                    if float(value) > limit[0] or float(value) < limit[1]:
                        self.sensor_errors['2'].pop(self.sensor_errors['2'].index(sensor))
                        changed = True
                
         
        # Only update if something has changed 
        # (new error or sensor stabilized again)
        if changed and 'Sensors' in self.campaign_config:
            campaign_sensor_dict = self.campaign_config['Sensors']  
            for group in campaign_sensor_dict:
                for sensor_dict in campaign_sensor_dict[group].values():
                    for sensor in sensor_dict:
                        if sensor in self.sensor_errors['1']:
                            sensor_dict[sensor]['State'] = 'Faulty'
                        elif sensor in self.sensor_errors['2']:
                            sensor_dict[sensor]['State'] = 'Out of Lim'
                        else:
                            sensor_dict[sensor]['State'] = 'Normal'
            campaign_details_file = os.path.join(self.campaign_folder,
                                                 'campaign_details.yaml')  
             
            # Update yaml / config gile
            ffu.write_yaml_file(campaign_details_file,
                                self.campaign_config,
                                sort_keys=True)
         
        # Create error log message
        for error_code in error_dict:
            error_list = error_dict[error_code]
            if error_list != []:
                error_message = 'Sensor ERROR: '
                if error_code == '1':
                    error_message = 'Following sensors have stopped working: '
                    error_message += ', '.join(error_list)
                else:
                    sensor_string = 'Following sensors are above / below their limits: '
                    for sensor_error in error_list:
                        val = sensor_error['Value']
                        sens = sensor_error['Sensor']
                        sensor_string += sens + ': ' + str(val) + 'K, '
                    error_message += sensor_string[:-2]
                try:
                    raise Exception(error_message)
                except Exception as err:
                    self.create_event_log_package(self.service, exception=err)

    """-------------------------- STATUS FUNCTION --------------------------""" 
    
    def respond_status(self, ref):
        """Return the status / data to the tempserver thread"""
        data = {}
        if all(group in self.last_readout_data for group in ['Sensors',
                                                             'Devices']):
            data = self.last_readout_data
            
        self.send_q.put(self.create_package(self.service,
                                            data=data,
                                            cmdref=ref))
        
        # Check after data has been send to webserver
        if data != {}:
            self.check_for_errors(data)
            
            # Reset last readout
            self.last_readout_data = {}
            
            self.create_log_time('publish')
            self.log_timestamps()
            
    """-------------------------- MESSAGE HANDLER --------------------------"""
    
    def message_handler(self, com):
        # Abbrevs
        header = com['HEADER']
        origin = header['FROM']
        cmdtype = header['CMDTYPE']
        package_data = com['BODY']['DATA']
        # Cases
        if (cmdtype == 'STATUS' and 
            origin == 'TempSrv'):
            self.respond_status(header['CMDREFERENCE']) 
        elif cmdtype == 'REQUEST':
            self._ref = header['CMDREFERENCE']
        elif cmdtype == 'SET':
            if origin == 'TempSrv':
                # Update log file only once per second
                # print(package_data)
                if self.campaign_sensors != {}:
                    self.prepare_data_log(package_data)
            elif origin == 'WebSrv':
                # Update the campaign config
                self.update_campaign_config(package_data)
        elif cmdtype  == 'START':
            self.start_campaign(package_data['TIMESTAMP'])
        elif cmdtype  == 'STOP':
            self.stop_campaign(package_data['TIMESTAMP'])
        elif cmdtype == 'INIT':
            self.init_campaign(package_data)

                       
    def run(self):
        # Create the logging thread
        while 1:
            try:
                cmd = self.recv_q.get(timeout=2) 
            except mp.queues.Empty:
                pass  
            else:
                if cmd == 'STOP':
                    break
                else:
                    try:
                        self.message_handler(cmd)
                    except Exception as err:
                        self.create_event_log_package(self.service,
                                                      exception=err)
        
    

###############################################################################
# PROCESS
###############################################################################


def logger_process(services, queues, settings, dummymode):
    service, serv_list = services
    
    # Init class
    Tlo = TempLogger(service, queues, serv_list, settings) 
    Tlo.run()

    
    
