
###############################################################################
# IMPORTS
############################################################################### 

# Time and clock related
import time
from datetime import datetime as dt

# Processes
import multiprocessing as mp
import threading as th
import queue

# Communication Package
from package_broker import PackageHandler

# For random Dummy values
import random 

# Import module sub functions
from module_basics import ModuleFunctions


###############################################################################
# CONSTANTS
###############################################################################

MODULE_METHODS = ['init_device',
                  'open_connection',
                  'close_connection',
                  'request_status',
                  'set_parameter',
                  'init_campaign',
                  'start_campaign',
                  'stop_campaign']

IGNORED_KEYS = ['SensorData']

PyVISA_ERRORS = ['-1073807298', '-1073807360']

###############################################################################
# CLASS
###############################################################################

class DummyClass:
    def __init__(self):
        # Dummy Class to work and test the software backend and frontend
        pass
    
    def dummy_set_parameter(self, channel, parameter, value):
        """Change set value of dummy"""
        
        # Create a shorten variable
        channel_settings = self.ser_cls.device_body['Channel'][channel]
        # channel_settings[parameter]['Set'] = value
        
        channel_settings[parameter]['Read'] = value
        
    def dummy_readout_device(self, device_state, log_request=False):
        device = self.ser_cls.device_body
        if device_state in ['Online', 'Busy']: 
            timestamp = self.mob.create_timestamp()
            device['LastReadout'] = timestamp
            if 'Channel' in device:
                device_channels = device['Channel']
                for channel in device_channels:
                    for parameter in device_channels[channel]:
                        parameter_values = device_channels[channel][parameter]
                        if 'Set' in parameter_values:
                            set_value = parameter_values['Set']
                            parameter_values['Read'] = set_value
                        elif 'Limits' in parameter_values:
                            limits = parameter_values['Limits']
                            parameter_values['Read'] = random.randint(limits[0], 
                                                                      limits[1])
                        else:
                            parameter_values['Read'] = random.randint(10, 12)
                        # device_channels[channel][parameter] = parameter_values
            if log_request and device_state == 'Busy':
                temp_data = {}
                for sens in self.campaign_sensors.keys():
                    temp_data[sens] = random.randint(280, 300)
                device['SensorData'] = {timestamp:temp_data}
                
    def dummy_init_module(self):
        device = self.ser_cls.device_body
        if 'Sensors' in device:
            if device['Sensors'] is None:
                device['Sensors'] = {}
                for sensor_type in device['SensorTypes']:
                    device['Sensors'][sensor_type] = '101-120; A1-A6; B-D'


class ModuleThread(DummyClass):
    def __init__(self, instance, device_id, module_q, dummy):
        super().__init__()
        
        # Extract GENERAL and DEVICE details from body
        self.general_body = instance.body['GENERAL']
        self.device_body = instance.body['ID'][device_id]
        
        self.instance = instance
        self.service = instance.service
        
        # Set the dummy friend
        self.dummy = instance.dummy
        
        #
        self.device_id = device_id
        self.module_q = module_q
        
        # Initialize the module script of the series
        # and add some usefull functions (ModuleFunctions)
        self.mob = ModuleFunctions(instance, self)
        self.ser_cls = instance.service_class(self.mob)
        
        # Define the device body in the module itself
        self.ser_cls.device_body = self.device_body.copy()
        
        # Initialize some dummy parameters
        if self.dummy:
            self.dummy_init_module()

        
        # Start thread
        self.mo = th.Thread(target=self._module_thread)
        self.mo.start()
        
   
    def connect_device(self, value):
        """
        connect or disconnect device
        value:
            True: Connect to device (on startup of this thread)
            False: Disconnect from device
        """
        
        # Get some device connection details (connection type, ip / port etc.)
        device_connection = self.ser_cls.device_body['Connection']
        
        # CURRENTLY A PLACEHOLDER
        self.check_connection_type(device_connection['Type'])
        
        if value:
            # Connect device
            if not self.dummy:
                self.ser_cls.open_connection(device_connection)
                self.ser_cls.init_device()
            self.ser_cls.device_body['LastConnection'] = self.mob.create_timestamp()
            self.ser_cls.device_body['State'] = 'Online'
            
        else:
            # Disconnect device
            if not self.dummy:
                self.ser_cls.close_connection(device_connection)
                
            
    def check_connection_type(self, connection):
        pass
         
            
    """--------------------- CAMPAIGN FUNCTIONS ---------------------"""
    
    def start_campaign(self):
        """
        Start the campaign and set the device state to busy
        """
        # Do some start stuff on the series module side
        if not self.dummy:
            self.ser_cls.start_campaign()

        if self.ser_cls.device_body['State'] == 'Online':
            self.ser_cls.device_body['State'] = 'Busy'
        else:
            raise ConnectionError("Device with ID: "
                                  + self.device_id
                                  + "is offline")
            
    def stop_campaign(self):
        """
        Stop the campaign and set the device state to busy
        """        
        # Do some stop stuff on the series module side
        if not self.dummy:
            self.ser_cls.stop_campaign()        
            
        if self.ser_cls.device_body['State'] == 'Busy':
            self.ser_cls.device_body['State'] = 'Online'
        else:
            raise ConnectionError("Device with ID: "
                                  + self.device_id
                                  + "is offline")
        # Reset the campaign sensors and sensor data
        self.campaign_sensors = {}
        if 'SensorData' in self.ser_cls.device_body:
            self.ser_cls.device_body['SensorData'] = {}

    def init_campaign(self, campaign_details):
        """
        Initialize the campaign like the used sensors (campaign_sensors)
        campaign_details:
            Includes some campaign details, like selected sensors, interval, etc.
        """

        self.campaign_sensors = {}
        self.create_sensor_dict(campaign_details)
            
        # Initialize some stuff on the series module side
        if not self.dummy:
            self.ser_cls.init_campaign(campaign_details)

            
    """--------------------- REQUEST AND GET FUNCTIONS ---------------------"""
    
    def request_status(self, log_request):
        """
        Sends an request to the get the current status of the device
        log_request:
            Mark request interval during an campaign (example every 5 sec)
        """
        
        # Get the current device state ('Offline', 'Online' or 'Busy')
        device_state = self.ser_cls.device_body['State']
        
        if self.dummy:
            self.dummy_readout_device(device_state, log_request)
        else:        
            self.ser_cls.request_status(device_state, log_request)

   
    def set_parameter(self, package_data):
        """
        Update parameters with set value
        """
        # Extract variables from package_data
        channel = package_data['CHANNEL']
        parameter = package_data['PARAMETER']
        value = package_data['VALUE']
        
        para_dict = self.ser_cls.device_body['Channel'][channel][parameter]
        value = self.check_set_parameter(para_dict, value)
        
        ## Send parameter to device with value and channel ##
        if not self.dummy:
            self.ser_cls.set_parameter(channel, parameter, value)
        else:
            self.dummy_set_parameter(channel, parameter, value)
        para_dict['Set'] = package_data['VALUE']

    
    """--------------------- COMMAND FUNCTIONS ---------------------"""    
    
    def commands(self, cmdtype, package_data={}):
        """
        Default commands which are sent to the device on cmdtype
        """
        if cmdtype == 'REQUEST':
            self.request_status(True)
            
        elif cmdtype == 'STATUS':
            self.request_status(False)
            
        elif cmdtype == 'SET':
            # Extract variables from package_data
            self.set_parameter(package_data)
            
        elif cmdtype == 'INIT':
            self.init_campaign(package_data[self.device_id])

        elif cmdtype == 'START':
            # Start campaign and devices #
            self.start_campaign()

        elif cmdtype == 'STOP':
            self.stop_campaign()  
        
        self.module_q['READ'].put(self.ser_cls.device_body)
        
            
    """--------------------------- MODULE INPUT ---------------------------"""
  
    def check_set_parameter(self, para_dict, value): 
        """
        Check if parameter needs to be converted
        """
        value = self.check_temperature_conversion(para_dict, value)
        value = self.check_decimal(para_dict, value)
        return value
  
    def check_decimal(self, parameter, value):
        """
        Check for decimals
        """
        if 'Decimal' in parameter:
            decimal = int(parameter['Decimal'])
            return str(round(value, decimal))
        return value
        
    def check_temperature_conversion(self, parameter, value):
        """
        Check if temperature needs to be converted
        """
        if 'Unit' in parameter:
            unit = parameter['Unit']
            if unit in ['K', '°C', '°F']:
                integer = parameter['InputType'] == 'Int'
                return self.convert_temperature(value, unit, integer)
        # Ignore if it is not a temperature 
        return value

    def convert_temperature(self, temperature, unit='K', integer=False): 
        """
        Convert the temperature into selected unit. 
        Can also be converted into integer.
        """
        if unit == 'K':
            result = temperature
        elif unit == '°C':
            float_temp = float(temperature) - 273.15
            result = self.convert_same_length(float_temp,
                                            temperature)
        elif unit == '°F':
            float_temp = (float(temperature) - 273.15) * 9/5 + 32
            result = self.convert_same_length(float_temp,
                                            temperature)
        if integer:
            return str(int(result))
        else:
            return str(result)
        
    def convert_same_length(self, float_value, string_value):
        return round(float_value, len(string_value.split('.')[1]))
    
    """--------------------------- SUB FUNCTIONS ---------------------------"""           
        
    def create_sensor_dict(self, campaign_details):
        """
        Save sensors of current campaign for each device into a dictionary
        """
        sensor_data = campaign_details['SENSOR']
        self.campaign_sensors = {}
        # Create empty sensor type list
        for sensor_type in sensor_data:
            for sensor in sensor_data[sensor_type]:
                self.campaign_sensors.update({str(sensor):
                                             {'TYPE':sensor_type}})
                    
    """--------------------------- MODULE THREAD ---------------------------"""
                    
    def _module_thread(self):
        """"""
        try:
            self.connect_device(True)
            message = 'Open Connection successfully'
            self.instance.create_event_log_package(self.service,
                                                  device_id = self.device_id,
                                                  event_message=message)
        except Exception as err:
            self.instance.create_event_log_package(self.service, exception=err)
        else:
            self._thread_run()
            
    def _thread_run(self):
        """"""
        while 1:
            try:
                message = self.module_q['SET'].get(timeout=0.5)
            except queue.Empty:
                pass
            else:
                cmdtype = message['CMD']
                packet_data = message['DATA']
                if cmdtype == 'DISCONNECT':
                    try:
                        self.connect_device(False)
                    except Exception as err:
                        self.instance.create_event_log_package(self.service,
                                                               exception=err)
                    time.sleep(0.2)
                    message = 'Closed Connection successfully'
                    self.instance.create_event_log_package(self.service,
                                                           device_id = self.device_id,
                                                           event_message=message)
                    break
                else:
                    try:
                        self.commands(cmdtype, packet_data)
                    except Exception as err:
                        self.instance.create_event_log_package(self.service,
                                                               exception=err)
                        # Intercept I/O error
                        if 'VI_ERROR' in str(err):
                            if any(s in err for s in PyVISA_ERRORS):
                                self.module_q['READ'].put(None)
                                break


class MainService(PackageHandler):
    def __init__(self, service, queues, settings,
                 service_class=None, dummy=False):

        super().__init__(service=service, queues=queues, settings=settings)
        
        self.service = service
        self.service_class = service_class

        # Dummy mode variables
        self.dummy = dummy
        
        try:
            mob = ModuleFunctions(self)
            
            ser_cls = service_class(mob)
            
            self.check_methods(ser_cls)
            
            self.init_service_module()
            self.create_event_log_package(self.service,
                                          event_message='Initialized',
                                          init=True)
        except Exception as err:
            self.create_event_log_package(self.service, exception=err, 
                                          event_message='Initialization Error')
        else:
            self.run()
    
    """--------------------------- INITIAL CHECK ---------------------------"""
    
    def check_methods(self, ser_cls):
        """
        Check if series class includes all needed methods
        """
        for method_name in MODULE_METHODS:
           if not self.has_method(ser_cls, method_name):
               raise Exception(self.service + ': The method "'
                               + method_name + '" is missing.')
    
    def has_method(self, ser_cls, method_name):
        """
        Check if class has method
        """
        return hasattr(ser_cls, method_name)


    """------------------------- MODULE FUNCTIONS -------------------------""" 

    # START AND STOP MODULE
    def init_service_module(self):
        """
        Initialize the module, creating default parameters
        """

        self.campaign_instrs = []
        
        # Get group of service
        for group in self.settings:
            if self.service in self.settings[group]:
                break  
        self.group = group
        self.body = self.settings[group][self.service]
        
        self.module_threads = {}
        self.module_q = {}
        for device_id in self.body['ID']:
            self.module_q[device_id] = {'READ':queue.Queue(),
                                        'SET':queue.Queue()}

    def stop_module(self):
        """
        Stop this module and its devices
        """
        try:
            self.ask('DISCONNECT')
        except Exception as err:
            self.create_event_log_package(self.service, exception=err)
        
                   
    def ask(self, cmdtype, device_ids=[], package_data={}):
        """
        A request to get current status or data from module / devices
        """
        if device_ids == []:
            device_ids = self.module_threads

        for did in device_ids:
            self.module_q[did]['SET'].put({'CMD':cmdtype,
                                           'DATA':package_data})

    def send(self, cmdtype, package_data):
        """

        """
        if cmdtype in ['INIT', 'START', 'STOP']:
            if cmdtype == 'INIT':
                self.campaign_instrs = list(package_data.keys())
            elif cmdtype == 'START' and self.campaign_instrs == []:
                # raise Warning("Devices have not been initialized")
                return
                
            self.ask(cmdtype, self.campaign_instrs, package_data)
            
            if cmdtype == 'STOP':       
                self.campaign_instrs = []
        else:
            self.ask(cmdtype, [package_data['ID']], package_data)
    
    def get(self, timeout):
        """
        
        """
        for device_id in self.module_threads.copy():
            try:
                msg = self.module_q[device_id]['READ'].get(timeout=timeout)
            except queue.Empty:
                pass
            else:
                if msg == None:
                    self.start_module_thread({'ID':device_id, 'VALUE':False})
                else:
                    device_body = msg.copy()
                    # self.check_body(device_body, device_id)
                    device_body = self.check_connection_timestamp(device_body)
                    self.body['ID'][device_id] = device_body.copy()
        return self.body
                


    """--------------------------- MODULE OUTPUT ---------------------------""" 
    
    def check_connection_timestamp(self, device_body):
        """
        Update the LastConnection parameter if new data has been readout
        """
        if (device_body['LastReadout'] != ''
            and device_body['LastConnection'] != ''):
            last_conn = device_body['LastConnection']
            last_read = device_body['LastReadout']

            datetime_last_conn = (dt.strptime(last_conn,
                                              '%Y-%m-%dT%H:%M:%S'))
            datetime_last_read = (dt.strptime(last_read,
                                              '%Y-%m-%dT%H:%M:%S'))
            
            if datetime_last_read > datetime_last_conn:
                device_body['LastConnection'] = last_read
        elif (device_body['LastReadout'] != ''
              and device_body['LastConnection'] == ''):
            device_body['LastConnection'] = device_body['LastReadout']
        return device_body
                
    
    def check_body(self, device_body, device_id):
        """
        Check if input_body (self.body) has still the some structure
        of the output_body
        """
        if not self.compare_dict_keys(self.body['ID'][device_id], device_body,
                                      ignore_keys=IGNORED_KEYS):
            raise Exception("Not allowed structure changes have been applied"
                            + " to the settings dictionary (body)")
        

    def is_valid_timestamp(self, timestamp):
        """
        Check if the timestamp is given in a valid format
        """
        try:
            # Try to parse the timestamp with the given format
            dt.strptime(timestamp, "%Y-%m-%dT%H:%M:%S")
            return True
        except ValueError:
            # If parsing fails, the timestamp is not in the correct format
            return False

    
    def compare_dict_keys(self, input_body, output_body, ignore_keys=[]):
        """
        Compare two dictionaries to check if they have the same keys and subkeys.
        Return True if both dictionaries have the same structure of keys,
        False otherwise.
        """
        if input_body.keys() != output_body.keys():
            return False
        
        for key in input_body.keys():
            if key not in ignore_keys:
                if (isinstance(input_body[key], dict)
                    and isinstance(output_body[key], dict)):     
                    if not self.compare_dict_keys(input_body[key],
                                                  output_body[key],
                                                  ignore_keys=ignore_keys):
                        return False
                elif (isinstance(input_body[key], dict)
                      or isinstance(output_body[key], dict)):
                    return False
        
        return True
    

    """-------------------------- MESSAGE HANDLER --------------------------""" 
    
    def convert_package_data(self, service, data):
        """"""
        return {self.group:{service:data}}
     
    def handler(self, msg):
        """"""
        # Abbrevs
        header = msg['HEADER']
        cmdtype = header['CMDTYPE']
        package_data = msg['BODY']['DATA']
        service = header['TO']
        reference = header['CMDREFERENCE']    
        
        # Cases
        if cmdtype in ['STATUS', 'REQUEST']:
            try:
                self.ask(cmdtype)
                                
                if cmdtype == 'STATUS':
                    timeout = 0.01
                else:
                    self.create_log_time('req')
                    timeout = 0.2
                    
                data = self.get(timeout)
                
                # Stop signal from module
                if data == 'STOP':
                    return True

                send_data = self.convert_package_data(service, data)
                
            except Exception as err:
                self.create_event_log_package(self.service, exception=err)
                send_data = {}
                
            package = self.create_package(service,
                                          data=send_data,
                                          cmdref=reference)
            self.send_q.put(package)
            if cmdtype == 'REQUEST':
                self.create_log_time('resp')
                self.log_timestamps()
                
        elif cmdtype in ['SET', 'CONNECT', 'START', 'STOP', 'INIT']:
            try:
                if cmdtype == 'CONNECT':
                    self.start_module_thread(package_data)
                else:
                    self.send(cmdtype, package_data)
                
            except Exception as err:
                self.create_event_log_package(self.service, exception=err)
    

    """--------------------------- MODULE THREAD ---------------------------""" 
    
    def start_module_thread(self, package_data):
        """
        Request to connect to device (VALUE: True) or disconnect (VALUE: False)
        """
        device_id = package_data['ID']
        module_q = self.module_q[device_id]
        if package_data['VALUE'] and device_id not in self.module_threads:
            self.module_threads[device_id] = ModuleThread(self, device_id,
                                                          module_q, self.dummy)
            
        elif package_data['VALUE'] == False:
            module_q['SET'].put({'CMD':'DISCONNECT', 'DATA':{}})
            self.module_threads.pop(device_id)
            self.body['ID'][device_id]['State'] = 'Offline'
    
    """--------------------------- MAIN PROCESS ---------------------------""" 
    

    def run(self):
        """"""
        while 1:
            try:
                msg = self.recv_q.get(timeout=0.5)
            except mp.queues.Empty:
                pass
            else:
                if msg == "STOP":
                    self.stop_module()
                    time.sleep(1)
                    break
                else:
                    if self.handler(msg):
                        break
                
    
    
###############################################################################
# PROCESS
###############################################################################
 
def service_process(device_class, services, queues, settings, dummymode):
    service, serv_list = services
    
    MainService(service, queues, settings, 
                service_class = device_class, 
                dummy = dummymode)

