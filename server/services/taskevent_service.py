
###############################################################################
# IMPORTS
###############################################################################

# Time and clock related
from datetime import timedelta
from datetime import datetime as dt
import time

# System and os
import os

# Math
import math

# Thread and processes
import multiprocessing as mp

# Scriptlib
import file_functions as ffu

# Package
from package_broker import PackageHandler

###############################################################################
# CLASS
###############################################################################

class ConditionBox:
    def temperature_stability(self, condition, package_data, con_num):
        """"""
        fulfilled = {}
        sensors = package_data['Sensors']['Data']
        timestamp = package_data['Sensors']['Timestamp']
        
        for sub_con in condition['VARIABLE']:
            sensor_id = sub_con['Sensor ID']
            timeout = int(sub_con['Period'])
            temp_change = float(sub_con['Temperature Range'])
            value = float(sensors[sensor_id])
            fulfilled[sensor_id] = False
            
            if sensor_id not in self.task_cache:
                self.task_cache[sensor_id] = {'Timestamp':[], 'Data':[]}     
                
            self.task_cache[sensor_id]['Timestamp'].append(timestamp)
            self.task_cache[sensor_id]['Data'].append(value)
            dt_now = self.string_to_datetime(timestamp)
            dt_first = self.string_to_datetime(self.task_cache[sensor_id]['Timestamp'][0])
            
            if dt_now - dt_first >= timedelta(seconds = timeout):
                # Check if within a certain time the temperature change is 
                # smaller than the given range
                float_list = [float(i) for i in self.task_cache[sensor_id]['Data']]
                minimum = min(float_list)
                maximum = max(float_list)
                diff = maximum-minimum
                
                self.condition_var['Max Temp Diff in Period'][sensor_id] = f"{diff:.2f}"
                if maximum-minimum <= temp_change:
                    fulfilled[sensor_id] = True
                else:
                    fulfilled[sensor_id] = False
                # Pop first Element
                self.task_cache[sensor_id]['Timestamp'].pop(0)
                self.task_cache[sensor_id]['Data'].pop(0)
                
        all_true = all(value for value in fulfilled.values())
        if all_true:
            self.condition_status[con_num] = True
    
    def temperature_limit(self, condition, package_data, con_num):
        """"""
        fulfilled = {}
        sensors = package_data['Sensors']['Data']
        for sub_con in condition['VARIABLE']:
            sensor_id = sub_con['Sensor ID']
            temp = float(sub_con['Temperature'])
            value = float(sensors[sensor_id])
            fulfilled[sensor_id] = False
            if 'Below' in condition['TYPE']:
                if value < temp:
                    fulfilled[sensor_id] = True
            else:
                if value > temp:
                    fulfilled[sensor_id] = True   
        all_true = all(value for value in fulfilled.values())
        if all_true:
            self.condition_status[con_num] = True
    
    def relative_time(self, condition, con_num):
        """"""
        timeout = int(condition['VARIABLE'])
        # Check if relative time is over
        if abs(dt.utcnow()-self.task_start) >= timedelta(seconds = timeout):
            self.condition_status[con_num] = True
        


class TaskEvent(PackageHandler, ConditionBox):   
    def __init__(self, service, queues, serv_list, settings):

        super().__init__(service=service, queues=queues, settings=settings)
        super().__init__()
        
        #
        self.task_pos = 1
        self.current_work = {}
        self.task_start = ''
        self.task_cache = {}
        self.iteration_var = {'Pos':0, 'End':0}
        self.condition_var = {}
        
        self.initialization()
        self.create_event_log_package(self.service,
                                      event_message='Initialized',
                                      init=True)
        
    """-------------------------- INITIALIZATION --------------------------""" 
    
    def initialization(self):
        """Call all initial functions"""
        self.event_logging = self.settings['campaign']['Active'] 
        
        if self.event_logging:
            self.current_campaign = self.settings['campaign']['CurrentCampaign']
            self.campaign_folder = os.path.join(self.logpaths['campaign'],
                                                self.current_campaign)
            self.condition_var = {'Passed Seconds':{},
                                  'Max Temp Diff in Period':{}}

    """----------------------------- EVENT LOG -----------------------------""" 

    def update_event_log(self, package_data, trigger='User'):
        """Update the event log"""
        if self.event_logging:
            header = ['Timestamp [UTC]','EventType','Trigger']
            timestamp = self.time_now_as_string()
            # 
            event_log = [timestamp, 'Set', trigger]
            for key in ['ID', 'CHANNEL', 'PARAMETER', 'VALUE']:
                if key == 'ID':
                    header.append('Device ID')
                else:
                    header.append(key)
                event_log.append(str(package_data[key]))
            
            # Create log filename
            folder = self.campaign_folder
            log_filename = os.path.join(folder, 'event.log')
            # Check if file exists
            exist = os.path.exists(log_filename)
             
            # If file does not exist add header
            log_text = ''
            if not exist:
                log_text = '\t'.join(header) + '\n'
                
            # Write log
            log_text += '\t'.join(event_log) + '\n'
    
            ffu.append_ascii_file(log_filename, log_text)
            

    """------------------------ CAMPAIGN FUNCTIONS ------------------------""" 

    def init_campaign(self, data):
        """Init the campaign"""
        self.current_campaign = data['ID']
        self.campaign_folder = os.path.join(self.logpaths['campaign'],
                                            self.current_campaign)

    def start_campaign(self):
        """Start the campaign"""
        self.event_logging = True
        
    def stop_campaign(self):
        """Stop the campaign"""
        self.event_logging = False
        
    """-------------------------- WORK FUNCTIONS --------------------------""" 

    def save_work(self, package_data):
        """"""
        filename = package_data['ID'] + '.yaml'
        file_path = os.path.join(self.logpaths['work'], filename)
        for key in package_data.copy():
            if key == 'NOI':
                package_data['NoI'] = int(package_data.pop(key))
            elif key != 'ID':
                package_data[key.capitalize()] = package_data.pop(key)
            
        package_data['VERSION'] = self.settings['version']
        #
        ffu.write_yaml_file(file_path, package_data)
        # Append new work to list file (work.txt)
        work_file = os.path.join(self.logpaths['work'], 'work.txt')
        # Check if work has already been defined before adding to list file
        work_list = ffu.read_ascii_file(work_file)
        if package_data['ID'] not in work_list:
            ffu.append_ascii_file(work_file, package_data['ID'] + '\n')

    def start_work(self, package_data):
        """Start work and some parameters"""
        filename = package_data['ID'] + '.yaml'
        file_path = os.path.join(self.logpaths['work'], filename)
        self.current_work = ffu.read_yaml_file(file_path)
        
        # Do some initial steps before starting
        statement = self.current_work['Init']
        self.do_steps(statement)
        
        # Initialize parameter
        self.task_pos = 1
        self.task_start = dt.utcnow()
        
        loop_end = int(package_data['NOI'])
        self.iteration_var['Pos'] = 1
        if loop_end <= 0:
            self.iteration_var['End'] = math.inf
        else:
            self.iteration_var['End'] = loop_end
        
        self.condition_var = {'Passed Seconds':{},
                              'Max Temp Diff in Period':{}}
        
        # Start message
        event_message = ("Started work '"
                         + self.current_work['Name'] + "'")
        self.create_event_log_package(self.service,
                                      event_message=event_message)
        
    def stop_work(self, user_stop=False):
        """Stop work and reset some parameters"""
        
        if user_stop == False:
            # Do some final steps before stop the work
            statement = self.current_work['Final']
            self.do_steps(statement)
            # Success message
            event_message = ("Successfully finished work '"
                             + self.current_work['Name'] + "'")
            self.create_event_log_package(self.service,
                                          event_message=event_message)
            
        # Reset parameters
        self.task_start = ''
        self.iteration_var = {'Pos':0, 'End':0}
        self.task_pos = 1
        self.current_work = {}
        self.task_cache = {}
        self.condition_var = {}
        
        
    def check_task(self, package_data={}):
        """
        Check if, at the current state, the conditions for the task have been
        fulfilled and start doing the jobs (statements)
        """
        currentTask = self.current_work['Tasks'][str(self.task_pos)]
        
        # Get list of conditions and statements
        condition_list = currentTask['Condition']
        statement_list = currentTask['Statement']
        
        # Set the status for each conditions (Fulffiled status = True)
        self.condition_status = {}
        logic_gate = {}
        con_num = 1
        
        for condition in condition_list:
            # Add to logic
            if condition['LOGIC'] not in logic_gate:
                logic_gate[condition['LOGIC']] = [con_num]
            else:
                logic_gate[condition['LOGIC']].append(con_num)
            # Initial status of condtion
            self.condition_status[con_num] = False
            # Calculate passed time (after start of task) in seconds
            self.condition_var['Passed Seconds'] = (dt.utcnow()-self.task_start).seconds
            # Check the several conditions if defined in task
            if condition['TYPE'] == 'Relative Time':
                self.relative_time(condition, con_num)
            elif (condition['TYPE'] == 'Temperature Stability'
                  and package_data != {}):
                self.temperature_stability(condition, package_data, con_num)
            elif (condition['TYPE'] in ['Temperature Below', 'Temperature Above']
                  and package_data != {}):
                self.temperature_limit(condition, package_data, con_num)
            con_num += 1
            
        # If all conditions are fullfilled
        for logic_num in logic_gate:
            if self.check_logic_gate(logic_gate[logic_num]):
                self.do_task(statement_list)
                break
     
    def check_logic_gate(self, logic_list):
        """"""
        for num in logic_list:
            if self.condition_status.get(num) is not True:
                return False
        return True
            
                      
    def do_task(self, statement):
        """"""
        # Do task if condition is fullfilled
        self.do_steps(statement)
        # Do next task or start next iteration or stop work
        if self.task_pos == len(self.current_work['Tasks']):
            self.task_pos = 1
            if self.iteration_var['Pos'] == self.iteration_var['End']:
                self.stop_work()
            else:
                if self.iteration_var['Pos'] < self.iteration_var['End']:
                    self.iteration_var['Pos'] += 1
                self.task_start = dt.utcnow()
        else:
            self.task_pos += 1
            self.task_start = dt.utcnow()
        # Reset some saved variables from the last task
        self.task_cache = {}
        
    def do_steps(self, statement, delay=0.1):
        """Prepare and send each step in a statement to the defined module"""
        for step in statement:
            self.create_step_package(step)
            self.update_event_log(step, trigger='Task')
            time.sleep(delay) # Wait delay in seconds before next task sent
        
    
    """--------------------------- SUB FUNCTIONS ---------------------------""" 

    def time_now_as_string(self):
        """Return current time to ISO 8601 timestamp"""
        return dt.utcnow().strftime('%Y-%m-%dT%H:%M:%S')
    
    def string_to_datetime(self, date_string):
        """Convert string timestamp into datetime like timestamp"""
        return dt.strptime(date_string, '%Y-%m-%dT%H:%M:%S')
    

    """-------------------------- STATUS FUNCTION --------------------------""" 
        
    def create_step_package(self, data):
        """Create package and send it to tempserver"""
        package = self.create_package(self.service, 
                                     receiver=data['SERIES'], 
                                     data=data, 
                                     cmdtype='SET')
        self.send_q.put(package)
    
    def respond_status(self, ref):
        """"""
        # Initialize some work parameter
        active = False
        started_task = ''
        current_work_id = ''
        # Get the current status of the work / tasks
        if self.task_start != '':
            self.check_task()
            active = True
            if self.task_start != '':
                started_task = self.task_start.strftime('%Y-%m-%dT%H:%M:%S')
        if self.current_work != {}:
            current_work_id = self.current_work['ID']
          
        iteration_pos = (str(self.iteration_var['Pos']) + ' / '
                         + str(self.iteration_var['End']))
        # Create the data_package
        data = {'work':{'CurrentTask':self.task_pos,
                        'CurrentWork':current_work_id,
                        'Active':active,
                        'StartedTask':started_task,
                        'Iteration':iteration_pos,
                        'ConditionVariable':self.condition_var}}
        #
        package = self.create_package(self.service, 
                                     data=data, 
                                     cmdref=ref)
        self.send_q.put(package)
        

    """-------------------------- MESSAGE HANDLER --------------------------"""
    
    def message_handler(self, com):
        """"""
        # Abbrevs
        header = com['HEADER']
        origin = header['FROM']
        cmdtype = header['CMDTYPE']
        package_data = com['BODY']['DATA']
        # Cases
        if (cmdtype in ['STATUS', 'REQUEST'] and 
            origin == 'TempSrv'):
            self.respond_status(header['CMDREFERENCE']) 
        elif cmdtype == 'SET':
            if origin == 'TempSrv':
                if self.task_start != '':
                    self.check_task(package_data)
            else:
                self.update_event_log(package_data)
        elif cmdtype  == 'START':
            if 'DEVICES' in package_data:
                self.start_campaign()
            else:
                self.start_work(package_data)
        elif cmdtype  == 'STOP':
            if 'DEVICES' in package_data:
                self.stop_campaign()
            else:
                self.stop_work(True)
        elif cmdtype == 'INIT':
            if 'DEVICES' in package_data:
                self.init_campaign(package_data)
            else:
                self.save_work(package_data)
    
                
    def run(self):
        """"""
        # Create logging thread
        while 1:
            try:
                cmd = self.recv_q.get(timeout=2)
                if cmd == 'STOP':
                    break
                else:
                    self.message_handler(cmd)
            except mp.queues.Empty:
                pass               
            except Exception as err:
                self.create_event_log_package(self.service, exception=err)
        

###############################################################################
# PROCESS
###############################################################################

def taskevent_process(services, queues, settings, dummymode):
    """"""
    service, serv_list = services
    
    # Init class
    Tke = TaskEvent(service, queues, serv_list, settings) 
    Tke.run()
    
    
    
