
###############################################################################
# IMPORTS
###############################################################################

# Time and clock related
from datetime import timedelta
from datetime import datetime as dt

# Thread and processes
import multiprocessing as mp
import threading as th
import queue

# Package
from package_broker import PackageHandler

# 
import init_messengers as inm


###############################################################################
# CLASS
###############################################################################

class Messenger(PackageHandler):
    def __init__(self, service, queues, serv_list, settings):
        super().__init__(queues=queues, settings=settings)
        
        self.service = service
        
        ## MSG Variables ###
        self.msg_cache = {}
        self.msg_entry_list = {}
        
        #
        self.time_window = 3
        
        ## Queue ##
        self.msg_q = queue.Queue()
        
        # Initialization Message
        self.create_event_log_package(self.service,
                                      event_message='Initialized',
                                      init=True)


    """------------------------- MESSAGE FUNCTIONS -------------------------"""
    
    def initial_messenger(self):
        """Initialization of the messenger"""
        #
        self.courier = inm.Courier()

        # CURRENTLY ONLY DESIGNED FOR EMAIL!!!
        message_service = 'email'
        messenger_settings = self.settings['messenger']
        if 'GENERAL' in messenger_settings[message_service]:
            if messenger_settings[message_service]['GENERAL']['active']:
                self.courier.setup_messenger(message_service,
                                             messenger_settings[message_service])
                ## NEEDS TO BE CHANGED FOR OTHER MESSENGERS!!!
                self.recipients = messenger_settings[message_service]['RECIPIENTS']['receiver_list']
                return True
        return False
    
    def process_message(self, log_entry):
        """"""
        # Extract the relevant fields from the log entry
        timestamp = log_entry['TIMESTAMP']
        service = log_entry['SERVICE']
        device_id = log_entry['ID']
        tag = log_entry['TAG']
        message = log_entry['MESSAGE']
        
        log_entry['STATUS'] = '----'

        # Parse the timestamp string into a datetime object
        timestamp_dt = dt.fromisoformat(timestamp)
        
        # Check if the tag is 'ERROR' and not from messenger itself 
        if tag == 'ERROR' and service != self.service:
            if tag not in self.msg_cache:
                self.msg_cache[tag] = {}
            if tag not in self.msg_entry_list:
                self.msg_entry_list[tag] = []
                
            # Ensure the service has an entry in the msg_cache
            if service not in self.msg_cache[tag]:
                self.msg_cache[tag][service] = {}

            # Check if this error message has been seen before for this service
            if message in self.msg_cache[tag][service]:
                last_seen_id, last_timestamp = self.msg_cache[tag][service][message]
                # Check if the error occurred within the defined time window
                if timestamp_dt - last_timestamp < timedelta(seconds=self.time_window):
                    # If it's within the time window, ignore it
                    self.msg_cache[tag][service][message] = (last_seen_id,
                                                             timestamp_dt)
                    # Check if error is an continuous error
                    self.error_list_check(message, tag)
                    return
            
            # If it's a new error or the time window has passed
            # Update the error cache with the new timestamp and reset the time window
            self.msg_cache[tag][service][message] = (device_id, timestamp_dt)
            self.msg_entry_list[tag].append(log_entry)
            self.last_timestamp = dt.now()
            
        elif tag == 'INFO':
            if service == 'TaskEv':
                if 'Successfully' in message:
                    di = {}
                    di[tag] = [log_entry]
                    self.msg_q.put(di)
                    

    def error_list_check(self, error_message, tag):
        """Check if status of error already active"""
        for log_entry in self.msg_entry_list[tag]:
            if error_message == log_entry['MESSAGE']:
                log_entry['STATUS'] = 'continuous'
                
    def create_message_text(self, msg):
        """Convert dictionary into string message text"""
        # Minimum char length
        min_length = {'TIMESTAMP':30,
                      'SERVICE':15,
                      'ID':15,
                      'TAG':15,
                      'STATUS':15,
                      'MESSAGE':0}
        
        # Init message header and content
        msg_header = ''
        msg_content = ''

        for tag in msg:
            if msg[tag] != []:
                for i in range(len(msg[tag])):
                    for key in min_length:
                        val = str(msg[tag][i][key])
                        if key == 'TIMESTAMP':
                            msg_content += val + (min_length[key]
                                               -len(val))*' '
                            if msg_header == '':
                                msg_header += (key + ' [UTC]'
                                               + (min_length[key]
                                                  -len(key)-6)*' ')
                        else:
                            if key == 'MESSAGE':
                                msg_content += val
                            else:
                                msg_content += val + (min_length[key]-len(val))*' '
                            if msg_header != '':
                                msg_header += key + (min_length[key]-len(key))*' '
                    msg_content += '\n'
            
            if msg_header != '':        
                # Create a full message
                full_message = ("This is an automated notification. "
                                + "Please review the following information:"
                                + "\n\n")
                full_message += msg_header + '\n' + msg_content
                full_message += ('\n'*3 + 'Thank You for your attention\n'
                                 + 'Your Temperature System')
                # print(full_message)
                # Send email
                self.courier.send_message(self.recipients,
                                          full_message,
                                          tag + " Notification")


    def messenger_thread(self):
        """Send the message to the receiver in this thread"""
        while 1:
            try:
                msg = self.msg_q.get()
                if msg == 'STOP':
                    break
                else:
                    self.create_message_text(msg)
            except queue.Empty:
                pass
            except Exception as err:
                self.create_event_log_package(self.service, exception=err)
     
    """-------------------------- STATUS FUNCTION --------------------------""" 
    
    def respond_status(self, ref):
        """Respond the status to the tempserver"""
        self.send_q.put(self.create_package(self.service,
                                            data={},
                                            cmdref=ref))
        
    """-------------------------- MESSAGE HANDLER --------------------------"""
    
    def message_handler(self, com, active=False):
        """"""
        # Abbrevs
        header = com['HEADER']
        origin = header['FROM']
        cmdtype = header['CMDTYPE']
        package_data = com['BODY']['DATA']
        # Cases
        if (cmdtype in ['STATUS', 'REQUEST'] and 
            origin == 'TempSrv'):
            self.respond_status(header['CMDREFERENCE']) 
        elif (cmdtype == 'SET' and 
              origin == 'TempSrv'):
            if active:
                self.process_message(package_data)
            
        
    def run(self):
        """"""
        th.Thread(target=self.messenger_thread).start()
        
        try:
            active = self.initial_messenger()
        except Exception as err:
            self.create_event_log_package(self.service, exception=err)
            active = False

        self.last_timestamp = dt.now()
        
        while 1:
            try:
                msg = self.recv_q.get(timeout=2)
                if msg == 'STOP':
                    self.msg_q.put(msg)
                    break
                else:
                    self.message_handler(msg, active)  
            except mp.queues.Empty:
                pass
            except Exception as err:
                self.create_event_log_package(self.service, exception=err)
                
            #
            if (timedelta(seconds=5) < (dt.now() - self.last_timestamp)
                and self.msg_entry_list != {}):
                self.msg_q.put(self.msg_entry_list)
                self.msg_entry_list = {}
        

###############################################################################
# PROCESS
###############################################################################


def messenger_process(services, queues, settings, dummymode):
    service, serv_list = services
    
    # Init class
    Msg = Messenger(service, queues, serv_list, settings) 
    Msg.run()

