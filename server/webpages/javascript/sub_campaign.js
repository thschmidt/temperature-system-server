/* 

*/

//--------------------------------------------------------------------
// GLOBAL VARIABLE
//--------------------------------------------------------------------


//------------------------------ UPDATE CAMPAIGN --------------------------------------

function createDeviceTables(devices, group, edit=false){
    for (var series in devices){
        seriesData = devices[series]
        var tableHeader = document.createElement('h3')
        tableHeader.id = group + '-' + series + '-deviceHeader'
        tableHeader.className = 'device-header'
        tableHeader.innerHTML = series
        general = Init_The_Settings[group][series]['GENERAL']

        if ('Details' in general){
            if ('Name' in general['Details']){
                tableHeader.innerHTML = general['Details']['Name']
            }   
        }
        var table = document.createElement('table')
        table.id = group + '-' + series + '-deviceTable'

        table.className = "device-table"
        const tbody = document.createElement('tbody');

        
        const headers = ['ID', 'Connection', 'State', 'LastConnection', 'ActiveSensors']

        const headerRow = document.createElement("tr");
        headers.forEach((headerText) => {
            const headerCell = document.createElement("th");
            headerCell.textContent = headerText;
            headerRow.append(headerCell);
        });
        tbody.appendChild(headerRow)

        for (var deviceId in seriesData){     
            var device = Init_The_Settings[group][series]['ID'][deviceId]
            const row = tbody.insertRow();

            // Iterate over dictionary
            j = 0
            for (key of headers) {  
                const cell = row.insertCell(j);
                try {
                    if (key == 'ID'){  
                        cell.textContent = deviceId;
                    } else if (key == 'Connection'){
                        cell.textContent = device[key]['Type'];
                    } else if (key == 'ActiveSensors'){
                        cell.textContent = seriesData[deviceId]['ActiveSensors'];
                    } else if (['Name', 'Description'].includes(key) && edit){
                        var nameDesInput = document.createElement("input")
                        if (device[key] != undefined){
                            nameDesInput.value = device[key]
                        }
                        //cell.id = deviceId + '-' + key
                        cell.appendChild(nameDesInput);
                    } else {
                        cell.id = deviceId + '-' + key
                        cell.textContent = device[key]
                    }
                } catch {
                    cell.title = 'Empty due to older format or unknown device ID'
                }
                j++
            }
        }
        table.appendChild(tbody);
        document.getElementById(group + '-deviceContainer').appendChild(tableHeader)
        document.getElementById(group + '-deviceContainer').appendChild(table)
    }
}



function createSensorTable(sensors, group, edit=false){
    var table = '<table id="' + group + '-sensorTable" class="sensor-table"><tr>';
    var j = 0
    var temperature_unit = getDefaultTemperatureUnit()
    var limits = [convertTemperature(0, temperature_unit),convertTemperature(1000, temperature_unit)]

    for (var series in sensors) {
        for (var sensor in sensors[series]){
            var row = sensors[series][sensor]
            if (j == 0) {
                table += '<th>ID</th><th>Name</th><th>Description</th><th>Group</th><th>Sensor Type</th>'+
                '<th>Lower Limit</th><th>Upper Limit</th><th>State</th>'+
                '<th>Style Colour</th><th>Style Line</th>';
                table += '</tr>';
            }
            // Data Rows
            if (edit){
                table += '<tr id="row-' + series + '">';
                table += '<td>' + sensor + '</td>';
                table += '<td><input type="text" value="' + row.Name + '" maxlength="30" </td>';
                table += '<td><input type="text" value="' + row.Description + '" maxlength="100" </td>';
                table += '<td><input type="text" value="' + row.Group + '" maxlength="30" </td>';
                table += '<td>' + row.SensorType + ' </td>';
                table += ('<td><input type="number" value='
                        + convertTemperature(row.Limit.Lower, temperature_unit)
                        + ' min=' + limits[0] + ' max=' + limits[1] + ' </td>');
                table += ('<td><input type="number" value='
                        + convertTemperature(row.Limit.Upper, temperature_unit)
                        + ' min=' + limits[0] + ' max=' + limits[1] + ' </td>');
                if ('State' in row){
                    table += '<td>' + row.State + '</td>';
                } else {
                    table += '<td>Normal</td>';
                }
                table += '<td><input type="color" value=' + row.Style.Colour + ' </td>';
                table += '<td><select>' + showLineStyles(row.Style.Line) + '</select></td>';
                table += '</tr>';
            }
            else {
                table += '<tr>';
                table += '<td>' + sensor + '</td>';
                table += '<td>' + row.Name + '</td>';
                table += '<td>' + row.Description + '</td>';
                table += '<td>' + row.Group + '</td>';
                table += '<td>' + row.SensorType + '</td>';
                table += '<td>' + convertTemperature(row.Limit.Lower, temperature_unit) + '</td>';
                table += '<td>' + convertTemperature(row.Limit.Upper, temperature_unit) + '</td>';
                if ('State' in row){
                    table += '<td>' + row.State + '</td>';
                } else {
                    table += '<td>Normal</td>';
                }
                table += '<td class="color-swatch" style="background-color: ' + row.Style.Colour + ';" ></td>';
                table += '<td>' + row.Style.Line + '</td>';
                table += '</tr>';
            }
            j++
        }
    }
    table += '</table>';
    document.getElementById(group + '-sensorContainer').innerHTML = table;
}

function selectCampaign(){
    var campaignSelect = document.getElementById("campaignSelect");
    var selectedCampaign = campaignSelect.options[campaignSelect.selectedIndex].value;
    document.getElementById('data_logger-header').innerHTML = '';
    document.getElementById('temp_control-header').innerHTML = '';
    recvCampaignData(selectedCampaign)
}

function updateCampaignDetails(dictionary) {
    document.getElementById('detailsBox').textContent = ''
    const table = document.createElement('table');
    table.id = 'campaignDetails'

    const tbody = document.createElement('tbody');
    
    // Iterate over dictionary
    for (const [key, value] of Object.entries(dictionary)) {  
        const row = tbody.insertRow();
        const cell1 = row.insertCell(0);
        const cell2 = row.insertCell(1);
        if (key !== 'Name'){  
            cell1.textContent = key;
            cell2.textContent = value;
        }
        else {
            var campaignSelect = document.getElementById("campaignSelect");
            cell1.textContent = 'Name';
            cell2.textContent = campaignSelect.options[campaignSelect.selectedIndex].value;
        }
    }
    table.appendChild(tbody);
    document.getElementById('detailsBox').appendChild(table);
}


function createContainer(group){
    var groupContainerHeader = document.getElementById(`${group}-header`);
    groupHeader = document.createElement('h2')
    groupHeader.setAttribute("onclick", "toggleSection(this)");
    if (group == 'data_logger'){
        groupHeader.innerHTML = "Data Logger"
    }
    else {
        groupHeader.innerHTML = "Temperature Controller"
    }
    groupContainerHeader.appendChild(groupHeader);
    var groupContainer = document.createElement('div');
    groupContainer.id = `${group}-container`;
    groupContainer.className = 'group-container';

    deviceContainerHeader = document.createElement('h3')
    deviceContainerHeader.id = `${group}-deviceContainerHeader`;
    deviceContainerHeader.innerHTML = 'Device';
    deviceContainerHeader.className = 'device-container-header';
    deviceContainerHeader.setAttribute("onclick", "toggleSection(this)");
    deviceContainer = document.createElement('div');
    deviceContainer.id = `${group}-deviceContainer`;
    deviceContainer.className = `device-container`;
    groupContainer.appendChild(deviceContainerHeader);
    groupContainer.appendChild(deviceContainer);

    sensorContainerHeader = document.createElement('h3');
    sensorContainerHeader.id = `${group}-sensorContainerHeader`;
    sensorContainerHeader.className = 'sensor-container-header';
    sensorContainerHeader.innerHTML = 'Sensor';
    sensorContainerHeader.setAttribute("onclick", "toggleSection(this)");
    sensorContainer = document.createElement('div');
    sensorContainer.id = `${group}-sensorContainer`;
    sensorContainer.className = `sensor-container`;
    groupContainer.appendChild(sensorContainerHeader);
    groupContainer.appendChild(sensorContainer);
    groupContainerHeader.appendChild(groupContainer);
}

// Function to toggle display of device content
function toggleSection(header) {
    content = header.nextElementSibling
    content.style.display = content.style.display === "none" ? "block" : "none"
}



function removeFromDictionary(dict, keysToRemove) {
    // Create a new object to hold the modified dictionary
    let newDict = {};
  
    // Copy all key-value pairs except the ones to be removed
    for (const key in dict) {
        if (keysToRemove.includes(key) == false) {
            newDict[key] = dict[key];
        }
    }

    return newDict;
}


function updateData(statusData){
    for (group in statusData){
        if (group != 'website'){
            for (series in statusData[group]){
                for (deviceId in statusData[group][series]['ID']){
                    deviceData = statusData[group][series]['ID'][deviceId];
                    connectElement = document.getElementById(deviceId + '-LastConnection');
                    if (connectElement !== null) {
                        connectElement.innerHTML = deviceData['LastConnection'];
                    }
                    stateElement = document.getElementById(deviceId + '-State');
                    if (stateElement !== null) {
                        stateElement.innerHTML = deviceData['State'];
                    }    
                } 
            } 
        }
    }  
}
