

//--------------------------------------------------------------------
// SERVICE FUNCTIONS
//--------------------------------------------------------------------

function serviceListing(service_stats){
    /* Create the service section */
    // Generate the service container of the server (TempSrv)
    createProcessContainer("TempSrv");
    // Loop over services to create service container
    for (let name of Object.keys(service_stats)){
        createProcessContainer(name);
    }
}

function createProcessContainer(name) {
    /* Create the process container including status of the process,
    Action buttons (Start, Stop and Restart) as well as the last logs of the service*/
    
    // Create Container ID and class
    const container = document.createElement("div");
    container.className = "process-container";
    container.id =  name + "-container";

    // Define service name and enable toggling the subcontainer
    const header = document.createElement("div");
    header.className = "process-title";
    const title = document.createElement("div");
    title.textContent = name;
    title.setAttribute("onclick", "toggleLogSection(this)");
    header.appendChild(title);
    
    const subContainer = document.createElement("div");

    // Create the Action Button and status lamp table (only if not TempSrv)
    if (name !== "TempSrv"){
        var processTable = document.createElement("table");
        processTable.className = "process-table";
        var processTableRow = document.createElement("tr");

        // Create Start, Stop and Restart button
        for (var buttonName of ["Start", "Stop", "Restart"]){
            var td = document.createElement("td");
            var button = document.createElement("button");
            button.className = "action-button"
            button.textContent = buttonName;
            button.onclick = function(){
                serviceApplication(name, this.textContent);
            };
            td.appendChild(button);
            processTableRow.appendChild(td);
        }
        // Create status lamp
        const statusLamp = document.createElement("div");
        statusLamp.className = "red";
        statusLamp.id = name + "$status";
        processTableRow.appendChild(statusLamp);
        processTable.appendChild(processTableRow);
        header.appendChild(processTable);
    }
    container.appendChild(header);

    // Create Logging Table
    const logTable = document.createElement("table");
    logTable.className = "log-table";
    logTable.id = name + "-log";
    var logTHead = document.createElement("thead");
    // Since TempSrv, TaskEvent and Logger have no device-id, leave third column empty
    if (startsWithCapital(name)){
        var headerList = ['Time [UTC]', 'Type', 'Message'];
    } else {
        var headerList = ['Time [UTC]', 'Type', 'Device-ID', 'Message'];
    }
    for (var headerName of headerList){
        var headerTh = document.createElement("th");
        if (headerName != 'Message'){
            if (headerName == 'Time [UTC]'){
                headerTh.style.width = "20%";
            } else {
                headerTh.style.width = "10%";
            }
        }
        headerTh.textContent = headerName;
        logTHead.appendChild(headerTh);
    }
    logTable.appendChild(logTHead);

    // Combine everything
    subContainer.appendChild(logTable);
    container.appendChild(subContainer);
    document.getElementById("processContainers").appendChild(container);
}


function updateService(service_stats){
    /* Update the status of all services */
    for (let name of Object.keys(service_stats)){
        var element = document.getElementById(name + "$status");
        if (service_stats[name]["Status"]){
            element.classList.remove("red");
            element.classList.add("green");
        } else {
            element.classList.remove("green");
            element.classList.add("red");
        } 
    }
}

function serviceApplication(service_name, event){
    /* Start, Stop or Restart event */
    let task = {Name: service_name, Event: event};
    changeService(task);
}

function toggleLogSection(header) {
    /* Function to toggle display of service log content */
    content = header.parentElement.nextElementSibling;
    content.style.display = content.style.display === "none" ? "block" : "none";
}

function startsWithCapital(str) {
    /* Check if string starts with a capital letter */
    return /^[A-Z]/.test(str);
}

//--------------------------------------------------------------------
// LOGGING FUNCTIONS
//--------------------------------------------------------------------

function logging(logData, init=false){
    /* Update the event logging of different services on this page */
    for (let service in logData){
        if (init == false){
            var firstRowData = getFirstRowData(service);
            if (firstRowData !== undefined){
                for (let logEntry of logData[service]){
                    if (!areEqual(firstRowData, logEntry)){
                        if (compareEntries(firstRowData, logEntry)){
                            addLogEntry(logEntry);
                        } 
                    }
                }
            }
        } else {
            for (let logEntry of logData[service]){
                if (service !== 'CAT'){
                    addLogEntry(logEntry);
                }
            }
        }
    }
}
    
function addLogEntry(data) {
    /* Add a new log entry in the log window of a service */
    var service = data["SERVICE"]
    /* Function to add log entry */
    var logTable = document.getElementById(service + "-log");
    // Insert at the beginning to show the latest entries first
    var newRow = logTable.insertRow(0); 
    // Insert cells for the new row
    var timeCell = newRow.insertCell(0);
    var messageTypeCell = newRow.insertCell(1);
    if (startsWithCapital(service)){
        var descriptionCell = newRow.insertCell(2);
    } else {
        var deviceCell = newRow.insertCell(2);
        var descriptionCell = newRow.insertCell(3);
    }
    
    // Set cell values
    timeCell.textContent = data["TIMESTAMP"];
    messageTypeCell.textContent = data["TAG"];
    descriptionCell.textContent = data["MESSAGE"];
    if (!startsWithCapital(service)){
        deviceCell.textContent = data["ID"]
    }

    // Limit the log to show only the last 15 entries
    if (logTable.rows.length > 15) {
        logTable.deleteRow(-1); // Delete the last row
    }
}

function getFirstRowData(service) {
    /* Function to get data of the last row */
    var table = document.getElementById(service + "-log");
    if (service !== 'CAT'){
        var firstRow = table.rows[0]; // Get the last row
    
        // Iterate through cells of the last row to extract data
        var rowData = {};
        var cols = ["TIMESTAMP", "ID", "TAG", "MESSAGE"];
        for (var i = 0; i < firstRow.cells.length; i++) {
            rowData[cols[i]] = firstRow.cells[i].textContent;
        }

        return rowData;
    }
}

function areListsIdentical(list1, list2) {
    /* Check if the lengths are different */
    if (list1.length !== list2.length) {
        return false;
    }

    // Iterate over each element and compare
    for (var i = 0; i < list1.length; i++) {
        if (list1[i] !== list2[i]) {
            return false;
        }
    }

    // If all elements are identical
    return true;
}

function areEqual(obj1, obj2) {
    // Check if the content of two dictionaries is equal
    return JSON.stringify(obj1) === JSON.stringify(obj2);
}

function compareTimestamps(timestamp1, timestamp2) {
    // Convert the timestamp strings to Date objects
    let date1 = new Date(timestamp1);
    let date2 = new Date(timestamp2);

    // Compare the two dates
    if (date1 >= date2) {
        return false; // timestamp2 is older than or equal to timestamp1
    } else {
        return true; // timestamp1 is newer than timestamp2
    }
}

function compareEntries(dict1, dict2){
    // Compare the TIMESTAMP key of two event dictionaries
    let timestamp1 = dict1["TIMESTAMP"];
    let timestamp2 = dict2["TIMESTAMP"];

    // Return True if timestamp1 is newer
    return compareTimestamps(timestamp1, timestamp2);
}


//--------------------------------------------------------------------
// UPDATE 
//--------------------------------------------------------------------

function createUpdateTable(res){
    /* Create the structure table of to be updated modules */
    document.getElementById("updateButton").disabled = false
    // Get versions
    const newVersion = res["New_Versions"];
    const currentVersion = res["Current_Versions"];
    tableBody = document.getElementById("updateBody");
    for (var modul in newVersion){
        //
        var row = tableBody.insertRow();
        var selectCell = row.insertCell();
        checkboxInput = document.createElement("input");
        checkboxInput.type = "checkbox";
        checkboxInput.id = "module-" + modul;
        selectCell.appendChild(checkboxInput);

        //
        var moduleCell = row.insertCell();
        moduleCell.innerHTML = modul;
        var currentVersionCell = row.insertCell();
        currentVersionCell.innerHTML = currentVersion[modul];
        var newVersionCell = row.insertCell();
        newVersionCell.innerHTML = newVersion[modul];
    }
}

function toogleUpdateModal(display="block") {
    /* Toggle the update modal */
    document.getElementById("updateModal").style.display = display;
}

function confirmUpdate(){
    /* Confirm update window */
    var table = document.getElementById("updateBody");
    var data = {};

    for (var j = 0; j < table.rows.length; j++) {
        var row = table.rows[j];
        var value = row.cells[0].querySelector("input").checked;
        var key = row.cells[0].querySelector("input").id.replace("module-","");
        data[key] = value;
    }
    if (oneValueTrue(data)){
        sendVersion(data);
        shutdownDialog(" and update");
    } else {
        alert("Nothing selected");
    }
}

function oneValueTrue(obj) {
    /* Check if on element in a dictionary is set true */
    for (let key in obj) {
        if (obj[key] === true) {
            return true; // If any value is true, return true
        }
    }
    return false; // If no value is true, return false
}

//--------------------------------------------------------------------
// RECEIVE
//--------------------------------------------------------------------

function getData(init=false){
    $.ajax({
        method: "GET",
        url: "/service_exchange",
        dataType: "json",
        contentType: "application/json",
        success: function(response) {
            if (init == true){
                serviceListing(response["Services"]);
            } 
            updateService(response["Services"]);
            logging(response["Events"], init);
        },
        error: function(xhr, status, error) {
            console.error("Error fetching data:", error);
        }
    });
};

function changeService(data){
    $.ajax({
        method: "POST",
        url: "/service_exchange",
        dataType: "json",
        contentType: "application/json",
        data: JSON.stringify(data)
    });
};

function recvVersion(){
    $.ajax({
        url: "/version_exchange",
        method: "GET",
        dataType: "json",
        contentType: "application/json",
        success: function(res) {
            if ("New_Versions" in res){
                createUpdateTable(res);
            }
            console.log(res);
        },
        error: function(xhr, status, error) {
            console.error("Error fetching data:", error);
        }
    });
}

function sendVersion(data){
    $.ajax({
        method: "POST",
        url: "/version_exchange",
        dataType: "json",
        contentType: "application/json",
        data: JSON.stringify(data)
    });
};


//--------------------------------------------------------------------
// INIT
//--------------------------------------------------------------------

recvVersion()
getData(true)
var refreshIntervalId = setInterval(getData, 1000)
