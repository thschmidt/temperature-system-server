/* 
THIS IS INTERFACE BETWEEN SERVER AND HTML PAGE.
ALL DATA IS SEND AND RECEIVED HERE. 
*/

//--------------------------------------------------------------------
// GLOBAL VARIABLE
//--------------------------------------------------------------------

var Init_The_Settings = {} //JUST IN CASE SOMEBODY WANT TO NAME A GLOBAL VARIABEL Init_Settings
var Temperature_Units = ['K', '°C', '°F']

//--------------------------------------------------------------------
// RECEIVE
//--------------------------------------------------------------------

function recvData(resolve=''){
    $.ajax({
        type: "GET",
        url: "/setting_exchange",
        dataType: 'json',
        contentType: 'application/json',
        success: function(res) {
            if (resolve != ''){
                // Update global variable
                Init_The_Settings = res
                // Execute init function 
                initData(res)
                resolve()
            }
            // Execute update function
            updateData(res)
        },
        error: function(xhr, status, error) {
            console.error('Error fetching data:', error);
        }
    });  
};


function recvCampaignData(campaign){
    $.ajax({
        type: "GET",
        url: "/campwork_exchange",
        dataType: "json",
        contentType: "application/json",
        data: {'key':campaign},
        success: function(response) {
            updateCampaignData(response)
        },
        error: function(xhr, status, error) {
            console.error('Error fetching data:', error);
        }
    })
}

function recvWorkData(workId, copy=false){
    $.ajax({
        type: "GET",
        url: "/campwork_exchange",
        dataType: "json",
        contentType: "application/json",
        data: {'key':workId, 'group':'work'},
        success: function(response) {
            updateWorkData(response, copy)
        },
        error: function(xhr, status, error) {
            console.error('Error fetching data:', error);
        }
    })
}


function recvGraphData(campaign, period=0, reload=false, firstDate=0) {
    $.ajax({
        url: '/data_exchange',
        method: 'GET',
        dataType: 'json',
        contentType: 'application/json',
        data: {'key':campaign, 'period':parseInt(period), 'first_date':firstDate},
        success: function(res) {
            updateGraphData(res, period, reload, firstDate)
        },
        error: function(xhr, status, error) {
            console.error('Error fetching data:', error);
        }
    });
}


function recvVersion(){
    $.ajax({
        url: '/version_exchange',
        method: 'GET',
        dataType: 'json',
        contentType: 'application/json',
        success: function(res) {
            console.log(res)
            
        },
        error: function(xhr, status, error) {
            console.error('Error fetching data:', error);
        }
    });
}

//--------------------------------------------------------------------
// SUBFUNCTIONS
//--------------------------------------------------------------------

function createTableHeader(headerRow, headerList, widths=[]){
    var i = 0
    for (const key of headerList) {
        const th = document.createElement('th');
        if (widths[i] != undefined){
            th.style.width = widths[i]
        }
        th.textContent = key;
        headerRow.appendChild(th);
        i += 1
    }
}

//-------------------------- CREATE OPTIONS ------------------------------------------

function createOptionsCampaign(settings) {
    var campaigns = settings['campaign']['CampaignLists']
    var campaignSelect = document.getElementById("campaignSelect");

    for (let i = 0; i < campaigns[0].length; i++){
        var option = document.createElement("option");
        option.text = campaigns[1][i];
        option.value = campaigns[0][i];
        campaignSelect.appendChild(option);
    }    
}

function createOptionsWork(settings) {
    var work = settings['work']['WorkLists']
    var workSelect = document.getElementById("workSelect");

    for (let i = 0; i < work[0].length; i++){
        var option = document.createElement("option");
        option.text = work[1][i];
        option.value = work[0][i];
        workSelect.appendChild(option);
    }    
}

function createOptions(selector, options){
    /* Add to a select instance the given options */
    for (var value of options){
        var option = document.createElement("option");
        option.text = value;
        option.value = value;
        selector.appendChild(option);
    } 
}

//-------------------------- TEMPERATURE RELATED ------------------------------------------

function checkTemperatureConversion(value, unit){
    /* Check if unit is a temperature unit and convert the temperature value */
    if (Temperature_Units.includes(unit)){
        defaultUnit = getDefaultTemperatureUnit()
        return [convertTemperature(value, defaultUnit), defaultUnit] ;
    } else {
        return [value, unit];
    }
}

function convertTemperature(inputTemperature, unit='K') {
    /* Convert the input temperature (in Kelvin) based on the specified unit, 
    with Kelvin (K) being the default unit */
    var temperature = parseFloat(inputTemperature)
    var result;
    switch(unit) {
        case 'K':
            result = temperature
            break;
        case '°C':
            result = temperature - 273.15;
            break;
        case '°F':
            result = (temperature - 273.15) * 9/5 + 32;
            break;
    }
    return (Math.round(result*1000) / 1000);
}

function temperatureInK(inputTemperature, unit, relative=false){
    /* Convert Kelvin, Celsius or Fahrenheit into Kelvin */
    var temperature = parseFloat(inputTemperature)
    if (relative){
        // Relative Temperatures
        switch (unit) {
            case "°F":
                return temperature * 1.8;
            default:
                return temperature
        }
    } else {
        // Absolute Temperatures
        var result;
        switch (unit){
            case 'K':
                result = temperature
                break;
            case '°C':
                result = temperature + 273.15;
                break;
            case '°F':
                result = (temperature - 32) * 5/9 + 273.15;
                break;
        }
        return (Math.round(result*1000) / 1000);
    }
}

function setDefaultTemperatureUnit(select){
    /* Get default temperature unit from server and move it to the top of any selector */
    var globalUnit;
    try {
        globalUnit = getDefaultTemperatureUnit()
    } catch(error) {
        globalUnit = 'K';
    }
    const optionsList = moveValueToFirstPosition(Temperature_Units, globalUnit);
    createOptions(select, optionsList);
}

function getDefaultTemperatureUnit(){
    return Init_The_Settings["website"]["GLOBAL"]['temperature_unit'];
}


//-------------------------- TIME RELATED ------------------------------------------

function convertIntoSeconds(inputTime, unit='second(s)'){
    /* Convert relative time input in minutes, hours, etc. into seconds
    Perform the multiplication based on the selected unit */
    var multiplier;
    switch (unit) {
        case "second(s)":
            multiplier = 1;
            break;
        case "minute(s)":
            multiplier = 60;
            break;
        case "hour(s)":
            multiplier = 60 * 60;
            break;
        case "day(s)":
            multiplier = 60 * 60 * 24;
            break;
        case "week(s)":
            multiplier = 60 * 60 * 24 * 7;
            break;
        default:
            multiplier = 1;
    }
    return multiplier * inputTime
}

//-------------------------- ARRAY RELATED ------------------------------------------

function moveValueToFirstPosition(arr, value) {
    var index = arr.indexOf(value);
    
    // If the value is found in the array
    if (index > -1) {
        // Remove the value from its current position
        arr.splice(index, 1);
        // Add the value to the beginning of the array
        arr.unshift(value);
    }
    return arr;
}


//-------------------------- TYPE OF VALUE ------------------------------------------

function restrictInputToNumber(event, input='', negative=false){
    var charCode = (event.which) ? event.which : event.keyCode;
    // Allow negative numbers (automatically shifts '-' to the beginning)
    if (input != '' && negative && charCode == 45){
        event.preventDefault();
        if (input.value.includes('-')){
            return false;
        } else {
            input.value = '-' + input.value
            return true;
        }
    }
    // Only allow numbers
    if (charCode < 48 || charCode > 57) {
        event.preventDefault();
        return false;
    }
    return true;
}

function restrictInputToFloat(event, input='', negative=false) {
    var charCode = (event.which) ? event.which : event.keyCode;
    // Allow dot
    if (charCode === 46) {
        // Allow max one dot
        if (input != ''){
            if (input.value.includes('.')){
                event.preventDefault();
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    } 
    return restrictInputToNumber(event, input, negative)
}


function isInteger(value){
    const intValue = parseInt(value);
    // Check if the converted value is not NaN and if its type is 'number'
    return !isNaN(intValue) && intValue.toString() === value && Number.isInteger(intValue);
}


function isFloat(value){
    // Use parseFloat to convert the value to a floating point number
    const floatValue = parseFloat(value);
    // Check if the parsed value is not NaN (Not a Number)
    // and if the original value is equal to the string representation of the parsed
    return !isNaN(floatValue) && value == floatValue.toString();
}


//------------------------- BUTTONS -------------------------------------------

// Function to deactivate button
function deactivateButton(buttonId) {
    var button = document.getElementById(buttonId)
    if (button !== null){
        button.disabled = true;
    }
}
  
// Function to activate button
function activateButton(buttonId) {
    var button = document.getElementById(buttonId)
    if (button !== null){
        button.disabled = false;
    }
}

function closeAlert(containerid="alertContainer"){
    document.getElementById(containerid).innerHTML = ''
}

function openAlert(msg, msgtype='warning', containerId="alertContainer"){
    var alertContainer = document.getElementById(containerId)
    if (msg != ''){
        alertContainer.innerHTML = ('<div class="' + msgtype +' alert">' + 
        '<div class="content"><div class="alert-icon">' + 
        '<img height="50px" src="/webpage/image/alert.svg"></div><p>' + msg +  
        '</p><button class="close-alert" onclick="closeAlert(' + "'" + containerId + "'" + ')">' + 
        'x</button></div></div>')
    } else {
        alertContainer.innerHTML = ''
    }  
}


//--------------------------------------------------------------------
//  INIT AND UPDATE DATA
//--------------------------------------------------------------------

let Initialization_Promise = new Promise((resolve, reject) => {
    recvVersion();
    recvData(resolve);

    setInterval(recvData, 500)
})


// Close the modal if user clicks outside of it
window.onclick = function(event) {
    var modal = document.getElementById("showLoginModal");
    if (event.target == modal) {
        modal.style.display = "none";
    }
}