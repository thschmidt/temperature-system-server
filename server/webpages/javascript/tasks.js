
//--------------------------------------------------------------------
// GLOBAL VARIABLE
//--------------------------------------------------------------------

var SensorList = []
var ControllerDict = {}
var CampaignActive = false //
var ConditionDict = {"Relative Time":[],
                    "Temperature Stability":["Sensor ID","Temperature Range", "Period"],
                    "Temperature Below":["Sensor ID","Temperature"],
                    "Temperature Above":["Sensor ID","Temperature"]
                }

//--------------------------------------------------------------------
// CAMPAIGN
//--------------------------------------------------------------------

function updateCampaignData(resp){
    /* Get infos about the current sensors and controllers of the campaign */
    getSensorsFromDict(resp["Sensors"])
    getControllerFromCampaign(resp["Devices"]["temp_control"])
}

function getControllerFromCampaign(controller){
    ControllerDict = {};
    for (var series in controller){
        ControllerDict[series] = {};
        for (var deviceId in controller[series]){
            ControllerDict[series][deviceId] = Init_The_Settings["temp_control"][series]["ID"][deviceId]
        }
    }
}

function getSensorsFromDict(sensorDict){
    SensorList = []
    for (var group in sensorDict){
        for (var series in sensorDict[group]){
            var array = Object.keys(sensorDict[group][series])
            SensorList = SensorList .concat(array)
        }
    }
}

//--------------------------------------------------------------------
// PREPARE / UPDATE
//--------------------------------------------------------------------

function initData(statusData){
    /* Initialize campaign info and its status data and show existing work options */
    const selectedCampaign = statusData["campaign"]["CurrentCampaign"]
    if (selectedCampaign != ''){
        recvCampaignData(selectedCampaign);
        createOptionsWork(statusData)
        //const selectedWork = statusData["work"]["CurrentWork"]
        //recvWorkData(selectedWork);
        updateData(statusData) 
    }
}

function updateData(statusData){
    // Fetch data for the update window
    // Get all variable instances
    var taskStarted = document.getElementById("taskStarted")
    var taskPosition = document.getElementById("taskPosition")
    var currentWork = document.getElementById("currentWork")
    var iteration = document.getElementById("iteration")
    // Update all variables
    taskStarted.innerHTML = statusData["work"]["StartedTask"]
    taskPosition.innerHTML = statusData["work"]["CurrentTask"]
    currentWork.innerHTML = statusData["work"]["CurrentWork"]
    iteration.innerHTML = statusData["work"]["Iteration"]

    var conditionVariable = statusData["work"]["ConditionVariable"]
    CampaignActive = statusData["campaign"]["Active"]
    var table = document.getElementById("currentVariableTable")
    // Reset the variable table
    table.innerHTML = ""
    // Fill the variable table with new data
    if (!jQuery.isEmptyObject(conditionVariable)){
        createCurrentStatusTable(table, conditionVariable)
    } else {
        distancer = document.getElementById("distancerCurrentTaskTable")
        distancer.style.margin = "0px"
    }
    if (statusData["work"]["Active"]){
        deactivateButton("startWorkButton")
        deactivateButton("workIteration")
        activateButton("stopWorkButton")
    } else {
        deactivateButton("stopWorkButton")
    }
}

function updateWorkData(resp, copy) {
    var workDiv = document.getElementById("workDiv")
    workDiv.innerHTML = ""
    loadWorkTable(resp, copy)
}

function createCurrentStatusTable(table, conditionVariable){
    for (var key in conditionVariable){
        var row = table.insertRow()
        var headerRow = document.createElement("tr")
        header = document.createElement('th')
        header.className = 'current-task-header'
        header.innerHTML = key
        headerRow.appendChild(header)
        if (conditionVariable[key].constructor == Object){
            row.appendChild(headerRow)
            num = 0
            for (var subkey in conditionVariable[key]){
                var dataRow = table.insertRow()
                var cell1 = dataRow.insertCell()
                cell1.innerHTML = subkey
                var cell2 = dataRow.insertCell()
                cell2.innerHTML = conditionVariable[key][subkey]
                num += 1
            }
            distancer = document.getElementById("distancerCurrentTaskTable")
            if (num > 2){
                distancer.style.margin = (num-1)*20 + "px"
            } else {
                distancer.style.margin = "0px"
            }
        } else {
            var cell = document.createElement('tr')
            cell.innerHTML = conditionVariable[key]
            headerRow.appendChild(cell)
            row.appendChild(headerRow)
        }
    }
}

//--------------------------------------------------------------------
// DIV FUNCTIONS
//--------------------------------------------------------------------

function resetWorkDiv(errorMessage=''){
    if (errorMessage != ''){
        openAlert(errorMessage, "warning", "workAlertContainer");
    }
    
    var workDiv = document.getElementById("workDiv")
    workDiv.innerHTML = ""
}

function createWork(){
    // On button press create a new work if no addTaskButton exists
    try{
        if (document.getElementById("addTaskButton") === null){
            initWorkDiv()
        }
    } catch(err){
        errorMessage = "Cannot create a new work as no campaign is running"
        resetWorkDiv(errorMessage)
    } 
}

function initWorkDiv(data={}, copy=false){
    var workDiv = document.getElementById("workDiv")
    resetWorkDiv()

    // Table Info / Details
    var workDetails = createWorkDetails(data)

    // Create Setup table
    var initDiv = createInitFinalDiv(data)
    var tasksDiv = createTasksDiv(data)
    var endDiv = createInitFinalDiv(data, prefix="Final")
    
    // Add class to divs
    initDiv.className = "work-divs"
    tasksDiv.className = "work-divs"
    endDiv.className = "work-divs"

    // Add all to div
    workDiv.appendChild(workHeaderButtons(data))
    workDiv.appendChild(workDetails)
    workDiv.appendChild(initDiv)
    workDiv.appendChild(tasksDiv)
    workDiv.appendChild(endDiv)
    
    if (jQuery.isEmptyObject(data)){
        // Add Save Button to WorkDiv
        var saveButton = document.createElement("button")
        saveButton.id = "saveWork"
        saveButton.className = "save-button"
        saveButton.innerHTML = "Save Work"
        saveButton.onclick = function(){
            saveWork()
        }

        workDiv.appendChild(saveButton)
        
        if (!copy){
            // Show an add task
            addNewConditionalTask()
        }
        
    } else {
        loadTaskRows(data["Tasks"])
        
    }
}


// INIT AND FINAL DIV

function createInitFinalDiv(data={}, prefix="Init"){
    var setupDiv = document.createElement("div")
    setupDiv.id = prefix.toLowerCase() + "Div"
    var contentDiv = document.createElement("div")
    contentDiv.className = "work-content-divs"
    contentDiv.id = prefix.toLowerCase() + "SetupDiv"
    var titleHeader = document.createElement("h2")
    titleHeader.innerHTML = prefix + " Setup"

    setupDiv.appendChild(titleHeader)
    setupDiv.appendChild(contentDiv)
    if (jQuery.isEmptyObject(data)){
        // Append Add Task Button
        var setupButton = document.createElement("button")
        setupButton.innerHTML = "Add " + prefix + " Setup"
        setupButton.id = prefix.toLowerCase() + "SetupButton"
        setupButton.className = "setup-button"
        setupButton.onclick = function(){
            initFinalButtonEvent(this, prefix, contentDiv)
        } 
        setupDiv.appendChild(setupButton)
    } else {
        if (data[prefix].length !== 0){
            var statementCell = document.createElement("div")
            createLoadedStatementCell(statementCell, data[prefix])

            contentDiv.appendChild(statementCell)
        }  
    }
    return setupDiv
}

function initFinalButtonEvent(button, prefix, contentDiv, copy=false){
    /* Button event of the init and final setup table
     to create or delete the statement div */
    text = button.innerHTML
    if (text.includes("Add")){
        button.innerHTML = "Delete " + prefix + " Setup"
        var statementCell = document.createElement("div")
        statementCell.id = prefix.toLowerCase()  +  "Table"
        contentDiv.appendChild(statementCell)
        createStatementTable(statementCell, copy)

    } else {
        contentDiv.innerHTML = ""
        button.innerHTML = "Add " + prefix + " Setup"
    }
}

// WORK DIV

function createTasksDiv(data={}){
    // Create task table and body
    var tableDiv = document.createElement("div")
    tableDiv.id = "tableDiv"
    var table = document.createElement("table")
    table.id = "workTable"
    var load = false

    var titleHeader = document.createElement("h2")
    titleHeader.innerHTML = "Work Setup"

    if (!jQuery.isEmptyObject(data)){
        load = true
    }
    createWorkTableHeader(table, load)
    var tbody = document.createElement("tbody")
    tbody.id = "workTableBody"
    table.appendChild(tbody)

    // Append all table to div
    tableDiv.appendChild(titleHeader)
    tableDiv.appendChild(table)

    if (jQuery.isEmptyObject(data)){
        // Append Add Task Button
        var addButton = document.createElement("button")
        addButton.innerHTML = "Add Task"
        addButton.id = "addTaskButton"
        addButton.className = "table-add-button"
        addButton.onclick = function(){
            addNewConditionalTask()
        }

        tableDiv.appendChild(addButton)
    }

    return tableDiv
}


function createWorkDetails(data={}){
    // Input task name / ID
    var workInfo = document.createElement("table")
    var header = ["Work Name", "Work ID"]
    for (const head of header){
        var tableRow = workInfo.insertRow()
        var td = tableRow.insertCell()
        td.innerHTML = head
        var rowDetails = tableRow.insertCell()
        if (head == "Work Name"){
            if (jQuery.isEmptyObject(data)){
                var workName = document.createElement("input")
                workName.oninput = function(){
                    createWorkId()
                }
            } else {
                var workName = document.createElement("div")
                workName.innerHTML = data["Name"]
            }
            workName.id = "workName"
            workName.className = "work-details"
            rowDetails.appendChild(workName)
        } else {
            var workId = document.createElement("div")
            workId.id = "workId"
            workId.className = "work-details"
            if (!jQuery.isEmptyObject(data)){
                workId.innerHTML = data["ID"]
            }
            rowDetails.appendChild(workId)
        }
    }
    return workInfo
}

function workHeaderButtons(parameter={}){
    // Add start and stop button
    var buttonDiv = document.createElement("div")
    var startButton = document.createElement("button")
    startButton.id = "startWorkButton"
    startButton.innerHTML = "Start Work"
    startButton.disabled = true
    startButton.onclick = function() {
        startWork()
    }
    var stopButton = document.createElement("button")
    stopButton.id = "stopWorkButton"
    stopButton.innerHTML = "Stop Work"
    stopButton.disabled = true
    stopButton.onclick = function() {
        stopWork()
    }
    // Create the iteration input
    var iterationLabel = document.createElement("label")
    iterationLabel.innerHTML = "Number of Iterations"
    var iterationNum = document.createElement("input")
    iterationNum.addEventListener("keypress", function(event){
        restrictInputToNumber(event)
    })
    iterationNum.setAttribute("type", "number")
    iterationNum.id = "workIteration"

    if (parameter.hasOwnProperty("NoI")){
        iterationNum.value = parameter["NoI"]
    } else {
        iterationNum.value = 1
    }
    
    iterationLabel.appendChild(iterationNum)
    buttonDiv.appendChild(startButton)
    buttonDiv.appendChild(stopButton)
    buttonDiv.appendChild(iterationLabel)
    return buttonDiv
}


//--------------------------------------------------------------------
// TABLE FUNCTIONS
//--------------------------------------------------------------------

//-------------------------- DELETE BUTTON --------------------------

function createDeleteButton(buttonName = "Delete", table = ""){
    var deleteButton = document.createElement("button");
    deleteButton.innerHTML = buttonName;
    deleteButton.className = "table-delete-button"
    deleteButton.onclick = function() {
        deleteRow(this);
        if (table !== ""){
            changeRowNumbers(table)
        }
    };
    return deleteButton
}

function deleteRow(btn) {
    var row = btn.parentNode.parentNode;
    row.parentNode.removeChild(row);
}

//-------------------------- WORK TABLE --------------------------

function createWorkTableHeader(table, deleteButton=false){
    const thead = document.createElement("thead");
    const headerRow = thead.insertRow()
    headerRow.className = "task-row"
    var header = ["Pos", "Condition", "Statement"]

    // Add header cell for delete button
    if (deleteButton){
        header.concat([""])
    }

    createTableHeader(headerRow, header, ['3%', '50%', '42%', '5%']) 

    table.appendChild(thead)
}

// ROWS

function createTaskRow(tbody, table, copy){
    // Get the current row Number
    var rowNumber = table.rows.length

    // Create a new task row
    var row = document.createElement("tr")
    row.className = "task-row"
    
    // Create cell for row number
    var rowNumberTd = document.createElement("td")
    rowNumberTd.innerHTML = rowNumber
    row.appendChild(rowNumberTd)

    // Create cell for the condition
    var conditionCell = document.createElement("td")
    row.appendChild(conditionCell)

    // Create cell for the statement
    var statementCell = document.createElement("td")
    row.appendChild(statementCell)

    // Create cell for the delete task row
    var deleteButtonCell = row.insertCell();
    deleteButtonCell.appendChild(createDeleteButton(buttonName="Delete Task", table=table))
    tbody.appendChild(row)

    // Create the individual tables 
    createConditionTable(conditionCell, copy)
    createStatementTable(statementCell, copy)
    return [conditionCell, statementCell]
}

// Other STUFF

function addNewConditionalTask(copy=false){
    var table = document.getElementById("workTable")
    var tbody = document.getElementById("workTableBody")
    var cells = createTaskRow(tbody, table, copy)
    return cells
}

function changeRowNumbers(table){
    for (let j = 1; j < table.rows.length; j++) {
        table.rows[j].cells[0].innerHTML = j;
    }
}

function createWorkId() {
    // Lowercase the task name 
    var input = document.getElementById("workName").value.toLowerCase();
    var output = document.getElementById("workId");
    // Define a regular expression to match special characters and spaces
    var regex = /[^a-zA-Z0-9]/g;
    // Replace special characters and spaces with underscores
    output.textContent = input.replace(regex, "_");
}

//------------------- CONDITONS ----------------------------------

function createConditionTable(conditionCell, copy){
    var conditionTable = document.createElement("table")
    const thead = document.createElement("thead");
    const headerRow = thead.insertRow();

    var header = ["Type", "Variable", "Logic", ""];
    createTableHeader(headerRow, header, ['20%', '65%', '5%', '10%']);

    conditionTable.appendChild(thead);
    var tbody = document.createElement("tbody");
    conditionTable.appendChild(tbody);

    const addButton = document.createElement("button");
    addButton.innerHTML = "Add Condition";
    addButton.className = "table-add-button"
    addButton.onclick = function(){
        addConditionRow(tbody);
    }

    if (!copy){
        addConditionRow(conditionTable);
    }
    
    conditionCell.appendChild(conditionTable);
    conditionCell.appendChild(addButton);

}

function addConditionRow(conditionTable, logic=""){
    var conditionRow = conditionTable.insertRow()

    // Condition Term
    var conditionSelectVariables = Object.keys(ConditionDict)
    var conditionSelect = document.createElement("select")
    conditionSelect.onchange = function() {
        changeConditionVariable(this)
    };
    
    createOptions(conditionSelect, conditionSelectVariables) 
    
    var conditionTerm = conditionRow.insertCell()
    conditionTerm.appendChild(conditionSelect)

    // Condition Description
    conditionRow.insertCell()

    // Condition Logic
    var logicSelect = document.createElement("select")

    createOptions(logicSelect, [1,2,3,4,5,6]) 

    var conditionLogic = conditionRow.insertCell()
    conditionLogic.appendChild(logicSelect)

    // Delete Button
    var deleteButtonCell = conditionRow.insertCell();
    deleteButtonCell.appendChild(createDeleteButton())
    if (logic == ""){
        changeConditionVariable(conditionSelect)
    } else {
        logicSelect.value = logic
        return conditionSelect
    }
    
}


function changeConditionVariable(conditionSelect, data={}){
    if (jQuery.isEmptyObject(data)){
        var key = conditionSelect.options[conditionSelect.selectedIndex].value;
    } else {
        conditionSelect.value = data["TYPE"]
        var key = data["TYPE"]
    }

    var conditionVariable = conditionSelect.parentNode.nextSibling
    conditionVariable.innerHTML = ""
    var conditionVaribalesTable = document.createElement("table")
    const thead = document.createElement("thead");
    const headerRow = thead.insertRow()
    const tbody = document.createElement("tbody");
    
    if (key == "Relative Time") {
        var newRow = tbody.insertRow()
        var conditionElement1 = document.createElement("input")
        conditionElement1.addEventListener("keypress", function(event){
            restrictInputToFloat(event, this)});
        var timeUnits = ["second(s)", "minute(s)", "hour(s)", "day(s)"]
        var conditionElement2 = document.createElement("select")

        createOptions(conditionElement2, timeUnits)

        var addButton = document.createElement("div")

        conditionElement1.value = data["VARIABLE"] === undefined ? 10 : data["VARIABLE"];
        newRow.appendChild(conditionElement1)
        newRow.appendChild(conditionElement2) 
    } else {
        var conditionElement1 = document.createElement("select")

        var header = ConditionDict[key].concat([""])
        createTableHeader(headerRow, header)

        var addButton = document.createElement("button")
        addButton.innerHTML = "Add Sensor"
        addButton.className = "table-add-button"
        addButton.onclick = function(){
            addConditionParameters(tbody, ConditionDict[key])
        }
        if (jQuery.isEmptyObject(data)){
            addConditionParameters(tbody, ConditionDict[key])
        }
    }
    conditionVaribalesTable.appendChild(thead)
    conditionVaribalesTable.appendChild(tbody)
    conditionVaribalesTable.appendChild(addButton)
    conditionVariable.appendChild(conditionVaribalesTable)

    if (!jQuery.isEmptyObject(data)){
        return tbody
    }
}

function addConditionParameters(tbody, header, data={}){
    /*  */
    var newRow = tbody.insertRow()
    for (var parameter of header){
        var parameterCell = newRow.insertCell()
        // Create selector for sensor ID
        if (parameter == "Sensor ID"){
            var parameterSelector = document.createElement("select")
            createOptions(parameterSelector, SensorList)
            parameterCell.appendChild(parameterSelector)

            if (!jQuery.isEmptyObject(data)){
                parameterSelector.value = data["Sensor ID"]
            }
        // Create input for Temperature
        } else if (parameter.includes("Temperature")){
            var temperatureInput = document.createElement("input")
            var parameterSelector = document.createElement("select")
            temperatureInput.addEventListener("keypress", function(event){
                if (parameter == "Temperature Range"){
                    restrictInputToFloat(event, this,)
                } else {
                    restrictInputToFloat(event, this, true)
                }
            });

            setDefaultTemperatureUnit(parameterSelector)
            parameterCell.appendChild(temperatureInput)
            parameterCell.appendChild(parameterSelector)

            if (!jQuery.isEmptyObject(data)){
                unit = getDefaultTemperatureUnit()
                if (data.hasOwnProperty("Temperature")){
                    temperatureInput.value = temperatureInK(data["Temperature"], unit)
                } else {
                    temperatureInput.value = temperatureInK(data["Temperature Range"], unit, true)
                }
            }
        // Create Input for time period
        } else if (parameter == "Period"){
            var timeInput = document.createElement("input")
            timeInput.addEventListener("keypress", function(event){
                restrictInputToFloat(event, this)});
            var timeUnits = ["minute(s)", "hour(s)", "day(s)"]
            var parameterSelector = document.createElement("select")
            createOptions(parameterSelector, timeUnits)
            parameterCell.appendChild(timeInput)
            parameterCell.appendChild(parameterSelector)

            if (!jQuery.isEmptyObject(data)){
                timeInput.value = data["Period"]/60
            }
        }
    }

    var deleteButtonCell = newRow.insertCell();
    deleteButtonCell.appendChild(createDeleteButton())
}



//------------------- STATEMENTS ----------------------------------

function createStatementTable(statementCell, copy=false){
    const table = document.createElement("table");
    const thead = document.createElement("thead");
    const headerRow = thead.insertRow();

    var header = ["Series", "Device ID", "Channel", "Parameter", "Value", "Unit", ""]
    createTableHeader(headerRow, header, ['15%', '15%', '15%', '22%', '15%', '6%', '12%'])

    table.appendChild(thead)
    const tbody = document.createElement("tbody");

    statementCell.appendChild(table)
    const addButton = document.createElement("button")
    addButton.innerHTML = "Add Statement"
    addButton.className = "table-add-button"
    addButton.onclick = function(){
        addStatementRow(tbody)
    }
    table.appendChild(tbody)
    statementCell.appendChild(table)
    statementCell.appendChild(addButton)

    if (!copy){
        addStatementRow(tbody)
    } 
}

function addStatementRow(tableBody, data={}) {
    const dictionary = ControllerDict
    const newRow = document.createElement("tr");
    
    var seriesCell = document.createElement("td");
    var deviceIdCell = document.createElement("td");
    var channelCell = document.createElement("td");
    var parameterCell = document.createElement("td");
    var cells = [seriesCell, deviceIdCell, channelCell]

    // select series
    const seriesSelect = document.createElement("select");
    seriesSelect.onchange = function(){
        var changeSelect = [true, true, true]
        createStatementOptions(cells, changeSelect, dictionary)
    }
    addSelection(seriesSelect, dictionary);
    seriesCell.className = 'statement-select'
    seriesCell.appendChild(seriesSelect);
    newRow.appendChild(seriesCell);

    // select device
    const deviceIdSelect = document.createElement("select");
    deviceIdSelect.onchange = function(){
        var changeSelect = [false, true, true]
        createStatementOptions(cells, changeSelect, dictionary)
    }
    deviceIdCell.className = 'statement-select'
    deviceIdCell.appendChild(deviceIdSelect);
    newRow.appendChild(deviceIdCell);

    // select channel
    const channelSelect = document.createElement("select");
    channelSelect.onchange = function(){
        var changeSelect = [false, false, true]
        createStatementOptions(cells, changeSelect, dictionary)
    }
    channelCell.className = 'statement-select'
    channelCell.appendChild(channelSelect);
    newRow.appendChild(channelCell);

    // select parameter
    const parameterSelect = document.createElement("select");
    parameterCell.className = 'statement-select'
    parameterCell.appendChild(parameterSelect);
    parameterCell.onchange = function(){
        changeValueType(cells, dictionary)
    }
    newRow.appendChild(parameterCell);

    // input value
    var valueCell = document.createElement("td");
    valueCell.className = 'statement-input'
    newRow.appendChild(valueCell);

    // unit 
    var unitCell = document.createElement("td");
    newRow.appendChild(unitCell);

    tableBody.appendChild(newRow);

    var changeSelect = [true, true, true]
    createStatementOptions(cells, changeSelect, dictionary)
    changeValueType(cells, dictionary)

    var deleteButtonCell = newRow.insertCell();
    deleteButtonCell.appendChild(createDeleteButton())

    if (!jQuery.isEmptyObject(data)){
        seriesSelect.value = data["SERIES"]
        deviceIdSelect.value = data["ID"]
        channelSelect.value = data["CHANNEL"]
        parameterSelect.value = data["PARAMETER"]
        valueCell.firstChild.value = data["VALUE"]
        unitCell.innerHTML = data["UNIT"]
    }
}


// SUBFUNCTIONS STATEMENT

function addSelection(select, dictionary){
    select.innerHTML = ""
    var keys = Object.keys(dictionary)
    keys.forEach(key => {
        var add = true;
        if (dictionary[key].hasOwnProperty("Read")){
            // Remove all not setable parameters
            if (!dictionary[key].hasOwnProperty("Set")){
                add = false
            }
        }
        if (add){
            const option = document.createElement("option");
            option.value = key;
            option.text = key;
            select.appendChild(option); 
        } 
    });
}

function getSelector(cell){
    // Get content of cell (select)
    return cell.nextElementSibling.firstElementChild
}

function getSelectedKey(cell){
    // Get value from select instance in cell
    var select = cell.firstElementChild
    return select.options[select.selectedIndex].value;
}

function getSubKeyDict(dictionary, keyList){
    // 
    for (var key of keyList){
        dictionary = dictionary[key]
    }
    return dictionary
}

function createStatementOptions(cells, changeSelect, dictionary){
    var subDict = dictionary
    for (var i = 0; i < cells.length; i++){
        var selected = getSelectedKey(cells[i])
        subDict = subDict[selected]
        if (i == 1){
            subDict = subDict["Channel"]
        }
        if (changeSelect[i]){
            addSelection(getSelector(cells[i]), subDict)
        }
    }
    changeValueType(cells, dictionary)
}

function changeValueType(cells, dictionary){
    var parameterCell = cells[2].nextElementSibling

    // Get value and unit cell and reset them
    var valueCell = parameterCell.nextElementSibling
    valueCell.innerHTML = ""
    var unitCell = valueCell.nextElementSibling
    unitCell.innerHTML = ""

    // Get current selected parameter, channel, device id and series
    const parameter = parameterCell.firstElementChild.value
    const channel = cells[2].firstElementChild.value
    const deviceId = cells[1].firstElementChild.value
    const series = cells[0].firstElementChild.value

    // Get variables of selected parameter
    var parameterVar = dictionary[series][deviceId]["Channel"][channel][parameter]

    if (parameterVar.hasOwnProperty("Unit")){
        // Update Unit cell
        unitCell = valueCell.nextElementSibling;
        var unit = parameterVar.Unit;
        if (["°K", "°C", "°F"].includes(unit)){
            unit = getDefaultTemperatureUnit();
        }
        unitCell.innerHTML = unit;
    }
    if (parameterVar.hasOwnProperty("InputType")){
        // Update value cell
        const inputType = parameterVar.InputType
        if (inputType == "Select"){
            var selectValue = document.createElement("select");
            // Create the options for the selection value
            if (parameterVar.hasOwnProperty("Values")){
                optionList = parameterVar.Values;
            } else {
                optionList = ["True", "False"];
            }
            createOptions(selectValue, optionList);
            valueCell.appendChild(selectValue);

        } else if (["Float", "Int", "String"].includes(inputType)){
            var inputValue = document.createElement("input");
            if (inputType == "Float"){
                // Make sure only numbers and max one dot can be entered
                inputValue.addEventListener("keypress", function(event){
                    restrictInputToFloat(event, this, true)});
                valueCell.appendChild(inputValue);
            } else if (inputType == "Int"){
                // Make sure only numbers can be entered
                inputValue.addEventListener("keypress", function(event){
                    restrictInputToNumber(event, this, true)});
            } 
            valueCell.appendChild(inputValue);
        } else if (inputType == 'Switch'){
            var selectValue = document.createElement("select");
            // Create the options for the selection value
            optionList = ["1", "0"];
            createOptions(selectValue, optionList);
            valueCell.appendChild(selectValue);
        }
    }
}


//--------------------------------------------------------------------
// SAVE WORK
//--------------------------------------------------------------------

function saveWork(){
    const workTable = document.getElementById("workTable");
    const initTable = document.getElementById("initTable");
    const finalTable = document.getElementById("finalTable");

    if (workTable !== null){
        // Get some work details
        const wid = document.getElementById("workId").innerHTML;
        const wname = document.getElementById("workName").value;
        const witeration = document.getElementById("workIteration").value;

        var errorMessage = ""
        try {
            checkWorkInput(wid)
            // Extract data from tables
            var data = getWorkTableData(workTable);
            var init = getSetupTableData(initTable);
            var final = getSetupTableData(finalTable);

            // Prepare message send to server
            msg = {"COM":"INIT",
                    "WORK":{"ID": wid,
                            "NAME":wname,
                            "TASKS":data,
                            "NOI":witeration,
                            "INIT":init,
                            "FINAL":final
                        }
                    };
        } catch (error){
            errorMessage = error
        }
        if (errorMessage == ""){
            sendCampaignData(msg); 
            if (CampaignActive){
                activateButton("startWorkButton")
            }
        }
        openAlert(errorMessage, "warning", "workAlertContainer");
    }  
}

function checkWorkInput(wid){
    if (wid === ""){
        throw ("No work name has been defined.")
    }
}

//------------------- EXTRACT TABLE DATA ---------------------------------

function getWorkTableData(table){
    var data = {};
    for (var j = 1; j < table.rows.length; j++) {
        var row = table.rows[j];
        var rowData = {};
        for (var k = 0; k < row.cells.length; k++) {
            var rowInfo = row.cells[k].firstElementChild
            if (k == 1){
                rowData["Condition"] = getStatCondTableData(rowInfo)
            }
            else if (k == 2){
                rowData["Statement"] = getStatCondTableData(rowInfo)
            }
        }
        data[j] = rowData;
    }
    return data
}

// INIT AND FINAL TABLES

function getSetupTableData(table){
    if (table !== null){
        return getStatCondTableData(table.firstElementChild)
    } else {
        return []
    }
}

function getStatCondTableData(table) {
    var headers = [];
    var data = [];

    // Get headers
    var headerRow = table.rows[0];
    for (var i = 0; i < headerRow.cells.length; i++) {
        headers.push(headerRow.cells[i].textContent.trim());
    }

    // Get data
    for (var j = 1; j < table.rows.length; j++) {
        var row = table.rows[j];
        var rowData = {};
        for (var k = 0; k < row.cells.length; k++) {
            var key = headers[k];
            if (key == "Variable"){
                const condition_type = row.cells[0].querySelector("select").value
                rowData[key.toUpperCase()] = getSubConditionTableData(row.cells[k],condition_type);
            } else if (key != ""){
                var value;
                if (row.cells[k].querySelector("input")) {
                    value = row.cells[k].querySelector("input").value;
                    if (value == ""){
                        throw ("An input is empty please check your work setup")
                    }
                } 
                else if (row.cells[k].querySelector("select")){
                    value = row.cells[k].querySelector("select").value;
                } else {
                    value = row.cells[k].textContent.trim();
                }
                if (key == "Device ID"){
                    key = "ID"
                } else if (key == "Unit" && ["°K", "°C", "°F"].includes(value)){
                    // Convert every temperature value into Kelvin
                    rowData["VALUE"] = temperatureInK(rowData["VALUE"], value).toString();
                    value = "°K"
                }
                rowData[key.toUpperCase()] = value;
            }
        }
        data.push(rowData);
    }
    // Output data
    return data
}

function getSubConditionTableData(cell, condition_type) {
    if (condition_type == "Relative Time"){
        var table = cell.firstElementChild.rows[1]

        var data = timeInSeconds(table)
    } else {
        var table = cell.firstElementChild

        var headers = [];
        var data = [];

        // Get headers
        var headerRow = table.rows[0];
        for (var i = 0; i < headerRow.cells.length; i++) {
            headers.push(headerRow.cells[i].textContent.trim());
        }

        // Get data
        for (var j = 1; j < table.rows.length; j++) {
            var row = table.rows[j];
            var rowData = {};
            for (var k = 0; k < row.cells.length; k++) {
                var key = headers[k];
                if (key != ""){
                    var value;
                    if (key == "Period"){
                        value = timeInSeconds(row.cells[k])
                    } else if (key.includes("Temperature")){
                        if (key == "Temperature Range"){
                            value = getTemperatureFromCell(row.cells[k], relative=true)
                        } else {
                            value = getTemperatureFromCell(row.cells[k])
                        }
                    }
                    else if (row.cells[k].querySelector("input")) {
                        value = row.cells[k].querySelector("input").value;
                    } 
                    else if (row.cells[k].querySelector("select")){
                        value = row.cells[k].querySelector("select").value;
                    } else {
                        value = row.cells[k].textContent.trim();
                    }
                    rowData[key] = value;
                }
            }
            data.push(rowData);
        }
    }
    // Output data
    return data
}

// SUBFUNCTIONS

function getTemperatureFromCell(cellData, relative=false){
    var inputTemperature = cellData.firstElementChild.value
    var unit = cellData.lastElementChild.value
    return temperatureInK(inputTemperature, unit, relative)
}

function timeInSeconds(cellData){
    var inputTime = cellData.firstElementChild.value
    var unit = cellData.lastElementChild.value
    return convertIntoSeconds(inputTime, unit)
}

//--------------------------------------------------------------------
// LOAD WORK
//--------------------------------------------------------------------

function loadWork(copy=false){
    const workSelect = document.getElementById("workSelect")
    if (workSelect.selectedIndex !== -1){
        const workId = workSelect.options[workSelect.selectedIndex].value;
        recvWorkData(workId, copy)
    }   
}

function loadWorkTable(data, copy){
    try{
        if (copy == true){
            copyWorkDiv(data)
        } else {
            initWorkDiv(data)
        }  
    } catch(err) {
        errorMessage = "The current work cannot be loaded because of conflicts with the existing setup"
        resetWorkDiv(errorMessage)
    }
}

// Condition Load 

function createLoadedConditionCell(conditionCell, condition){
    const conditionTable = document.createElement("table")
    const conditionTHead = conditionTable.createTHead()
    const conditionTBody = conditionTable.createTBody()
    var conditionHeaderRow = conditionTHead.insertRow()
    const conditionHeader = ["Type", "Variable", "Logic"]
    createTableHeader(conditionHeaderRow, conditionHeader)

    for (var i = 0; i < condition.length; i++){
        const conditionRow = conditionTBody.insertRow()
        for (var j = 0; j < Object.keys(condition[i]).length; j++){
            const subConditionCell = conditionRow.insertCell()
            if (conditionHeader[j] == "Variable"){
                if (condition[i]["TYPE"] == "Relative Time"){
                    subConditionCell.innerHTML = condition[i][conditionHeader[j].toUpperCase()] + " seconds"
                } else if (condition[i]["VARIABLE"].constructor == Array){
                    var subCondition = condition[i]["VARIABLE"]
                    var conditionType = condition[i]["TYPE"]
                    createLoadedSubConditionCell(subConditionCell, subCondition, conditionType)
                } 
            } else {
                subConditionCell.innerHTML = condition[i][conditionHeader[j].toUpperCase()]
            }
        }
    } 
    conditionCell.appendChild(conditionTable)
}

function createLoadedSubConditionCell(subConditionCell, subCondition, conditionType){
    var subConditionTable = document.createElement("table")
    var variableHeaderRow = subConditionTable.insertRow()
    const variableHeader = ConditionDict[conditionType]

    createTableHeader(variableHeaderRow, variableHeader)

    for (var i = 0; i < subCondition.length; i++){
        const conditionVariableRow = subConditionTable.insertRow()
        for (var j = 0; j < Object.keys(subCondition[i]).length; j++){
            const condtionVariableCell = conditionVariableRow.insertCell()
            condtionVariableCell.innerHTML = subCondition[i][variableHeader[j]]
            if (variableHeader[j] == "Period"){
                condtionVariableCell.innerHTML += " second(s)"
            } 
        }
    }
    subConditionCell.appendChild(subConditionTable)
}

// Statement Load 

function createLoadedStatementCell(statementCell, statement){
    const statementTable = document.createElement("table")
    const statementTHead = statementTable.createTHead()
    const statementTBody = statementTable.createTBody()
    var statementHeaderRow = statementTHead.insertRow()
    const statementHeader = ["Series", "Device ID", "Channel", "Parameter", "Value", "Unit"]

    createTableHeader(statementHeaderRow, statementHeader)

    for (var i = 0; i < statement.length; i++){
        const statementRow = statementTBody.insertRow()
        for (var j = 0; j < Object.keys(statement[i]).length; j++){
            const subStatementCell = statementRow.insertCell()
            if (statementHeader[j] == "Device ID"){
                subStatementCell.innerHTML = statement[i]["ID"]
            } else {
                subStatementCell.innerHTML = statement[i][statementHeader[j].toUpperCase()]
            }
        }
    } 
    statementCell.appendChild(statementTable)
}

function loadTaskRows(work){
    var table = document.getElementById("workTable")

    for (const taskPosition in work){
        const task = work[taskPosition]
        const condition = task.Condition
        const statement = task.Statement

        // Position Cell
        var newRow = table.insertRow()
        newRow.className = "task-row"
        const taskPositionCell = newRow.insertCell()
        taskPositionCell.innerHTML = taskPosition

        // Condition Cell
        var conditionCell = newRow.insertCell()
        createLoadedConditionCell(conditionCell, condition)

        // Statement Cell
        const statementCell = newRow.insertCell()
        createLoadedStatementCell(statementCell, statement)

        if (CampaignActive){
            activateButton("startWorkButton")
        }
    }
}

//--------------------------------------------------------------------
// COPY WORK
//--------------------------------------------------------------------

function copyWorkDiv(data){
    initWorkDiv({}, true)
    // Copy name and ID and add "_copy" to it
    var workName = document.getElementById("workName")
    workName.value = data["Name"] + "_copy"
    var workId = document.getElementById("workId")
    workId.innerHTML = data["ID"] + "_copy"

    // Init / Final Setup Table
    for (prefix of ["Init", "Final"]){
        if (data[prefix] != []){
            lowerPrefix = prefix.toLowerCase()
            button = document.getElementById(lowerPrefix + "SetupButton")
            contentDiv = document.getElementById(lowerPrefix + "SetupDiv")
            // Create Init / Final Statement Table
            if (data[prefix].length >0){
                initFinalButtonEvent(button, prefix, contentDiv, true)
            }
            initFinalTable = document.getElementById(lowerPrefix + "Table")
            for (rowData of data[prefix]){
                addStatementRow(initFinalTable.firstChild.lastChild, rowData)
            }
        }
    }
    
    // Work Table
    for (pos in data["Tasks"]){
        var conTaskCells = addNewConditionalTask(data["Tasks"], true)
        // Update Conditions
        for (rowData of data["Tasks"][pos]["Condition"]){
            conditionSelect = addConditionRow(conTaskCells[0].firstChild.lastChild,
                                             rowData["LOGIC"])
            tbody = changeConditionVariable(conditionSelect, rowData)
            var key = rowData["TYPE"]
            if (key != "Relative Time"){
                for (condition of rowData["VARIABLE"]){
                    addConditionParameters(tbody, ConditionDict[key], condition)
                } 
            } 
        }
        // Update Statements
        for (rowData of data["Tasks"][pos]["Statement"]){
            addStatementRow(conTaskCells[1].firstChild.lastChild, rowData)
        }
    }
}


//--------------------------------------------------------------------
// START / STOP WORK
//--------------------------------------------------------------------

function startWork(){
    if (CampaignActive){
        var wid = document.getElementById("workId").innerHTML;
        var workName = document.getElementById("workName")

        if (workName.querySelector("input")){
            var wname = workName.value;
        } else {
            var wname = workName.innerHTML;
        }

        var wIteration = document.getElementById("workIteration").value;
        msg = {"COM":"START",
                "WORK":{"ID": wid,
                        "NAME":wname,
                        "NOI":wIteration
                    }
                };

        sendCampaignData(msg)

        // Update visibility of buttons / inputs
        deactivateButton("startWorkButton")
        deactivateButton("workIteration")
        activateButton("stopWorkButton")
    }
}

function stopWork(){
    msg = {"COM":"STOP", "WORK":{}}
    deactivateButton("stopWorkButton")
    sendCampaignData(msg)
}


//--------------------------------------------------------------------
// SEQUENCE
//--------------------------------------------------------------------

function createSequence(){

}

function loadSequence(){

}

function startSequence(){

}

function stopSequence(){

}

