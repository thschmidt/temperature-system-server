

//--------------------------------------------------------------------
// PLOT FUNCTIONS
//--------------------------------------------------------------------

// CURENTLY NOT AVAILABLE !!!!
function initGraph(seriesName, data){
    var parameter = document.getElementById(seriesName + '_parameterSelect').value;

    // Create dataset traces
    var traces = []
    for (var device in data){
        traces.push({x:data[device]['Timestamp'],
                y:data[device][parameter], 
                mode: 'lines',
                name: device
        })
    }

    var layout = {
        xaxis: {
            title: 'Time'
        },
            yaxis: {
                title: data[parameter + "_Unit"] || 'Value'
        },
        margin: { l: 100, r: 50, t: 10, b: 50 },
        height: 300
    };
    Plotly.newPlot(seriesName + '_graph', [traces], layout, {displayModeBar: false});
}

function updateGraph(deviceName, data) {
    var parameter = document.getElementById(deviceName + '_parameterSelect').value;
    x_values = data.Time
    y_values = data[parameter]
    Plotly.extendTraces(deviceName + '_graph', { x: [x_values], y: [y_values]}, [0], 10);

}

//--------------------------------------------------------------------
// SEND
//--------------------------------------------------------------------

function setParameter(seriesInput){
    /* Check the input parameters in its value and send it to the server */
    if (seriesInput !== null) {
        var deviceId = seriesInput.id.split('--')[0]
        var channel = seriesInput.id.split('--')[1]
        var parameter = seriesInput.id.split('--')[2]
        
        var error_msg = ''
        // Check if input type is correct
        if (seriesInput.hasOwnProperty('inputType')){
            var value = seriesInput.value
            var inputType = seriesInput.inputType
            if (inputType == 'Switch'){
                value = seriesInput.checked ? 1 : 0
            }
            else if (inputType == 'Button') {
                value = 1
            }
            var limits = seriesInput.limits
            if (limits != undefined){
                if (value > limits[1] || value < limits[0]){
                    error_msg = ("Value of '" + parameter + "' is outside of its limits: "
                                 + limits[0] + " : "+ limits[1])
                }
            }
            if (Temperature_Units.includes(seriesInput.unit)){
                value = temperatureInK(value, seriesInput.unit)
            }
        }  

        if (error_msg == ''){
            // Define the message
            msg = {'COM':'SET',
                'SERIES': seriesInput.name,
                'DATA':{'ID': deviceId,
                        'CHANNEL': channel,
                        'PARAMETER': parameter,
                        'VALUE': value
                    }}
            sendStatusData(msg);
        }
        else {
            alert(error_msg)
        }
    }
}

function connectDevice(deviceId, selectedSeries){
    /* Send a dis-/ connection request for the selected device to the server */
    state = document.getElementById(`${deviceId}_state`).innerText;
    connect = state === "Offline" ? true : false
    // Define the message
    msg = {'COM':'CONNECT',
            'SERIES':selectedSeries,
            'DATA':{'ID': deviceId, 'VALUE':connect}}
    //console.log(msg)
    sendStatusData(msg);
}

//--------------------------------------------------------------------
// CONTROLLER FUNCTIONS
//--------------------------------------------------------------------

function generateSectionChannelSeries(container, statusData, seriesName) {
    /* Creation of the container including a table with editable parameters, 
    connection status and more */

    // Fetch the name of the series
    var seriesLongName = seriesName
    if ('Details' in statusData['GENERAL']){
        if ('Name' in statusData['GENERAL']['Details']){
            seriesLongName = statusData['GENERAL']['Details']['Name']
        }
    }

    // Create the header of the container with the selection to toggle its visibility
    var header = document.createElement("div");
    header.className = "series-header";
    header.textContent = seriesLongName;
    header.setAttribute("onclick", "toggleSection(this)");
    container.appendChild(header);

    // Create the container of the series
    var seriesContainer = document.createElement("div");
    seriesContainer.id = seriesName
    seriesContainer.className = 'series-container'

    // Create a table which will include all devices of the series
    var seriesTable = document.createElement("table")
    seriesTable.className = 'series-table'
    var seriesTableRow = document.createElement("tr");
    
    // Initialize graph (currently not available) and inital series data of the devices
    var graphData = {}
    seriesData = statusData['ID']

    // Maximum Channels / Devices per row
    var max_row = 4
    var row_counter = 0

    // Loop over every device (with deviceId) in series
    Object.keys(seriesData).forEach(function(deviceId) {
        var deviceLongName = deviceId
        
        var device = seriesData[deviceId];

        // Create a new Row if two much Channels / Devices in one Row
        row_counter += Object.keys(device.Channel).length;
        if (row_counter > max_row && Object.keys(device.Channel).length != row_counter){
            seriesTable.appendChild(seriesTableRow);
            seriesTableRow = document.createElement("tr");
            row_counter = 0;
        }

        graphData[deviceId] = {'Timestamp':device['LastReadout']}

        // Create a section (td) for the device
        var section = document.createElement("td");
        section.className = `device-section`;

        // Create a header for the device with its long name (will be editable in the future)
        var header = document.createElement("div");
        header.className = "device-header";
        header.textContent = deviceLongName;
        section.appendChild(header);

        // Create a container for the device content
        var content = document.createElement("div");
        content.className = "device-content";

        // Create a table for the channels of the device
        var deviceTable = document.createElement("table");
        deviceTable.className = 'device-channel-table';
        var deviceTableRow = document.createElement("tr");

        // Loop over every channel of the device
        for (var channelName in device.Channel){
            var channelTd = document.createElement("td");

            // Create a header of each channel with its channel number as name (will be editable in the future)
            var channelHeader = document.createElement("div");
            channelHeader.className = "device-channel-header";
            channelHeader.textContent = channelName;

            // Create the table with the parameter info
            var channelTable = document.createElement("table");
            //channelTable.className = 'device-channel-table'

            // Create the header of this table
            var thead = document.createElement("thead");
            var channelHeaderRow = document.createElement("tr");
            ["Parameter", "Set", "Read", "Unit"].forEach(function(headerText) {
                var th = document.createElement("th");
                th.textContent = headerText;
                channelHeaderRow.appendChild(th);
            });
            thead.appendChild(channelHeaderRow);
            channelTable.className = 'channel-table'
            channelTable.appendChild(thead);

            // Create an additional table below for action buttons
            var buttonTable = document.createElement("table");
            buttonTable.className = 'button-table';

            // Create the body of the channel table
            var tbody = document.createElement("tbody");
            for (var parameterName in device.Channel[channelName]) {
                // Create a row for the parameter
                var parameter = device.Channel[channelName][parameterName];
                var row = document.createElement("tr");
                var parameterCell = document.createElement("td");
                parameterCell.textContent = parameterName.replace(/_/g, " ");
                parameterCell.className = 'cell-parameter'
                row.appendChild(parameterCell);
                // Depending on the InputType, configure the input of the button or channel/parameter table
                if (parameter['InputType'] == 'Switch'){
                    // Switches are added to the button table
                    var toggleSwitch = document.createElement('label');
                    toggleSwitch.className = 'toggle-switch';
                    var checkbox = document.createElement('input');
                    checkbox.type = 'checkbox';
                    checkbox.id = deviceId + "--" + channelName + "--" + parameterName + '--SET';
                    var span = document.createElement('span');
                    span.className = 'slider';
                    checkbox.name = seriesName;
                    checkbox.inputType = parameter.InputType;
                    checkbox.onclick = function(){
                        setParameter(this)
                    };
                    toggleSwitch.appendChild(checkbox);
                    toggleSwitch.appendChild(span);
                    row.appendChild(toggleSwitch);
                    buttonTable.appendChild(row);
                } else if (parameter['InputType'] == 'Button'){
                    // Buttons are added to the button table
                    var button = document.createElement('button');
                    button.id = deviceId + "--" + channelName + "--" + parameterName + '--SET';
                    button.name = seriesName;
                    button.innerHTML = 'Submit';
                    button.inputType = parameter.InputType;
                    button.onclick = function(){
                        setParameter(this)
                    };
                    row.appendChild(button);
                    buttonTable.appendChild(row);
                } else if (parameter['InputType'] == 'Select'){ 
                    // Seletions are added to the channel/parameter table
                    var setCell = document.createElement("td");
                    if (parameter.hasOwnProperty('Set')) {
                        var setSelect = document.createElement("select");
                        setSelect.name = seriesName;
                        setSelect.id = deviceId + "--" + channelName + "--" + parameterName + "--SET";
                        for (let val of parameter['Values']){
                            var option = document.createElement("option");
                            option.text = val
                            option.value = val
                            setSelect.appendChild(option);
                        }    
                        setSelect.inputType = parameter.InputType;
                        setSelect.onchange = function(){setParameter(this)};
                        setSelect.className = 'cell-select'
                        setCell.appendChild(setSelect);
                    }
                    row.appendChild(setCell);
                    var readCell = document.createElement("td");
                    if (parameter.hasOwnProperty('Read')) {
                        readCell.id = deviceId + "--" + channelName + "--" + parameterName + '--READ';
                        readCell.textContent = parameter.Read;
                        readCell.className = 'cell-read'
                        //graphData[deviceId][parameterName] = [parameter.Read]
                    }
                    row.appendChild(readCell);
                    var unitCell = document.createElement("td");
                    if (parameter.Unit !== undefined) {
                        unitCell.textContent = parameter.Unit;
                    }
                    row.appendChild(unitCell);
                    tbody.appendChild(row);
                    
                } else {
                    // Float / Int or String InputTypes are added to the channel/parameter table
                    var setCell = document.createElement("td");
                    if (parameter.hasOwnProperty('Set')) {
                        var setInput = document.createElement("input");
                        setInput.name = seriesName;
                        setInput.type = "text";
                        setInput.id = deviceId + "--" + channelName + "--" + parameterName + "--SET";
                        setInput.inputType = parameter.InputType;
                        setInput.limits = parameter.Limits;
                        [setInput.value, setInput.unit] = checkTemperatureConversion(parameter.Set, parameter.Unit)
                        if (setInput.inputType == 'Float'){
                            setInput.addEventListener('keypress', function(event){
                                restrictInputToFloat(event, this, true)});
                        } else if (setInput.inputType == 'Int'){
                            setInput.addEventListener('keypress', function(event){
                                restrictInputToNumber(event, this, true)});
                        }
                        setInput.onchange = function(){setParameter(this)};
                        setInput.className = 'cell-input'
                        setCell.className = 'cell-set'
                        setCell.appendChild(setInput);
                    }
                    row.appendChild(setCell);
                    var readCell = document.createElement("td");
                    if (parameter.hasOwnProperty('Read')) {
                        readCell.id = deviceId + "--" + channelName + "--" + parameterName + '--READ';
                        [readCell.textContent, readCell.unit] = checkTemperatureConversion(parameter.Read, parameter.Unit);
                        //graphData[deviceId][parameterName] = [parameter.Read]
                    }
                    row.appendChild(readCell);
                    var unitCell = document.createElement("td");
                    if (parameter.Unit !== undefined) {
                        [, unitCell.textContent] = checkTemperatureConversion(0, parameter.Unit);
                    }
                    unitCell.className = 'cell-unit'
                    row.appendChild(unitCell);
                    tbody.appendChild(row);
                }   
            };
            // Add the body with the parameter data to the channel table
            channelTable.appendChild(tbody);
            // Add the header, this table and the button table to the channel container (td)
            channelTd.appendChild(channelHeader);
            channelTd.appendChild(channelTable);
            channelTd.appendChild(buttonTable);
            // 
            deviceTableRow.appendChild(channelTd);
        }
        //
        deviceTable.appendChild(deviceTableRow);

        // Create connection table at the end of each device
        var connectionTable = document.createElement("table");
        var connectionRow = document.createElement("tr");
        connectionTable.className = 'device-table';

        // Define Readout cell
        var lastReadoutCell = document.createElement("td");
        lastReadoutCell.id = deviceId + '-lastReadout';
        lastReadoutCell.textContent = "Last Readout: " + device.LastReadout + ' UTC';
        connectionRow.appendChild(lastReadoutCell);

        // Define connection status cell
        var connnectiontCell = document.createElement("td");
        connnectiontCell.id = `${deviceId}_state`;
        connnectiontCell.textContent = "Offline";
        connectionRow.appendChild(connnectiontCell);

        // Create cell for the connection button
        var connnectiontCell = document.createElement("td");
        var connectButton = document.createElement("button");
        connectButton.id = `${deviceId}_connect`;
        connectButton.innerHTML = "Connect";
        connectButton.className = 'connect-button';
        connectButton.onclick = function() {
            connectDevice(deviceId, seriesName);
        };
        // Combine the connection table
        connnectiontCell.appendChild(connectButton);
        connectionRow.appendChild(connnectiontCell);
        connectionTable.appendChild(connectionRow);

        // Append device table and connection table
        content.appendChild(deviceTable);
        content.appendChild(connectionTable);

        // Append the content of this device in the series table
        section.appendChild(content);
        seriesTableRow.appendChild(section);
    });
    seriesTable.appendChild(seriesTableRow);
    seriesContainer.appendChild(seriesTable);

    // Add everything to the series container
    container.appendChild(seriesContainer);
    
    // CURRENTLY NOT AVAILABLE THE GRAPHS!!!

    // Create Graph Box below
/*     var graphBox = document.createElement("div");
    graphBox.className = "graph-box";
    graphBox.id = seriesName + "_graph_box";
    
    var parameterSelect = document.createElement("select");
    parameterSelect.id = seriesName + '_parameterSelect'
    for (parameterName of parameters){
        var parameterSelector = document.createElement("option");
        parameterSelector.text = parameterName;
        parameterSelector.value = parameterName;
        parameterSelect.appendChild(parameterSelector);
    }
    graphBox.append(parameterSelect)

    var graph = document.createElement("div")
    graph.id = seriesName + '_graph'
    graphBox.append(graph)

    seriesContainer.appendChild(graphBox);
    container.appendChild(seriesContainer);
    initGraph(seriesName, graphData) */
}

function updateTables(seriesData){
    /* Update data in the series tables */
    for (deviceName in seriesData){
        var graphData = {'Time':[seriesData[deviceName]['LastReadout']]};
        var parameters = seriesData[deviceName]['Channel'];
        var channels = Object.keys(seriesData[deviceName]['Channel']);

        // Loop over every channel
        for (var channel of channels){
            for (var parameterName in parameters[channel]){
                var parameter = parameters[channel][parameterName];
                // Update every parameters 'Read' and 'Set' value
                if (parameter.hasOwnProperty('Read')) {
                    var readParamater = deviceName + '--' + channel + '--' + parameterName + '--READ';
                    var readCell = document.getElementById(readParamater);
                    if (readCell !== null){
                        [readCell.textContent, readCell.unit] = checkTemperatureConversion(parameter.Read, parameter.Unit);
                        //graphData[parameterName] = [parameter.Read];
                    }
                }
                if (parameter.hasOwnProperty('Set')) {
                    var setParamater = deviceName + '--' + channel + '--' + parameterName + '--SET';
                    if (document.activeElement !== document.getElementById(setParamater)){
                        var setCell = document.getElementById(setParamater);
                        [setCell.value, setCell.unit] = checkTemperatureConversion(parameter.Set, parameter.Unit);
                        // If cell is a checkbox update the checked status
                        if (setCell.type == "checkbox"){
                            setCell.checked = String(parameter.Set) === '1' ? true : false;
                        } 
                    }
                }
            }
        }
        // Update the connection status
        lastReadout = document.getElementById(deviceName + '-lastReadout');
        lastReadout.textContent = "Last Readout: " + seriesData[deviceName]['LastReadout'] + ' UTC';
        //updateGraph(deviceName, graphData)
    }
}

//--------------------------------------------------------------------
// REQUESTS
//--------------------------------------------------------------------

function initData(statusData){
    /* Initialize the received status data */
    for (series in statusData['temp_control']){
        category = statusData['temp_control'][series]['GENERAL']['Category'];

        var categoryContainer = document.getElementById(category.toLowerCase()  + "Div");

        if (categoryContainer.innerHTML == ""){
            categoryContainer.innerHTML = ""; // Clear previous content
        
            var header = document.getElementById(category.toLowerCase() + 'Header');
            header.textContent = category.replace('_', ' ');
            header.className = 'category-header'
            
        }
        seriesData = statusData['temp_control'][series];
        generateSectionChannelSeries(categoryContainer, seriesData, series)   
    }
}

function updateData(statusData){
    /* Update the tables and plots with the recevied data */
    for (series in statusData['temp_control']){
        if (jQuery.isEmptyObject(statusData['temp_control'][series]) == false){
            category = statusData['temp_control'][series]['GENERAL']['Category'];
            seriesData = statusData['temp_control'][series]['ID'];
            updateTables(seriesData);
            for (deviceId in statusData['temp_control'][series]['ID']){
                device = statusData['temp_control'][series]['ID'][deviceId];
                updateConnectButton(deviceId, device['State'], statusData);
            }
        }    
    }
}

function updateConnectButton(deviceId, state, statusData){
    /* Update the connection button of a device */
    connectButton = document.getElementById(`${deviceId}_connect`);
    stateElement = document.getElementById(`${deviceId}_state`);
    if (connectButton !== null){
        connectButton.innerHTML = state === "Offline" ? "Connect" : "Disconnect";
        statusData['campaign']['Active'] ? deactivateButton(`${deviceId}_connect`) : activateButton(`${deviceId}_connect`);
        stateElement.innerHTML = state;
    }
}

//--------------------------------------------------------------------
// USER INPUTS
//--------------------------------------------------------------------

function toggleSection(header) {
    /* Function to toggle display of device content */
    content = header.nextElementSibling;
    content.style.display = content.style.display === "none" ? "block" : "none";
}

