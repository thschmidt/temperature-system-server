/* start

*/

//--------------------------------------------------------------------
// GLOBAL VARIABLE
//--------------------------------------------------------------------


//--------------------------------------------------------------------
// CAMPAIGN
//--------------------------------------------------------------------

function updateCampaignData(resp){
    for (group in resp['Devices']){
        createContainer(group)
        createDeviceTables(resp['Devices'][group], group)
    }
    for (group in resp['Sensors']){
        createSensorTable(resp['Sensors'][group], group)
    }
    updateCampaignDetails(removeFromDictionary(resp, ['Sensors', 'Devices']))
}

//--------------------------------------------------------------------
//  CHECK
//--------------------------------------------------------------------

function variableCheck(varList){
    for (variable of varList){
        if (variable == ""){
            console.log(variable)
            return false
        }
    }
    return true
}

//--------------------------------------------------------------------
// PREPARE
//--------------------------------------------------------------------

function initData(statusData){
    createOptionsCampaign(statusData)
    currentCampaign = statusData['campaign']['CurrentCampaign']
    if (currentCampaign != ''){
        recvCampaignData(currentCampaign)
    }
}

//--------------------------------------------------------------------
// DEVICE FUNCTIONS
//--------------------------------------------------------------------


function deleteDevice(btn) {
    var row = btn.parentNode.parentNode;
    row.parentNode.removeChild(row);
}

//--------------------------------------------------------------------
// SUBFUNCTIONS
//--------------------------------------------------------------------

function convertToList(text) {
    elements = text.split(",").map(element => element.trim()).filter(element => element !== "");
    return elements
}

function convertSensorType(dictionary){
    const result = [];
    for (const key in dictionary) {
        if (dictionary.hasOwnProperty(key)) {
            const values = dictionary[key];
            values.forEach(value => {
                result.push(`${key}-${value}`);
            });
        }
    }
    return result
}

function convertDictToString(dict, level=''){
    var str = ''
    for (const did in dict){
        if (dict[did].constructor == Object) {
            str += level + did + '<br>'
            str += convertDictToString(dict[did], level + ' :')
        }
        else{
            str += level + did + ' :' + dict[did]
        }
        str += '<br>'
        
    }
    return str
}


//--------------------------------------------------------------------
// MODAL FUNCTIONS
//--------------------------------------------------------------------

function createNewCampaign(){
    if (Init_The_Settings['campaign']['Active']){
        alert("Cannot create a new campaign as long as another campaign is active")
    }
    else {
        window.location.href = "/create_campaign";
    }
    
}

function editCampaign(){
    window.location.href = "/edit_campaign";
}


//------------------------------ UPDATE CAMPAIGN --------------------------------------
