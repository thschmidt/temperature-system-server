
//--------------------------------------------------------------------
//  SEND
//--------------------------------------------------------------------


function sendCampaignData(data){
    $.ajax({
        type: "POST",
        url: "/campwork_exchange",
        dataType: "json",
        contentType: "application/json",
        data: JSON.stringify(data)
    })
}

function sendStatusData(data){
    $.ajax({
        type: "POST",
        url: "/setting_exchange",
        dataType: "json",
        contentType: "application/json",
        data: JSON.stringify(data)
    })
}

