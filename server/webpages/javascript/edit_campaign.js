/* 

*/

//--------------------------------------------------------------------
// GLOBAL VARIABLE
//--------------------------------------------------------------------



//--------------------------------------------------------------------
// CAMPAIGN
//--------------------------------------------------------------------

function stopCampaign(){
    data = {'COM':'STOP',
            'CAMPAIGN':{'DEVICES':{}}}
    sendCampaignData(data)
    deactivateButton('stopButton')
}

function updateCampaignData(resp){
    for (group in resp['Devices']){
        createContainer(group)
        createDeviceTables(resp['Devices'][group], group, edit=true)
    }
    for (group in resp['Sensors']){
        createSensorTable(resp['Sensors'][group], group, edit=true)
    }

    updateCampaignDetails(removeFromDictionary(resp, ['Sensors', 'Devices']))
    if (resp['Active']){
        activateButton('stopButton')
    }
    else{
        deactivateButton('stopButton')
    }
}


//--------------------------------------------------------------------
// PREPARE
//--------------------------------------------------------------------

function initData(statusData){
    createOptionsCampaign(statusData)
    currentCampaign = statusData['campaign']['CurrentCampaign']
    if (currentCampaign != ''){
        recvCampaignData(currentCampaign)
    }
}

//--------------------------------------------------------------------
// DEVICE FUNCTIONS
//--------------------------------------------------------------------


function getTableData2Row(tableId){
    var table = document.getElementById(tableId);

    var data = {};

    for (var j = 0; j < table.rows.length; j++) {
        var row = table.rows[j];
        var value
        if (row.cells[1].querySelector('input')) {
            value = row.cells[1].querySelector('input').value;
        } 
        else if (row.cells[1].querySelector('select')){
            value = row.cells[k].querySelector('select').value;
        } else {
            value = row.cells[1].textContent.trim();
        }
        data[row.cells[0].textContent.trim()] = value
    }
    return data
}

function getTableSensorData(tableId, group) {
    var table = document.getElementById(tableId);

    var temperature_unit = getDefaultTemperatureUnit()
    
    var groupData = {}

    var headers = [];
    var data = {};

    // Get headers
    var headerRow = table.rows[0];
    for (var i = 0; i < headerRow.cells.length; i++) {
        headers.push(headerRow.cells[i].textContent.trim());
    }

    // Get data
    for (var j = 1; j < table.rows.length; j++) {
        var row = table.rows[j];
        series = row.id.split('-')[1]
        if (series in data == false){
            data[series] = []
        }
        var rowData = {};
        for (var k = 0; k < row.cells.length; k++) {
            var key = headers[k];
            var value;
            if (row.cells[k].querySelector('input')) {
                value = row.cells[k].querySelector('input').value;
                if (key.includes('Limit')){
                    value = temperatureInK(value, temperature_unit)
                } 
            } 
            else if (row.cells[k].querySelector('select')){
                value = row.cells[k].querySelector('select').value;
            } else {
                value = row.cells[k].textContent.trim();
            }
            rowData[key] = value;
        }
        data[series].push(rowData);
    }
    // Output data
    groupData[group] = data

    return groupData
}

function getTableDeviceData(tableId, series) {
    var table = document.getElementById(tableId);

    var seriesData = {}

    var headers = [];
    var data = {};

    // Get headers
    var headerRow = table.rows[0];
    for (var i = 0; i < headerRow.cells.length; i++) {
        headers.push(headerRow.cells[i].textContent.trim());
    }

    // Get data
    for (var j = 1; j < table.rows.length; j++) {
        var row = table.rows[j];

        var rowData = {};
        for (var k = 0; k < row.cells.length; k++) {
            var key = headers[k];
            if (row.cells[k].querySelector('input')) {
                rowData[key] = row.cells[k].querySelector('input').value;
            } else if (key == 'ActiveSensors'){
                value = row.cells[0].textContent.trim()
                if (value != undefined){
                    rowData[key] = value
                } 
            }
        }
        data[row.cells[0].textContent.trim()] = rowData
    }
    // Output data
    seriesData[series] = data

    return seriesData
}

//--------------------------------------------------------------------
// SUBFUNCTIONS
//--------------------------------------------------------------------

function showLineStyles(line_style){
    const styles = ['solid', 'dash', 'dot', 'dashdot']
    if (!styles.includes(line_style)){
        line_style = 'solid'
    } 
    var options = '<option>' + line_style + '</option>'
    for (var s of styles){
        if (s != line_style){
            options += '<option>' + s + '</option>'
        }
    }
    return options
}

//------------------------------ UPDATE CAMPAIGN --------------------------------------

function saveCampaign(){
    var sensorTableData = {}
    for (group of ['data_logger', 'temp_control']){
        groupSensorTable = document.getElementById(group + '-sensorTable')
        sensorTableData[group] = {}
        if (groupSensorTable !== null){
            sensorTableData[group] = getTableSensorData(group + '-sensorTable', group)[group]
        }
    }
    errorMessage = checkTableData(sensorTableData)
    var deviceTableData = {}
    for (group of ['data_logger', 'temp_control']){
        deviceTableData[group] = {}
        for (series in Init_The_Settings[group]){
            seriesDeviceTable = document.getElementById(group + '-' + series + '-deviceTable')
            deviceTableData[group][series] = {}
            if (seriesDeviceTable !== null){
                deviceTableData[group][series] = (getTableDeviceData(group + '-' + series + '-deviceTable', series))[series]
            } 
        }
    }
    if (errorMessage == ""){
        closeAlert()
        data = {...{'Sensors':sensorTableData}, 
                ...{'Devices':deviceTableData},
                ...getTableData2Row('campaignDetails')}
        var campaignSelect = document.getElementById("campaignSelect");
        var cid = campaignSelect.options[campaignSelect.selectedIndex].value;
        sendCampaignData({'COM':'SET', 'CAMPAIGN':{'ID': cid, 'DATA':data}});
    }
    else {
        msgtype = 'warning'
        openAlert(errorMessage, msgtype)
    }
}


function checkTableData(sensorData){
    const limits = ['Lower Limit', 'Upper Limit']

    for (group in sensorData){
        for (series in sensorData[group]){
            tableData = sensorData[group][series]
            for (var line of tableData){
                for (var l of limits){
                    if (!(isFloat(line[l]))){
                        return l + " of device ID '" + line['ID'] + "' is not a Number" 
                    }
                    if (line[l] < 0){
                        return (l + " of device ID '" + line['ID']
                        + "' is below absolute zero")
                    }
                    if (line[l] > 1000){
                        return (l + " of device ID '" + line['ID']
                        + "' is above the limit of " + 1000)
                    }
                }
                if (line[limits[0]] >= line[limits[1]]){
                    return ("The limits of device ID '" + line['ID']
                    + "' are equal or the lower limit is bigger than the upper")
                }
            }
            return ""
        }
    }
}


