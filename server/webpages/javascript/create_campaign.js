/* 

*/

//--------------------------------------------------------------------
// GLOBAL VARIABLE
//--------------------------------------------------------------------


var Allowed_Sensors = {}
var Last_State = {}

//--------------------------------------------------------------------
// CAMPAIGN
//--------------------------------------------------------------------

function initCampaign(){
    var copyCampaign = document.getElementById("copyCampaign").value;
    if (copyCampaign === undefined){
        copyCampaign = ''
    }
    // Campaign name and id
    const cname = document.getElementById('campaignName').value;  
    const cid = document.getElementById("campaignId").innerHTML;
    // Readout Interval
    const interval = document.getElementById('interval').value;
    var errorMessage = checkCampaignInput(cname, interval, cid);
    // Close previous Alert Window
    closeAlert()
    var convSensorData = {};
    var convDeviceData = {};
    var devices = {};
    // Loop through the two groups
    for (var group of ['data_logger', 'temp_control']){
        // Check if the table of the group exists
        if (document.getElementById(group + "-table").rows[0] !== undefined){
            var groupData = getTableData(group + "-table");
            if (groupData != []){
                convSensorData[group] = {};
                convDeviceData[group] = {};
                var msg = {}
                for (deviceNumber in groupData){
                    // Create the msg structure for each series
                    var series = groupData[deviceNumber]['Series'];
                    var deviceId = groupData[deviceNumber]['Device ID'];
                    // Check if device is still online
                    if (Last_State[deviceId] != 'Online'){
                        errorMessage = deviceId + ": Device is " + Last_State[deviceId]
                        break
                    } 
                    var sensorGDat = groupData[deviceNumber]['Sensors']
                    if (sensorGDat != ''){
                        // Check for faulty sensor input
                        var sensorDict = checkSensorInput(sensorGDat, deviceId);
                        if (typeof sensorDict === 'string'){
                            errorMessage = sensorDict;
                            break
                        }
                    } else {
                        sensorDict = ''
                    }
                    var channelString = groupData[deviceNumber]['Channel']
                    if (series in convDeviceData[group] == false){
                        convDeviceData[group][series] = {};
                        devices[series] = {}
                    }
                    // Create Device and Sensor Details content
                    var deviceData = {}
                    if (sensorDict !== ''){
                        numberOfSensors = sumArraysLengthInDictionary(sensorDict);
                        if (series in convSensorData[group] == false){
                            convSensorData[group][series] = {};
                        }
                        convSensorData[group][series][deviceId] = sensorDict;
                        deviceData = {...deviceData, ...{'ActiveSensors':numberOfSensors}};
                    }
                    if (channelString != ''){
                        deviceData = {...deviceData, ...{'Channel':{}}};
                        channelList = channelString.split(', ')
                        for (ch of channelList){
                            deviceData['Channel'][ch] = {'Name':'', 'Description':''};
                        } 
                    }
                    deviceData = {...deviceData, ...{'Name':'', 'Description':''}};
                    
                    convDeviceData[group][series][deviceId] = deviceData
                    devices[series][deviceId] = {'SENSOR':sensorDict,
                                                'CHANNEL':'',
                                                'INTERVAL':interval}            
                }  
            }
        }
    }
    if (errorMessage != ''){
        msgtype = 'warning';
        openAlert(errorMessage, msgtype);
    } else {
        // If no error occured sent the data to the logger and modules
        for (series in devices){
            msg = {'COM':'INIT',
                'SERIES':series,
                'DEVICES':devices[series]};
            sendCampaignData(msg);
        }
        msgLogger = {'COM':'INIT',
                    'CAMPAIGN':{'ID': cid,
                                'NAME':cname,
                                'INTERVAL':interval,
                                'SENSORS':convSensorData,
                                'DEVICES':convDeviceData,
                                'COPY':copyCampaign
                            }};
        sendCampaignData(msgLogger); 
        setTimeout(activateButton, 1000, 'startButton');
    }  
}


function startCampaign(){
    var int = document.getElementById('interval').value;
    var cid = document.getElementById("campaignId").innerHTML;
    var cname = document.getElementById("campaignName").value;
    if (isInteger(int)){
        msg = {'COM':'START', 'CAMPAIGN':{'ID': cid,
                                        'NAME':cname,
                                        'INTERVAL':int,
                                        'DEVICES':{}}}
        //console.log(msg)
        sendCampaignData(msg)
        deactivateButton('startButton')
        deactivateButton('initButton')
        deactivateButton('copyButton')
        // Redirect to campaign page
        setTimeout(redirectCampaign, 200);
    } else{
        msg = "The Readout Rate parameter is not an Number"
        msgtype = 'warning'
        openAlert(msg, msgtype)
    } 
}

function redirectCampaign(){
    current_host = window.location.host
    protocol = window.location.protocol
    window.location.replace(protocol + '//' + current_host + '/campaign')
}

function connectAllDevices(){
    devicesList = []
    /* Connect to all devices from the copied campaign */
    for (var group of ['data_logger', 'temp_control']){
        // Check if the table of the group exists
        if (document.getElementById(group + "-table").rows[0] !== undefined){
            var groupData = getTableData(group + "-table");
            for (deviceNumber in groupData){
                // Create the msg structure for each series
                var series = groupData[deviceNumber]['Series'];
                var deviceId = groupData[deviceNumber]['Device ID'];
                if (Last_State[deviceId] == 'Offline'){
                    connectDevice(deviceId, series)
                };
                // Create a list of used device IDs
                devicesList.push(deviceId)
            }
        }
    }
    let i = 0;
    // Check if devices have been successfully connected
    const intervalId = setInterval(() => {
        const offlineDevices = devicesList.filter(key => Last_State[key] === 'Offline');
        if (offlineDevices.length === 0) {
            clearInterval(intervalId);
            deactivateButton('connectAllButton')
        } else if (i > 10){
            // After a timeout of approx 5 seconds return an error
            var errorMessage = ("Can't connect to the following device(s): " +  offlineDevices.toString())
            msgtype = 'warning';
            openAlert(errorMessage, msgtype);
            clearInterval(intervalId);
        }
        i++;
    }, 500); 
}


function connectDevice(deviceId, selectedSeries){
    /*Send the dis-/connection request to server*/
    state = Last_State[deviceId];
    if (state == 'Offline'){
        connect = true
    } else {
        connect = false
    }
    msg = {'COM':'CONNECT',
            'SERIES':selectedSeries,
            'DATA':{'ID': deviceId, 'VALUE':connect}}

    sendStatusData(msg);
}


//--------------------------------------------------------------------
//  CHECK
//--------------------------------------------------------------------

function checkCampaignInput(cname, readrate, cid){
    if (!isInteger(readrate)){
        return "The Readout Rate parameter is not an Number."
    }
    if (Init_The_Settings['campaign']['CampaignLists'][0].includes(cid)){
        return ('Campaign id has already been defined.' +
                'Please choose a different name for your campaign.')
    }
    if (cname != '' && readrate >= 5){
        return ''
    } else if (cname == ''){
        return ('No campaign name has been defined.')
    } else if (readrate < 5) {
        return ('The Readout Rate is too low. Minimum readout rate is currently 5 sec.')
    } else {
        return ("Something is wrong with your input please check it again.")
    }
}


function checkIdInTable(table, did, col=1){
    for (var j = 1; j < table.rows.length; j++){
        var deviceId = table.rows[j]['cells'][col].innerHTML
        if (did == deviceId){
            return true
        }
    }
    return false
}


function checkSensorInput(sensorData, deviceId){
    /* Check the content in the sensor input*/
    var sensorDeviceData = {};
    var sensorList = [];
    for (element of sensorData){
        if (sensorDeviceData.hasOwnProperty(element['SensorType'])){
            return deviceId + ": The same sensor type is given at least twice";
        }
        else{
            var sensors = convertToList(element['Sensors'])
            var sensorType = element['SensorType'].split('-')[0];
            if (sensors ==''){
                return deviceId + ": No sensor has been selected for sensor type " + sensorType;
            }
            var allowedSensors = [];
            const ranges = Allowed_Sensors[deviceId][sensorType].split(';');
            ranges.forEach(element => {
                allowedSensors.push(...convertRangeToList(element));
            }); 
            if (!sensors.every(item => allowedSensors.includes(item))){
                return deviceId + ": At least one of the sensors is not allowed";
            } else if (sensors.length === 0){
                return deviceId + ": Wrong sensor input";
            } else {
                sensorDeviceData[element['SensorType']] = sensors;
                sensorList = sensorList.concat(sensors);
            }
        }  
    };
    if (jQuery.isEmptyObject(sensorDeviceData)){
        return deviceId + ": No sensor has been selected";
    }
    if (hasDuplicate(sensorList)){
        return deviceId + ": At least one sensor is defined twice";
    }
    return sensorDeviceData
}

function idExistsInTable(tableId, deviceId){
    var table = document.getElementById(tableId);

    // Get data
    for (var j = 1; j < table.rows.length; j++) {
        var row = table.rows[j];
        if (row.cells[1].textContent == deviceId){
            return true
        };
    }
    return false
}

//--------------------------------------------------------------------
// PREPARE
//--------------------------------------------------------------------

function initData(statusData){
    createOptionsCampaign(statusData)
}

function updateData(statusData){
    /* Update the details of the device in the popup window */
    for (group in statusData){
        if (group != 'website'){
            for (series in statusData[group]){
                for (deviceId in statusData[group][series]['ID']){
                    // Update its last connection
                    deviceData = statusData[group][series]['ID'][deviceId];
                    connectElement = document.getElementById(deviceId + '_LastConnection');
                    if (connectElement !== null) {
                        connectElement.innerHTML = deviceData['LastConnection'];
                    }
                    // Update its state
                    stateElement = document.getElementById(deviceId + '_State');
                    if (stateElement !== null) {
                        stateElement.innerHTML = deviceData['State'];
                    }
                    // Update its allowed sensors
                    if ('Sensors' in deviceData){
                       sensorElement = document.getElementById(deviceId + '_Sensors');
                        if (sensorElement !== null) {
                            let sensorString = "";
                            for (const sensorType in deviceData['Sensors']) {
                                if (deviceData['Sensors'] === null) {
                                    sensorString += sensorType + '\n';
                                } else {
                                    sensorString += sensorType + ": " + deviceData['Sensors'][sensorType] + '\n';
                                }
                            }
                            sensorElement.innerHTML = sensorString;
                        } 
                        Allowed_Sensors[deviceId] = deviceData['Sensors'];
                    }
                    Last_State[deviceId] = deviceData['State'];
                    // Update the connect button
                    updateConnectButton(deviceId, deviceData['State']);
                    
                }
            } 
        }
    }  
}

function updateCampaignData(resp){
    if (resp.hasOwnProperty('VERSION') && resp.hasOwnProperty('Interval') ){
        campaingName = document.getElementById('campaignName')
        campaingName.value = resp['Name'] + '_copy'
        createCampaignID()

        interval = document.getElementById('interval')
        interval.value = resp['Interval']
    
        var sensorData = resp['Sensors']
        var deviceData = resp['Devices']
        for (var group of ['data_logger', 'temp_control']){
            groupTable = document.getElementById(group + "-table")
            groupTable.innerHTML = '<thead></thead><tbody></tbody>'
        }
        for (var group in deviceData){
            for (var series in deviceData[group]){
                seriesSensors = {}
                if (sensorData.hasOwnProperty(group)){
                    if (sensorData[group].hasOwnProperty(series)){
                        for (var sensorId in sensorData[group][series]){
                            var sensor = sensorData[group][series][sensorId]
                            var deviceId = sensorId.split('-')[0]
                            var sensorNumber = sensorId.split('-')[1]
                            var sensorType = sensor['SensorType']
                            if (!seriesSensors.hasOwnProperty(deviceId)){
                                seriesSensors[deviceId] = {}
                                seriesSensors[deviceId][sensorType] = [sensorNumber]
                            } else if (!seriesSensors[deviceId].hasOwnProperty(sensorType)) {
                                seriesSensors[deviceId][sensorType] = [sensorNumber]
                            } else {
                                seriesSensors[deviceId][sensorType].push(sensorNumber)
                            }
                        }
                        for (var deviceId in seriesSensors){
                            saveSelectedDeviceId(deviceId, group)
                            addDevice(series, seriesSensors[deviceId]) 
                            Allowed_Sensors[deviceId] = Init_The_Settings[group][series]['ID'][deviceId]['Sensors']
                        }
                    }
                }
                for (var deviceId in deviceData[group][series]){
                    if (!seriesSensors.hasOwnProperty(deviceId)){
                        saveSelectedDeviceId(deviceId, group)
                        addDevice(series, '')
                    }
                }
            }
        } 
        activateButton("connectAllButton")
    } else {
        alert("Cant copy config file which is created in an older version than v0.3")
    }  
}

//--------------------------------------------------------------------
// DEVICE FUNCTIONS
//--------------------------------------------------------------------

function addDevice(series='', sensors='', channel='') {
    var errorMessage = '';
    var selectedDeviceId = document.getElementById('selectedDeviceId').name;
    if (selectedDeviceId == ''){
        errorMessage = ('No device selected')
    }
    else {
        var group = document.getElementById('selectedDeviceId').group;
        var tableId = group + '-table'

        stateElement = document.getElementById(selectedDeviceId + '_State')
        if (series == ''){
            if (idExistsInTable(tableId, selectedDeviceId)){
                errorMessage = ('Device already added')
            } else if (stateElement.innerHTML == 'Offline'){
                errorMessage = ('Device is offline')
            }
        }
        if (errorMessage == '') {
            if (series != ''){
                selectedSeries = series
            } else {
                var seriesSelect = document.getElementById("seriesSelect");
                var selectedSeries = seriesSelect.options[seriesSelect.selectedIndex].text;
            }
            //generalData = Init_The_Settings[group][selectedSeries]['GENERAL']
            idData = Init_The_Settings[group][selectedSeries]['ID'][selectedDeviceId]

            var tableHeader = document.getElementById(tableId).getElementsByTagName('thead')[0];
            if (tableHeader.firstElementChild === null){
                const headers = ['Series', 'Device ID', 'Sensors', 'Channel', '']
                const headerRow = document.createElement("tr");
                headers.forEach((headerText) => {
                    const headerCell = document.createElement("th");
                    headerCell.textContent = headerText;
                    headerRow.append(headerCell);
                });
                tableHeader.appendChild(headerRow)
            }

            var table = document.getElementById(tableId).getElementsByTagName('tbody')[0];
            var newRow = table.insertRow(table.rows.length);
            var header = ['Sensors', 'Channel']

            var newCell = newRow.insertCell(0);
            newCell.className = 'table-series-name'
            newCell.innerHTML = selectedSeries;

            var newCell = newRow.insertCell(1);
            newCell.className = 'table-device-id'
            newCell.innerHTML = selectedDeviceId;

            i = 2
            for (var key of header) {
                var newCell = newRow.insertCell(i);
                var selectedKey = document.getElementById(selectedDeviceId + '_' + key)
                if (selectedKey !== null ){
                    createSubTableContent(key, newCell, selectedDeviceId)
                } else if (sensors != '' && key == 'Sensors'){
                    createSubTableContent(key, newCell, selectedDeviceId, sensors)
                } else if (channel != '' && key == 'Channel'){
                    createSubTableContent(key, newCell, selectedDeviceId)
                } else {
                    var element = document.createElement("div");
                    newCell.appendChild(element);
                }
                i++ 
            }
            
            var deleteButtonCell = newRow.insertCell(i);
            var deleteButton = document.createElement("button");
            deleteButton.innerHTML = "Delete Device";
            deleteButton.className = 'table-delete-button'
            deleteButton.onclick = function() {
                deleteDevice(this, group);
            };
            deleteButtonCell.appendChild(deleteButton);
            closeGroupModal();
        }
    }
    if (errorMessage != ''){
        msgtype = 'warning'
        openAlert(errorMessage, msgtype, "moduleAlertContainer")
    }   
}

function createSubTableContent(key, newCell, selectedDeviceId, sensors=''){
    if (key == 'Sensors'){ 
        var skey = 'SensorTypes'
        var sensorOptions = idData[skey]
        var table = createSensorTable(selectedDeviceId,
                                        sensorOptions,
                                        sensors)
        newCell.appendChild(table)
        var addButton = document.createElement("button");
        addButton.innerHTML = "Add";
        addButton.className = 'table-delete-button'
        addButton.onclick = function() {
            addSensorType(table, sensorOptions);
        };
        newCell.appendChild(addButton)
    } else if (key == 'Channel'){
        newCell.className = 'table-channel-list'
        newCell.innerHTML = Object.keys(idData[key]).join(', ');
    }
}

function deleteDevice(btn, group) {
    var row = btn.parentNode.parentNode;
    row.parentNode.removeChild(row);
    groupTable = document.getElementById(group + "-table")
    if (groupTable.rows[1] === undefined){
        groupTable.innerHTML = '<thead></thead><tbody></tbody>'
    }
}

//--------------------------------------------------------------------
// SUBFUNCTIONS
//--------------------------------------------------------------------

function sumArraysLengthInDictionary(dictionary) {
    var result = 0;

    // Iterate over each key-value pair in the dictionary
    for (const key in dictionary) {
        if (dictionary.hasOwnProperty(key)) {
            const array = dictionary[key];

            // Check if the value is an array
            if (Array.isArray(array)) {
                // Initialize the sum for the current key
                result += array.length;
            } 
        }
    }

    return result;
}

function convertToList(inputText) {
    const list = [];
    const items = inputText.split(','); // Split input text by commas
    
    items.forEach(item => {
        if (item.includes('-')) { // Check if the item contains a range
            list.push(...convertRangeToList(item));
        } else {
            list.push(item.trim()); // Add individual item to the list
        }
    });

    return list;
}


function hasDuplicate(list) {
    const countMap = new Map();
    for (const item of list) {
        if (countMap.has(item)) {
            // If item already exists in the map, increment its count
            countMap.set(item, countMap.get(item) + 1);
        } else {
            // Otherwise, initialize its count to 1
            countMap.set(item, 1);
        }
    }

    // Check if any item has a count greater than 1
    for (const count of countMap.values()) {
        if (count > 1) {
            return true; // If any item appears twice or more, return true
        }
    }
    return false; // If no item appears twice or more, return false
}


function convertRangeToList(input) {
    // Extract the first character and the last character (if applicable)
    const list = [];
    const firstChar = input.split('-')[0].trim();
    const lastChar = input.split('-')[1].trim();
    // If the input ends with a digit
    if (!isNaN(parseInt(lastChar)) && !isNaN(parseInt(firstChar))) {
        const startIndex = parseInt(firstChar);
        const endIndex = parseInt(lastChar);
        for (let i = startIndex; i <= endIndex; i++) {
            list.push(i.toString());
        }
        return list;
    } else if (firstChar[0].toUpperCase() === lastChar[0].toUpperCase()) { // If the input has different starting and ending characters
        // If the input contains a single character
        const startCharCode = firstChar.substring(1);
        const endCharCode = lastChar.substring(1);
        
        for (let i = startCharCode; i <= endCharCode; i++) {
            list.push(firstChar[0] + i);
        }
    } else {
        // If the input contains a single character
        const startCharCode = firstChar.charCodeAt(0);
        const endCharCode = lastChar.charCodeAt(0);
        
        for (let i = startCharCode; i <= endCharCode; i++) {
            list.push(String.fromCharCode(i));
        }
    }

    return list;
}

//--------------------------------------------------------------------
// MODAL FUNCTIONS
//--------------------------------------------------------------------

function resetModal(group){
    var selectedDevice = document.getElementById('selectedDeviceId')
    selectedDevice.name = ''
    selectedDevice.group = group
    document.getElementById("seriesSelect").innerHTML = "";
    document.getElementById("selectableTable").innerHTML = "";
}

//------------------------------ DEVICE --------------------------------------

function openGroupModal(group) {
    resetModal(group)
    document.getElementById("deviceModal").style.display = "block";
    if (document.getElementById("seriesSelect").options.length == 0){
        createOptionsLogger(group)
        showSeriesInfo()
    }
    else {
        closeGroupModal()
    }
}

// Function to close the modal
function closeGroupModal() {
    document.getElementById("deviceModal").style.display = "none";
}

// Function to show device information
function showSeriesInfo() {
    document.getElementById("selectableTable").innerHTML = "";
    const group = document.getElementById('selectedDeviceId').group;
    const seriesInfo = Init_The_Settings[group];
    const seriesSelect = document.getElementById("seriesSelect");
    const selectedDevice = seriesSelect.options[seriesSelect.selectedIndex].value;
    const deviceInfos = {};
    let header = ['Connection', 'State', 'LastConnection'];
    
    for (const deviceId in seriesInfo[selectedDevice]['ID']) {
        const device = seriesInfo[selectedDevice]['ID'][deviceId];
        if ('SensorTypes' in device && !header.includes('SensorTypes')) {
            header.push('SensorTypes', 'Sensors')
        }
        if ('Channel' in device && !header.includes('Channel')) {
            header.push('Channel')
        }
        deviceInfos[deviceId] = { 'ID': deviceId };

        for (const key of Object.keys(device)) {
            if (header.includes(key)) {
                if (key === 'SensorTypes') {
                    deviceInfos[deviceId][key] = Object.keys(device[key]).join(', ');
                } else if (key === 'Sensors') {
                    Allowed_Sensors[deviceId] = device[key];
                    let sensorString = "";
                    for (const sensorType in device[key]) {
                        if (device[key] === null) {
                            sensorString += sensorType + '\n';
                        } else {
                            sensorString += sensorType + ": " + device[key][sensorType] + '\n';
                        }
                    }
                    deviceInfos[deviceId][key] = sensorString;
                } else if (key === 'Channel') {
                    deviceInfos[deviceId][key] = Object.keys(device[key]).join(', ');
                } else if (key === 'Connection'){
                    deviceInfos[deviceId][key] = device[key]['Type'];
                } else {
                    deviceInfos[deviceId][key] = device[key];
                }
            }
        }
    }
    header = ['ID'].concat(header);
    const generatedTable = generateHTMLTable(deviceInfos, group, header, selectedDevice);
    document.getElementById("selectableTable").appendChild(generatedTable);
}


function createOptionsLogger(group) {
    var seriesInfo = Init_The_Settings[group]
    var seriesSelect = document.getElementById("seriesSelect");

    for (const device in seriesInfo){
        var option = document.createElement("option");
        option.text = device;
        option.value = device;
        seriesSelect.appendChild(option);
    }
    
}

function saveSelectedDeviceId(key, group){
    var selectedDeviceId = document.getElementById('selectedDeviceId');
    selectedDeviceId.name = key
    selectedDeviceId.group = group
}

function generateHTMLTable(data, group, header, selectedDevice) {
    const table = document.createElement('table');
    table.id = group + '-table'
    const headerRow = table.insertRow();
    
    const selectHeader = document.createElement('th');
    selectHeader.textContent = 'Select';
    headerRow.appendChild(selectHeader);

    for (const key of header) {
        const th = document.createElement('th');
        th.textContent = key;
        headerRow.appendChild(th);
    }

    const actionHeader = document.createElement('th');
    headerRow.appendChild(actionHeader);

    for (const key in data) {
        const row = table.insertRow();
        row.id = `row_${key}`;
        const selectCell = row.insertCell();
        const radioInput = document.createElement('input');
        radioInput.type = 'radio';
        radioInput.name = 'selectedRow';
        radioInput.id = `device_${key}`;
        radioInput.onchange = function() {
            if (this.checked) {
                saveSelectedDeviceId(key, group);
            }
        };
        selectCell.appendChild(radioInput);

        for (const subKey of header) {
            if (typeof data[key][subKey] === 'string') {
                const cell = row.insertCell();
                const idName = `${key}_${subKey}`;
                const value = data[key][subKey];
                cell.id = idName;
                cell.textContent = value;
            }
        }

        const actionCell = row.insertCell();
        const button = document.createElement('button');
        const buttonName = data[key]['State'] == 'Offline' ? 'Connect' : 'Disconnect';
        const idNameButton = `${key}_connect`;
        button.id = idNameButton;
        button.textContent = buttonName;
        button.onclick = function() {
            connectDevice(key, selectedDevice);
        };
        actionCell.appendChild(button);
    }

    return table;
}


function updateConnectButton(deviceId, state){
    connectButton = document.getElementById(`${deviceId}_connect`);
    if (connectButton !== null){
        connectButton.innerHTML = state === "Offline" ? "Connect" : "Disconnect"
    }
}


//--------------------------------------------------------------------
// CAMPAIGN
//--------------------------------------------------------------------

function createCampaignID() {
    // Lowercase the campaign name 
    var input = document.getElementById("campaignName").value.toLowerCase();
    var output = document.getElementById("campaignId");
    // Define a regular expression to match special characters and spaces
    var regex = /[^a-zA-Z0-9]/g;
    // Replace special characters and spaces with underscores
    output.textContent = input.replace(regex, '_');
}


function copyCampaign(){  
    selectedCampaign = changeCopyCampaign()
    if (selectedCampaign != ''){
        recvCampaignData(selectedCampaign)   
    }
}

function changeCopyCampaign(){
    var campaignSelect = document.getElementById("campaignSelect");
    index = campaignSelect.selectedIndex
    if (index != -1){
        var selectedCampaign = campaignSelect.options[index].value;
        const copyCampaignCheckbox = document.getElementById("copyCampaignCheckbox")
        var copyCampaign = document.getElementById("copyCampaign")
        if (copyCampaignCheckbox.checked){
            copyCampaign.value = selectedCampaign
        } else {
            copyCampaign.value = ""
        }
        return selectedCampaign
    } else {
        return ''
    }
}


//--------------------------- CREATE SENSOR TABLE IN SERIES --------------------------------------

function createSensorTable(deviceId, sensorOptions, sensors=''){
    var table = document.createElement('table')
    table.id = deviceId + '-sensorTable'
    table.className = 'series-sensor-table'
    if (sensors == ''){
        addSensorType(table, sensorOptions)
    }
    else{
        for (const sensorType in sensors){
            addSensorType(table, sensorOptions, sensorType, sensors[sensorType])
        }
    }
    return table
}

function addSensorType(table, sensorOptions, sensType, value=''){
    newRow = table.insertRow(table.rows.length)
    var newCell = newRow.insertCell(0);
    newCell.appendChild(createSensorTypeCell(sensorOptions, sensType))
    var newCell = newRow.insertCell(1);
    newCell.appendChild(createSensorCell(value))

    var deleteButtonCell = newRow.insertCell(2);
    var deleteButton = document.createElement("button");
    deleteButton.innerHTML = "Delete";
    deleteButton.className = 'table-delete-button'
    deleteButton.onclick = function() {
        deleteSensType(this);
    };
    deleteButtonCell.appendChild(deleteButton)
}


function deleteSensType(btn) {
    var row = btn.parentNode.parentNode;
    row.parentNode.removeChild(row);
}

function getIndexOfSensorType(sensType, sensorTypeList, j=0){
    const mainType = sensType.split('-')[j]
    for (var i = 0; i<sensorTypeList.length; i++){
        if (mainType == sensorTypeList[i]){
            return i
        }
    }
}

function createSensorTypeCell(dictionary, sensType='') {
    const sensorTypeTable = document.createElement('table');
    const newRow = sensorTypeTable.insertRow();
    const newCell = newRow.insertCell(0);
    const tableBody = document.createElement('table');
    tableBody.className = "sensor-selection";

    const typeSelect = document.createElement("select");
    typeSelect.onchange = () => createSensorTypeTableBody(typeSelect, tableBody, dictionary);
    
    for (const key in dictionary) {
        const option = document.createElement("option");
        option.value = key;
        option.text = key;
        typeSelect.appendChild(option);
    }
    index = getIndexOfSensorType(sensType, Object.keys(dictionary))
    typeSelect.selectedIndex = index 
    newCell.appendChild(typeSelect);
    createSensorTypeTableBody(typeSelect, tableBody, dictionary, sensType);

    newRow.insertCell(1).appendChild(tableBody);

    return sensorTypeTable;
}


function createSensorTypeTableBody(selector, tableBody, dictionary, sensType=''){
    var sensorType = selector.value
    tableBody.innerHTML = ''
    var j = 1
    for (const key in dictionary[sensorType]){
        var newRow = tableBody.insertRow()
        var newCell1 = newRow.insertCell(0);
        newCell1.innerHTML = key
        var newCell2 = newRow.insertCell(1);
        const select = document.createElement('select');
        dictionary[sensorType][key].forEach(val => {
            const option = document.createElement('option');
            option.text = val;
            select.appendChild(option);
        });
        index = getIndexOfSensorType(sensType, dictionary[sensorType][key],j)
        select.selectedIndex = index 
        newCell2.appendChild(select)
        j++
    }
}

function createSensorCell(value=''){
    sensorInput = document.createElement('input')
    if (value != ''){
        sensorInput.value = value.toString();
    }
    return sensorInput
}

//--------------------------- READOUT TABLE DATA --------------------------------------


function getTableData(tableId) {
    const table = document.getElementById(tableId);
    const headers = Array.from(table.rows[0].cells).map(cell => cell.textContent.trim());
    const data = [];
    
    for (let j = 1; j < table.rows.length; j++) {
        const row = table.rows[j];
        const rowData = {};
        
        for (let k = 0; k < row.cells.length; k++) {
            const key = headers[k];
            let value;
            
            if (key === 'Sensors') {
                if (row.cells[k].querySelector('select')) {
                    const deviceId = row.cells[k - 1].textContent.trim();
                    value = getSubSensorTableData(deviceId + "-sensorTable")
                } else {
                    value = '';
                }
            } else if (row.cells[k].querySelector('input')) {
                value = row.cells[k].querySelector('input').value;
            } else if (row.cells[k].querySelector('select')) {
                value = row.cells[k].querySelector('select').value;
            } else {
                value = row.cells[k].textContent.trim();
            }
            
            rowData[key] = value;
        }
        
        data.push(rowData);
    }
    return data;
}

function getSubSensorTableData(tableId) {
    const headers = ['SensorType', 'Sensors'];
    const table = document.getElementById(tableId);
    const data = Array.from(table.rows).map(row => {
        const rowData = {};
        Array.from(row.cells).forEach((cell, k) => {
            const key = headers[k];
            let value;
            if (cell.querySelector('input')) {
                value = cell.querySelector('input').value;
            } else if (headers[k] === 'SensorType') {
                value = cell.querySelector('select').value;
                value += getTableData2RowString(cell.firstElementChild.firstChild.firstChild.lastChild.firstChild);
            } else if (cell.querySelector('select')) {
                value = cell.querySelector('select').value;
            } else {
                value = cell.textContent.trim();
            }
            rowData[key] = value;
        });
        return rowData;
    });
    return data;
}

function getTableData2RowString(table){
    var data = '';

    for (var j = 0; j < table.rows.length; j++) {
        var row = table.rows[j];
        var value
        if (row.cells[1].querySelector('input')) {
            value = row.cells[1].querySelector('input').value;
        } 
        else if (row.cells[1].querySelector('select')){
            value = row.cells[1].querySelector('select').value;
        } else {
            value = row.cells[1].textContent.trim();
        }
        data += '-' + value
    }
    return data
}

