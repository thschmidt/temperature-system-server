
//--------------------------------------------------------------------
// GLOBAL VARIABLE
//--------------------------------------------------------------------

var CurrentCampaign = '' // Name of current campaign
var SensorDetails = {} // Styles/Infos of each individual sensor of current campaign
var Groups = {} // Sensor groups of current campaign
var CurrentData = {} // Full dataset of current campaign
var FirstTimestamp = 0 // First timestamp of current dataset in UTC

var ShownRange = 600 // Current shown data range in seconds
var Interval = 0 // Readout rate of current campaign

// Factor of data reduction 
var TimeStep = 1 // Example: TimeStep == 2, show every 2nd datapoint

var ZoomXLimits = [] // Current shown x-range


//--------------------------------------------------------------------
// PLOT FUNCTIONS
//--------------------------------------------------------------------

function initGraph(){
    /* Initialize the graph */
    
    // Reset factors of data reduction (active campaign)
    TimeStep = 1

    // Get div of graph and get default temperature unit
    var graphDiv = document.getElementById('graph');
    const defaultUnit = getTemperatureUnit();

    // Convert current data
    var showPoints = Math.floor(ShownRange/Interval) + 1;
    var newData = getPartialCurrentData(CurrentData, showPoints);
    // Extract xData
    const xData = newData['Timestamp'];

    // Define Layout of graph
    var layout = {
        autosize: true,
        xaxis: {
            title: 'Time [Local]'
        },
        yaxis: {
            title: 'Temperature'
        },
        margin: { l: 60, r: 30, t: 0, b: 50 }, 
        showlegend: true,
		legend: {"orientation": "h", 
                xanchor: "center",
                x: 0.5,
                y: 20},
        modebar: {
            orientation: 'v'
        }
    };
    
    // Define configuration of graph (remove buttons and logo)
    var config = {
        modeBarButtonsToRemove: ['select2d','lasso2d'],
        modeBarButtonsToAdd:['v1hovermode','togglespikelines'],
        displaylogo: false,
        displayModeBar: true
    };

    // Create dataset traces
    var traces = [];
    for (var y_var in newData){
        if (y_var != 'Timestamp'){
            traces.push({
                x: xData,
                y: newData[y_var].map(temperature => convertTemperature(temperature, defaultUnit)), 
                mode: 'lines',
                name: SensorDetails[y_var]['Name'],
                line: {
                    color: SensorDetails[y_var]['Style']['Colour'], // Change the color of the line
                    dash: SensorDetails[y_var]['Style']['Line'] // Change the line type (e.g., 'solid', 'dash', 'dot', 'dashdot')
                }
            });
        }
    }
    // Create new Plotly plot with event listeners
    Plotly.newPlot(graphDiv, traces, layout, config).then( function (){
        graphDiv.on('plotly_relayout', function(eventData) {
            if ('dragmode' in eventData){
            }
            else {
                zoomUpdate(eventData);
            }
        });
        
    })
    // Initial resize of the graph
    resizeGraph();
    // Show the last timestamp of the dataset
    document.getElementById('lastUpdate').innerHTML = convertDatetime(xData[xData.length-1]);
}
  
// UPDATE PLOT

function updatePlot(newData, getData=false){
    /* Refresh the existing plot with new data and provide its updated dataset if necessary */
    var defaultUnit = getTemperatureUnit();
    var graphDiv = document.getElementById('graph');
    // Create dataset traces
    var traces = []
    for (var y_var in newData){
        if (y_var != 'Timestamp'){
            traces.push(
                newData[y_var].map(temperature => convertTemperature(temperature, defaultUnit)), 
            )
        }
    }
    var updatedData = {x: [newData['Timestamp']],
                        y: traces}
    // Return dataset if requested
    if (getData){
        return updatedData
    }
    // Update the plot by restyling 
    Plotly.restyle(graphDiv, updatedData);
}


function addGraphData(data) {
    /* Add extend graph with new data and check for data reduction */
    var graphDiv = document.getElementById('graph');
    var defaultUnit = getTemperatureUnit()
    
    // Get last and first timestamp of CurrentData object
    var sens_data = graphDiv.data
    var last_timestamp = CurrentData['Timestamp'][CurrentData['Timestamp'].length-1]
    var first_timestamp = CurrentData['Timestamp'][0]
    var last_plot_timestamp = sens_data[0]['x'][sens_data[0]['x'].length-1]

    for (var point in data){
        var timestamp = convertToUtcTimestamp(data[point].Timestamp)
        // Check if the data of the 10 last datasets has been cached
        if (+timestamp > +last_timestamp){
            // Add data
            addCurrentData(data[point], last_timestamp)

            if (CurrentData['Timestamp'].length > 2){
                timestamp_diff = last_timestamp - first_timestamp
                if (timestamp_diff >= ShownRange*1000){
                    var showPoints = Math.floor(ShownRange/Interval/TimeStep) + 1
                } else {
                    var showPoints = Math.floor((timestamp_diff/1000)/Interval/TimeStep) + 2
                }
                // Extend data of graph only if difference between new timestamp and last timestamp in graph is
                // equal to Timestep (factor) of the Readout rate (Interval)
                var diff = Interval*TimeStep*1000 == (timestamp - last_plot_timestamp)
            } else {
                // first three datapoints are added what ever is set
                var diff = true
                var showPoints = 3
            }
            
            if (diff && !ZoomXLimits.length){
                // Init added 
                var xValues = []
                var yValues = []
                var traceList = []
                // Loop over all sensors and add data
                for (let i=0; i<sens_data.length; i++){
                    var ydata= [data[point]['Data'][Object.keys(SensorDetails)[i]]]
                    yValues.push(ydata.map(temperature => convertTemperature(temperature, defaultUnit)))
                    traceList.push(i)
                    xValues.push([timestamp])
                }
                // Extend traces of plotly graph
                Plotly.extendTraces(graphDiv, {x: xValues, y: yValues}, traceList, showPoints);
            } 

            //
            if (showPoints >= 1000){
                if (!ZoomXLimits.length){
                    var newData = getPartialCurrentData(CurrentData, showPoints, TimeStep)
                    updatePlot(newData)
                }
            }

            //
            document.getElementById('lastUpdate').innerHTML = convertDatetime(timestamp)
        }
    }

}



// ZOOM FUNCTIONS

function resetAxis() {
    /* Function to reset the axis */
    var graphDiv = document.getElementById('graph');
    Plotly.relayout(graphDiv , {
        'xaxis.autorange': true,
        'yaxis.autorange': true
    });
}

function zoomUpdate(eventdata){
    /* Update the graph on zoom / or reset zoom */
    var xaxis = [eventdata['xaxis.range[0]'], eventdata['xaxis.range[1]']]

    var showPoints = Math.floor(ShownRange/Interval) + 1
    if (xaxis[0] == undefined && xaxis[1] == undefined){   
        // Reset zoom or panning
        var newData = getPartialCurrentData(CurrentData, showPoints)
        ZoomXLimits = []
    }
    else {
        // Get ranges of current zoom in x (Timestamps)
        var startInSec = convertTimestampString(xaxis[0])
        var endInSec = convertTimestampString(xaxis[1])

        // 
        var data = getPartialCurrentData(CurrentData, showPoints, n=1)

        var xRange = getEdges(startInSec, endInSec, data)

        // Add additional data outside the zoom range to facilitate panning functionality
        var newData = getElementsAroundRegion(data, xRange[0], xRange[1]-xRange[0])
        ZoomXLimits = [startInSec, endInSec]
    }
    // Refresh graph with updated data
    updatePlot(newData)
}


// GROUP FUNCTIONS

function createGroups(data){
    /* Initial creation of the group checkboxes */
    removeCheckBoxes()
    Groups = {}
    for (did in data){
        groupName = data[did]['Group'];
        if (!(data[did]['Group'] in Groups)){
            Groups[groupName] = []
            addCheckbox(groupName);
        }
        Groups[groupName].push(did);
    }
}

function addCheckbox(checkboxName, checked=true) {
    /* Create a checkbox with checkbox_name in the group container */
    const container = document.getElementById('groups');

    if (checkboxName) {
        const wrapper = document.createElement('div');
        wrapper.classList.add('checkboxWrapper'); // Add a class for styling
        const checkbox = document.createElement('input');
        checkbox.type = 'checkbox';
        checkbox.name = checkboxName;
        checkbox.id = checkboxName + '_checkbox';
        checkbox.addEventListener('change', handleCheckboxChange);
        checkbox.checked = checked
        
        const label = document.createElement('label');
        label.appendChild(document.createTextNode(checkboxName));

        wrapper.appendChild(checkbox);
        wrapper.appendChild(label);

        container.appendChild(wrapper);
    }
}

function removeCheckBoxes() {
    /* Remove existin gcheckboxes in group */
    const container = document.getElementById('groups');
    container.innerHTML = "Groups: "
}

function handleCheckboxChange(event) {
    /* De- / activate checkboxes in group selection */
    toggleTraces(event.target.id, Groups[event.target.name])
}

function toggleTraces(checkboxid, sensors) {
    /* Toggle sensors depending on the selected groups */
    const checkbox = document.getElementById(checkboxid);
    var graphDiv = document.getElementById('graph');
    var sens_data = graphDiv.data
    
    // Loop over all sensors and update their visbility variable for selected sensors
    var changedSensors = []
    for (let i=0; i<sens_data.length; i++){
        if (sensors.includes(Object.keys(SensorDetails)[i])){
            changedSensors.push(i)
        }   
    } 
    // Restyle the visibility of the selected sensors
    Plotly.restyle(graphDiv, { visible: checkbox.checked }, changedSensors);
}

//--------------------------------------------------------------------
// RECEIVE
//--------------------------------------------------------------------

function initData(status_data){
    /* Initialize campaigns with the received status_data */
    createOptionsCampaign(status_data)
    CurrentCampaign = status_data['campaign']['CurrentCampaign']
}

function updateData(status_data){
    CurrentCampaign = status_data['campaign']['CurrentCampaign']
}

function updateCampaignData(response){
    /* Update the campaign data of the current selected campaign */
    FirstTimestamp = convertToUtcTimestamp(response['Start'].substring(0, response['Start'].length-4));
    var convertedSensorData  = convertSensorData(response['Sensors'])
    SensorDetails = convertedSensorData
    if (response.hasOwnProperty('Interval')){
        Interval = response['Interval']
    } else {
        // For older versions than v0.3
        Interval = response['ReadoutRate']
    }
    
    //if (jQuery.isEmptyObject(Groups)){
    createGroups(convertedSensorData)
    //}
}

function updateGraphData(response, period, reload, firstDate){
    /* Update or init the graph data if it is fetched from the server */
    if (!jQuery.isEmptyObject(response)){
        if (period != 0){
            try{
                if (firstDate != 0){
                    // combine cached data and older datasets
                    combineData(response);
                } else {
                    // reset the Current data with new datasets
                    convertData(response);
                }
                // Reset Modal
                toogleLoadingModal('none');
                if (reload){
                    // Update graph but do not reset zoom
                    changeRange();
                } else {
                    // Initialize graph
                    initGraph();
                }
            }
            catch {
                // RETRY
                selectCampaign()
            }
        }
        else {
            try{
                // Add new graph data
                addGraphData(response['Sensors'])
            }
            catch {}
        }
    } 
}

//--------------------------------------------------------------------
// CAMPAIGN
//--------------------------------------------------------------------

function selectCampaign(reload=false){
    /* Update graph data depending on campaign selection */
    var campaignSelect = document.getElementById("campaignSelect");
    if (campaignSelect.selectedIndex != -1){
        var selected_campaign = campaignSelect.options[campaignSelect.selectedIndex].value;

        if (reload == false){
            // Reset Current Data and selected range
            CurrentData = {};
            document.getElementById("showRange").value = 600;
            var timeUnit = document.getElementById("timeUnit");
            timeUnit.value = "second(s)";
            ShownRange = 600;
            // Create graph and group boxes
            recvCampaignData(selected_campaign);
            recvGraphData(selected_campaign, ShownRange); 
        } else {
            recvGraphData(selected_campaign, ShownRange, true); 
        }
        toogleLoadingModal();
    }
}

function updateTheGraph(load=false){
    /* Update graph data with new or old datasets of selected campaign */
    // Does not update during the loading of data (executed by user)
    if (!jQuery.isEmptyObject(CurrentData) && document.getElementById("loadingModal").style.display == 'none'){
        var campaignSelect = document.getElementById("campaignSelect");
        var selected_campaign = campaignSelect.options[campaignSelect.selectedIndex].value;

        // Get first timestamp of graph
        var graphDiv = document.getElementById('graph');
        var sensData = graphDiv.data
        var firstGraphTimestamp = sensData[0]['x'][0]

        //recvCampaignData(selected_campaign);
        if (load){
            // Load additional older data if available
            if (FirstTimestamp < CurrentData['Timestamp'][0]){
                firstDate = convertDatetime(CurrentData['Timestamp'][0]).split(' ')[0]
                recvGraphData(selected_campaign, ShownRange, true, firstDate); 
                toogleLoadingModal();
            } else if (firstGraphTimestamp >= CurrentData['Timestamp'][0]){
                changeRange(true)
            }
        } else if (selected_campaign == CurrentCampaign){
            // New graph data is available (only for running campaigns)
            recvGraphData(selected_campaign);
        } 
    }
}

//--------------------------------------------------------------------
// DATA FUNCTIONS
//--------------------------------------------------------------------

function addCurrentData(data){
    /* Add new data to the data variable of current selected campaign (convert timestamp data) */
    CurrentData['Timestamp'].push(convertToUtcTimestamp(data['Timestamp']))
    for (key in data.Data){
        CurrentData[key].push(data.Data[key])
    } 
}

function convertSensorData(sensorData){
    /* Convert fetched sensor data and return them */
    var sensorDict = {}
    for (let group in sensorData){
        for (series in sensorData[group]){
            for (sensorName in sensorData[group][series]){
                sensorConfig = sensorData[group][series][sensorName]
                sensorDict[sensorName] = sensorConfig
            }
        }
    }
    return sensorDict
}

function convertData(data){
    /* Convert string timestamp into datetime like timestamp */
    CurrentData = data
    CurrentData['Timestamp'] = convertTimestampData(data['Timestamp'])
}

function combineData(data){
    /* Combine current data and additional loaded data */
    for (let key in data){
        if (key == 'Timestamp'){
            data[key] = convertTimestampData(data[key])
        }
        CurrentData[key] = data[key].concat(CurrentData[key])
    }
}

function getEdges(startInSec, endInSec, data){
    /* Retrieve the edges of the selected zoom,
     incorporating an additional range that mirrors the shown range,
     both to the left and right of the selected region*/
    var diffInSec = endInSec - startInSec

    // Add the additional range to the new edges
    var newStart = (startInSec - diffInSec)
    var newEnd = (endInSec + diffInSec)

    // Get the next timestamp values closest to these edges from the data
    var list = data['Timestamp']
    var x0 = findNextValue(list, newStart, direction = 'bigger')
    var x1 = findNextValue(list, newEnd, direction = 'smaller')
    
    // x0 lower edge, x1 upper edge
    return [x0, x1]
}

function getPartialCurrentData(obj, regionIndex, n=0){
    /* Limit the displayed data on the graph if it exceeds a certain value. */
    result = {}
    for (let key in obj) {
        if (obj.hasOwnProperty(key) && Array.isArray(obj[key]) && obj[key].length > 0) {
            // Get the partial element around the specified region
            let startIndex = Math.max(0, obj[key].length-regionIndex);
            let endIndex = obj[key].length
            // Only reduce data if n != 1
            if (n == 1){
                var partialElement = obj[key].slice(startIndex, endIndex);
            }
            else {
                var partialElement = singleDataReduction(obj[key].slice(startIndex, endIndex), n);
            }
            // Store the elements in the result object
            result[key] = partialElement
        }
    }
    return result
}

function singleDataReduction(data, n=0, divider=1000){
    /* Reduce dataset if it exceds divider by the factor n */

    // Calculate n if defined as 0
    if (n == 0){
        n = Math.ceil(data.length/divider)
    }
    if (n > 1){
        var newData = showEveryNthElement(data, n)
        return newData
    }
    return data
}

//--------------------------------------------------------------------
// USER INPUT
//--------------------------------------------------------------------

function changeRange(redo=false){
    /* Change shown range depending on user input */
    var newRange = document.getElementById('showRange').value
    if (isInteger(newRange)){
        newRangeInSeconds = convertShowTime(newRange)
        ShownRange = newRangeInSeconds
        // Check if data available if graph can be shown only with the data available in the cache.
        // Load data from server if not
        var lastTimestamp = CurrentData['Timestamp'][CurrentData['Timestamp'].length-1]
        var firstTimestamp = CurrentData['Timestamp'][0]
        var newFirstTimestamp = new Date(lastTimestamp.getTime() - ShownRange*1000)
        
        if (newFirstTimestamp < firstTimestamp && redo == false){
            updateTheGraph(true)
        } else {
            var showPoints = Math.floor(ShownRange/Interval) + 1
            var newData = getPartialCurrentData(CurrentData, showPoints)
            updatePlot(newData)
            resetAxis()
            if (showPoints >= 1000){
                if (CurrentData['Timestamp'].length > showPoints){
                    TimeStep = Math.ceil(showPoints/1000)
                } else {
                    TimeStep = Math.ceil(CurrentData['Timestamp'].length/1000)
                }
            } else {
                TimeStep = 1
            }     
        }
    }
}

function getTemperatureUnit(){
    /* Get current selected temperature unit */
    var tempUnitSelect = document.getElementById("tempUnit")
    var tempUnit = tempUnitSelect.options[tempUnitSelect.selectedIndex].value
    return tempUnit
}

function resizeGraph() {
    /* Create a function to resize the graph */
    var graphDiv = document.getElementById('graph');
    var windowHeight = window.innerHeight;
    var windowWidth = window.innerWidth;
    var header = document.getElementById("header")
    var graphHeight = header.style.display === "none" ? windowHeight-90 : windowHeight-195
    var graphWidth = windowWidth-35;

    // Update the graph height
    Plotly.relayout(graphDiv, {
        height: graphHeight,
        width: graphWidth
    });
}

function toggleHeaders() {
    /* Display or hide website header to maximize size of graph */
    var header = document.getElementById("header")
    header.style.display = header.style.display === "none" ? "block" : "none"
    var content = document.getElementById("graphHeader")
    content.style.display = content.style.display === "none" ? "block" : "none"
    resizeGraph() 
}

function reload(){
    selectCampaign(true)
}

//--------------------------------------------------------------------
// SUB FUNCTIONS
//--------------------------------------------------------------------

function findNextValue(list, inputValue, direction = 'smaller') {
    /* Provide the nearest value from a list to the input value based on the specified direction */
    let nextValue = null;
    let nextValueIndex = -1;

    // Iterate through the list
    for (let i = 0; i < list.length; i++) {
        // If direction is "smaller" and the current value is smaller than the input value and larger than the current nextValue (if any), update nextValue
        if (direction === "smaller" && list[i] < inputValue && (nextValue === null || list[i] > nextValue)) {
            nextValue = list[i];
            nextValueIndex = i;
        }
        // If direction is "bigger" and the current value is bigger than the input value and smaller than the current nextValue (if any), update nextValue
        else if (direction === "bigger" && list[i] > inputValue && (nextValue === null || list[i] < nextValue)) {
            nextValue = list[i];
            nextValueIndex = i;
        }
    }
    return nextValueIndex;
}

function getElementsAroundRegion(obj, regionIndex, regionSize) {
    let result = {};

    // Iterate over the object
    for (let key in obj) {
        if (obj.hasOwnProperty(key) && Array.isArray(obj[key]) && obj[key].length > 0) {
            // Get the first element
            let firstElement = obj[key][0];

            // Get the partial element around the specified region
            let startIndex = Math.max(0, regionIndex);
            let endIndex = Math.min(obj[key].length - 1, startIndex + regionSize - 1);
            var divider = (1000 + Math.ceil(safeDivision(regionIndex, regionSize)*500)
                            + Math.ceil(safeDivision(obj[key].length - endIndex, regionSize)*500))

            let partialElement = singleDataReduction(obj[key].slice(startIndex, endIndex + 1),
                                                    n=0,
                                                    divider=divider);
            // Get the last element
            let lastElement = obj[key][obj[key].length - 1];

            // Store the elements in the result object
            var array = [firstElement].concat(partialElement, [lastElement])
            result[key] = array

        }
    }

    return result;
}

function safeDivision(dividend, divisor) {
    /* Perform the division */
    var result = dividend / divisor;

    // Check if the result is greater than 1
    if (result > 1) {
        return 1;
    } else {
        return result;
    }
}

function showEveryNthElement(list, n) {
    /* Get every "n"-th element of a list  */
    var result = [];
    for (var i = 0; i < list.length; i += n) {
        result.push(list[i]);
    }
    return result;
}

function convertShowTime(inputTime) {
    /* Get the selected unit and input value */
    var timeUnit = document.getElementById("timeUnit")
    var unit = timeUnit.options[timeUnit.selectedIndex].value
    // Perform the multiplication based on the selected unit
    return convertIntoSeconds(inputTime, unit)
}


// CONVERT TIMESTAMPS

function convertTimestampString(timestampString){
    var timestamp = new Date(timestampString);

    // Get the timestamp in milliseconds
    var timestampInMilliseconds = timestamp.getTime();

    // Convert milliseconds to seconds
    return timestampInMilliseconds;
}

function convertDatetime(date){
    /* Extract the date and time components */
    var year = date.getFullYear();
    var month = (date.getMonth() + 1).toString().padStart(2, '0'); // Months are zero-based
    var day = date.getDate().toString().padStart(2, '0');
    var hours = date.getHours().toString().padStart(2, '0');
    var minutes = date.getMinutes().toString().padStart(2, '0');
    var seconds = date.getSeconds().toString().padStart(2, '0');
    var timezone = date.getTimezoneOffset()

    // Construct the timestamp string
    return `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`;
}

function convertToUtcTimestamp(timestampString){
    var timestamp = new Date(Date.UTC(
        parseInt(timestampString.substr(0, 4)), // Year
        parseInt(timestampString.substr(5, 2)) - 1, // Month (zero-based)
        parseInt(timestampString.substr(8, 2)), // Day
        parseInt(timestampString.substr(11, 2)), // Hours
        parseInt(timestampString.substr(14, 2)), // Minutes
        parseInt(timestampString.substr(17, 2)) // Seconds
    ));
    return timestamp
}

function convertTimestampData(timestampDataStrings){
    var timestampData = []
    for (var timestamp of timestampDataStrings){
        timestampData.push(convertToUtcTimestamp(timestamp))
    }
    return timestampData
}


// MODAL
function toogleLoadingModal(display="block") {
    /* Toggle the update modal */
    document.getElementById("loadingModal").style.display = display;
}





//--------------------------------------------------------------------
// UPDATE AND INIT DATA
//--------------------------------------------------------------------


Initialization_Promise.then(() => {
    // Fetch initial data
    setTimeout(selectCampaign, 100);

    // Set default Temperature Unit
    var select = document.getElementById('tempUnit');
    setDefaultTemperatureUnit(select)

    // Set interval for fetching data periodically
    setInterval(updateTheGraph, 500); // Fetch data half a second


    // Add an event listener for window resize
    window.addEventListener('resize', resizeGraph);
});





