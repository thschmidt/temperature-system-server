# -*- coding: utf-8 -*-

###############################################################################
# IMPORTS
###############################################################################

import os

# Time and clock related
from datetime import datetime as dt

# Webpage
import cherrypy

# Thread and processes
import threading as th
import multiprocessing as mp

# Sub Functions
import version_update as vru 

# Scriptlib
import logging_functions as lof

###############################################################################
# CONSTANTS
############################################################################### 

ADMIN_SERVICES = ['CAT', 'WebSrv', 'TempSrv', 'TaskEv']
ADMIN_CMDS =['SET', 'START', 'STOP', 'INIT', 'REQUEST', 'CONNECT']
USER_CMDS = ['STATUS', 'RESPONSE', 'EVENT']
CMDTYPES = ADMIN_CMDS + USER_CMDS

RECEIVER = 'TempSrv'

###############################################################################
# CLASSES
###############################################################################

class ServiceParameter:
    def __init__(self, service, settings):
        self.service = service
        self.settings = settings
        
        self.timestamp_logs= {}
        self.timestamp_logging = settings['config']['DEBUG']['timestamp_log']
     
    def log_timestamps(self):
        """
        Logs the timestamps within the program for time tests and debbuging
        """
        if self.timestamp_logging:
            folder = self.settings['paths']['service']
            data_str = ''
            for name in self.timestamp_logs:
                data_str += name + '\t' + self.timestamp_logs[name] + '\n' 
            if data_str != '':
                filename = os.path.join(folder, self.service + '_timestamp.txt')
                with open(filename, 'a') as f:
                    f.write(data_str)
            self.timestamp_logs= {}
            
    def create_log_time(self, name):
        """Create log timestamp for the timstamps logging"""
        if self.timestamp_logging:
            self.timestamp_logs[name] = self.create_timestamp(True)
     
    def create_timestamp(self, milliseconds=False):
        """
        Return the current time as a formated timestamp
        Default format: yyyy-mm-ddThh:mm:ss
        """
        if milliseconds:
            return dt.utcnow().strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3]
        else:
            return dt.utcnow().strftime('%Y-%m-%dT%H:%M:%S')

class PackageHandler(ServiceParameter):
    def __init__(self, service=None, queues=None, settings=None):
        if queues is not None:
            self.send_q = queues[0]
            if isinstance(queues[1], dict):
                self.recv_qs = queues[1]
            else:
                self.recv_q = queues[1]
        
            self.logpaths = settings['paths']
            super().__init__(service=service, settings=settings)
            

    # Package Functions
    def check_package(self, package):
        """Check the current package if all criterias are fullfilled"""

        # Check if header includes all keys
        for key_header in ['FROM', 'TO', 'TIMESTAMP', 'CMDTYPE', 'CMDREFERENCE']:
            if isinstance(package['HEADER'][key_header], str) != True:
                raise Exception('INPUT IS NOT STRING: HEADER - ' + key_header)
                break
            if key_header == 'CMDTYPE':
                if package['HEADER'][key_header] not in CMDTYPES:
                    raise Exception ("Only accept the following cmdtypes: "
                                     + ", ".join(CMDTYPES))
                if package['HEADER'][key_header] in ADMIN_CMDS:
                    if package['HEADER']['FROM'] not in ADMIN_SERVICES:
                        raise Exception ("Only Admin Services are allowed "
                                         + "to Start, Stop or to Set: "
                                         + ", ".join(ADMIN_SERVICES))
                elif package['HEADER'][key_header] not in USER_CMDS:
                    raise Exception ("Only accept the following cmdtypes: "
                                     + ", ".join(USER_CMDS))
        # Check if body includes all keys
        for key_body in ['DATA', 'ERROR']:
            if isinstance(package['BODY'][key_body], dict) != True:
                raise Exception('INPUT IS NOT DICTIONARY: BODY - ' + key_body)
                break
  
    
    
    def package_spam_filter(self, package, sender_log, max_pack=10):
        """
        Check if to many packages per second are send by the service
        return senderlog of the last 'max_pack' packages
        and a string naming a service to kill in case of to many messages
        """
        sender = package['HEADER']['FROM']
        sender_log[sender].append(package['HEADER']['TIMESTAMP'])
        
        kill_sender = ''
        if len(sender_log[sender]) >= max_pack:
            timestamp_last = dt.fromisoformat(sender_log[sender][-1])
            timestamp_first = dt.fromisoformat(sender_log[sender][0])
            delta = (timestamp_last - timestamp_first)
            if delta.seconds < 1 and sender not in ADMIN_SERVICES:
                # Set sender service to be killed
                kill_sender = sender
            sender_log[sender].pop(0)
        return sender_log, kill_sender
    
    
    def create_package(self, sender, receiver=RECEIVER, cmdtype='RESPONSE',
                       cmdref='', data={}, error={}):
        """
        Creates the package used in the temperature software
        to communicate between the different services
        input: sender, cmdtype and cmdreference and
        receiver as string, data and error as dictionary
        """
        now = dt.utcnow().strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3]
        if isinstance(data, dict) and isinstance(error, dict):
            if error != {}:
                if 'TYPE' not in error or 'MESSAGE' not in error:
                    raise Exception ("Wrong error input ."
                                     + "Please add a 'TYPE' and 'MESSAGE' "
                                     + "key to your dictionary")
            if cmdtype in CMDTYPES:
                if cmdtype in ADMIN_CMDS:
                    if sender not in ADMIN_SERVICES:
                        raise Exception ("Only Admin Services are allowed "
                                         + "to Start, Stop or to Set: "
                                         + ", ".join(ADMIN_SERVICES))
                if cmdtype == 'EVENT':
                    receiver = 'TempSrv'
                package = {'HEADER':{'FROM':sender,
                                     'TO':receiver,
                                     'TIMESTAMP':now,
                                     'CMDTYPE':cmdtype,
                                     'CMDREFERENCE':cmdref},
                           'BODY':{'DATA':data, 'ERROR':error}}
                return package
            else:
                raise Exception ("Only accept the following cmdtypes: "
                                 + ", ".join(USER_CMDS))
        else:
            raise Exception ("Only except 'data' and 'error' as dictionary, "
                             + "your input: data = "
                             + str(type(data))
                             + ", error = "
                             + str(type(error)))
    
    
    def create_event_log_package(self, driver, exception='', event_message='',
                                 device_id='----', tag='INFO', init=False):
        """Create logging message with 'tag' and send it to TempSrv"""
        if exception != '':
            if isinstance(exception, Exception):
                if isinstance(exception, Warning):
                    tag = 'WARNING'
                else:
                    tag = 'ERROR'
                err_text = lof.create_error_trace()
                lof.default_logger(self.logpaths['error'], driver + '-error',
                                  err_text, module=device_id, tag=tag)
                if event_message == '':
                    event_message = str(exception)
                else:
                    event_message += ', see ErrorLog of ' + driver
                # err = {'TYPE':type(exception).__name__,
                #        'MESSAGE':str(exception)}
            else:
                # Only needed for debugging
                raise Exception('"exception" not of type EXCEPTION')
        # else:
        #     err = {}
        message = lof.default_logger(self.logpaths['event'], driver + '-event',
                                     event_message, module=device_id, tag=tag)
        timestamp = message.split('\t')[0]
        self.send_q.put(self.create_package(driver, 
                                            receiver=RECEIVER,
                                            cmdtype='EVENT', 
                                            data={'TIMESTAMP':timestamp,
                                                  'SERVICE':driver,
                                                  'ID':device_id,
                                                  'TAG':tag,
                                                  'MESSAGE':event_message,
                                                  'INIT':init}))
        

class Cat(PackageHandler):
    def __init__(self, queues, settings, update):
        super().__init__(queues=queues, settings=settings)
        self.service = 'CAT'
        
        self.update_allowed = update['allowed']
        self.current_versions = update['versions']
        
        self.service_states = {}
        for service in self.recv_qs:
            self.service_states[service] = True
        
    def _update_thread(self):
        """Check for updates every midnight (UTC)"""
        try:
            new_versions = vru.check_for_updates(self.current_versions)
        except Exception:
            pass
        else:
            if new_versions != {}:
                versions = {'Current_Versions':self.current_versions['Versions'],
                            'New_Versions':new_versions}
                package = self.create_package(self.service, 
                                              data=versions, 
                                              cmdtype='SET')
                self.send_q.put(package)
     
    def _send_to_receiver(self, msg, ignore_list):
        """Send the checked data to the recveiver"""
        receiver = msg['HEADER']['TO']
        origin = msg['HEADER']['FROM']
        
        # Check if state of service / receiver is active
        if (receiver == 'CAT' and origin == 'TempSrv'
            and msg['HEADER']['CMDTYPE'] == 'SET'):
            state = msg['BODY']['DATA']['STATE']
            service = msg['BODY']['DATA']['SERVICE']
            self.service_states[service] = state
            
        if receiver in self.recv_qs:
            if self.service_states[receiver]:
                # Check if queue of receiver is full
                try:
                    self.recv_qs[receiver].put(msg)
                except mp.queues.Full:
                    self.recv_qs[receiver].get()
                    self.recv_qs[receiver].put(msg)
        else:
            # Create an error message if receiver does not exist and
            # add the receiver to the ignore_list
            if receiver not in ignore_list:
                e_m = ('The service ' + str(receiver)
                       + ' does not exist and will be ignored.')
                ignore_list.append(receiver)
                self.create_event_log_package(self.service,
                                             event_message=e_m)
       
    def _kill_sender_message(self, sender):
        """Send message to kill the sender"""
        knife = self.create_package(self.service, 
                                    cmdtype='SET', 
                                    data={'Service':sender})
        self.send_q.put(knife)
        
    # CAT Process
    def run(self):
        # Init Variables
        ignore_list = []
        sender_log = {}
        for service in self.recv_qs:
            sender_log[service] = []
        sender_log['WebSrv'] = []  
        sender_log[self.service] = []
        
        day = int(dt.utcnow().strftime('%d')) - 1
        
        while 1:
            try:
                msg = self.send_q.get(timeout=0.2)
            except mp.queues.Empty:
                if self.update_allowed:
                    new_day = dt.utcnow().strftime('%d')
                    if new_day != day:
                        day = new_day
                        th.Thread(target=self._update_thread).start()
            except Exception as err:
                self.create_event_log_package(self.service, exception=err) 
            else:
                try:
                    # Check package before giving it to receiver
                    self.check_package(msg)
                except Exception as err:
                    self.create_event_log_package(self.service, exception=err)
                else:
                    sender_log, kill_sender = self.package_spam_filter(msg, 
                                                                      sender_log)
                    if kill_sender != '':
                        self._kill_sender_message(kill_sender)

                    self._send_to_receiver(msg, ignore_list)
                        

"""----------------------------- SUBSCRIPTIONS -----------------------------""" 


class Portier(th.Thread):
    """
    The Doorman (Portier) detects changes of message by listening to the
    subscribed channel, opens 'the door' as a message appears, yield it
    and closes the door once trough.
    channel: the cherrypy bus channel to listen to.
    """
    def __init__(self, channel,group=None, target=None, name=None,
                 args=(), kwargs=None, *, daemon=None):
        super().__init__(group=group, target=target, name=name,
                         daemon=daemon)
        self.channel = channel
        self.name = 'Portier-'+self.name
        cherrypy.engine.subscribe(channel, self._msgs)
        
    @property
    def message(self):
        """contains the last message published to the bus channel"""
        return self._message

    @message.setter
    def message(self, msg):
        """Sets the latest message and triggers the 'door' to open"""        
        self._message = msg

    def _msgs(self, message):
        """Receives the messages from the bus"""
        self.message = message

    def unsubscribe(self):
        """Unsubscribe from the message stream"""     
        cherrypy.engine.unsubscribe(self.channel, self._msgs)   
