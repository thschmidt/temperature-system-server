
###############################################################################
# IMPORTS
###############################################################################  

# Some tkinter
import tkinter as tk

import sys
import os

# Subdirectory
sys.path.insert(1, 'setup')
sys.path.insert(1, 'server/messengers')

sys.path.insert(1, os.path.join(os.path.dirname(os.getcwd()), 'modules'))

import multiprocessing as mp
import threading as th
import time

import setup
import server

import logging_functions as lof

from install_config_module import ModuleConfigPage, ModuleInstallPage
from install_config_messenger import MessengerConfigPage
from install_config_server import ServerConfigPage, ServerInstallPage

from install_config_subclasses import Defaults

from module_basics import ModuleFunctions
        
###############################################################################
# START SCREEN CLASS
###############################################################################  

class StartScreen(tk.Frame, Defaults):
    def __init__(self, parent, controller, home, queues):
        tk.Frame.__init__(self, parent)
        Defaults.__init__(self, parent, controller, home)
        
        # Check if first installation
        install_state = 'normal'
        if self.check_first_installation():
            
            install_state = 'disabled'

        srv_label = tk.Label(self, text="Setup Server", font=("Helvetica", 16))
        srv_label.pack(pady=10, padx=10)
        
        self.btns = {}
        
        self.btns['srv_init'] = tk.Button(self,
                             text="Install Server",
                             command=lambda: controller.show_frame(ServerInstallPage))
        self.btns['srv_init'].pack(pady=10)
    
        self.btns['srv_cfg'] = tk.Button(self,
                             text="Configure Server",
                             command=lambda: controller.show_frame(ServerConfigPage),
                             state=install_state)
        self.btns['srv_cfg'].pack(pady=10)
        
        mod_label = tk.Label(self, text="Setup Module", font=("Helvetica", 16))
        mod_label.pack(pady=10, padx=10)
        
        self.btns['mod_init'] = tk.Button(self,
                             text="Install Module",
                             command=lambda: controller.show_frame(ModuleInstallPage),
                             state=install_state)
        self.btns['mod_init'].pack(pady=10)
        
        self.btns['mod_cfg'] = tk.Button(self,
                             text="Configure Module",
                             command=lambda: controller.show_frame(ModuleConfigPage),
                             state=install_state)
        self.btns['mod_cfg'].pack(pady=10)
        
        ext_label = tk.Label(self, text="Setup Messenger", font=("Helvetica", 16))
        ext_label.pack(pady=10, padx=10)
 
    
        msg_btn = tk.Button(self,
                            text="Install Messenger",
                            state='disabled')
        msg_btn.pack(pady=10)
    
        self.btns['msg_cfg'] = tk.Button(self,
                             text="Configure Messenger",
                             command=lambda: controller.show_frame(MessengerConfigPage),
                             state=install_state)
        self.btns['msg_cfg'].pack(pady=10)
        
        exit_button = tk.Button(self, text="Exit",
                                command=controller.quit)
        exit_button.pack(pady=10, side='bottom')
        
    
    def reset_page(self):
        # Implement any specific reset logic
        self.controller.geometry("300x500")
        
        if not self.check_first_installation():
            for key in self.btns:
                self.btns[key].config(state='normal')

 
###############################################################################
# MAIN CLASS
###############################################################################     

class App(tk.Tk):
    def __init__(self, send_q, recv_q):
        tk.Tk.__init__(self)
        
        self.title("Setup Temperature System")

        container = tk.Frame(self)
        container.pack(side="top", fill="both", expand=True)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)
        
        self.frames = {}
        for F in (StartScreen, ServerInstallPage, ServerConfigPage,
                  ModuleInstallPage, ModuleConfigPage, MessengerConfigPage):
            page_name = F.__name__
            frame = F(parent=container, controller=self,
                      home=StartScreen, queues=[send_q, recv_q])
            self.frames[page_name] = frame
            frame.grid(row=0, column=0, sticky="nsew")
        
        self.show_frame(StartScreen)
        
    def show_frame(self, page_class):
        self.geometry("500x350")
        frame = self.frames[page_class.__name__]
        
        frame.tkraise()
        frame.reset_page()
 
 
###############################################################################
# CONNECTION THREAD / PROCESS
###############################################################################    

class ModuleThread():
    def __init__(self):
        self.campaign_sensors = {}
        self.general_body = {}
        self.device_id = ''
        self.mob = ModuleFunctions(thread_instance=self)
        
    def update_body(self, general_body):
        self.general_body = general_body
        return self.mob
        

def connection_check(service, connection, general_body, return_dict):
    """Check the connectiopn to the device and get inital device parameters"""
    import init_modules as imo
    mbt = ModuleThread()
    mob = mbt.update_body(general_body)
    try:
        out = imo.start_device(service, connection, mob)
    except Exception as err:
        return_dict['ERROR'] = str(err)
    else:
        return_dict['DATA'] = out
        
    
def connection_thread(send_q, recv_q):
    """Thread to handle connection checks"""
    proc = ''
    start_time = 0 # Need for timeout
    return_dict = {}
    
    while 1:
        try:
            msg = send_q.get(timeout=1)
        except mp.queues.Empty:
            if start_time != 0:
                now = time.time()
                if proc.is_alive():
                    # Terminate process in case of no response
                    if now - start_time > 5:
                        proc.terminate()
                        recv_q.put({'ERROR':'No connection to the device '
                                    + 'could be established'})
                        start_time = 0
                else:
                    proc.join()
                    start_time = 0
                    # Return info to config module calss
                    recv_q.put(return_dict)
        else:
            if msg == 'STOP':
                if proc != '':
                    # Kill process if still alive
                    if proc.is_alive():
                        proc.terminate()
                        time.sleep(0.1)
                break
            else:
                # Define parameter to be updated in this thread (from process)
                manager = mp.Manager()
                return_dict = manager.dict()
                
                #
                service = msg['service']
                connection = msg['connection']
                general_body = msg['general']
                
                # Start process
                proc= mp.Process(target=connection_check, args=(service,
                                                                connection,
                                                                general_body,
                                                                return_dict))
                proc.start()
                start_time = time.time()


###############################################################################
# RUN
###############################################################################
  
if __name__ == "__main__":
    send_q = mp.Queue()
    recv_q = mp.Queue()
    
    # Thread to check device connections
    con_thread = th.Thread(target=connection_thread, args=(send_q, recv_q))
    con_thread.start()
    
    try:
        app = App(send_q, recv_q)
    except Exception:
        print(lof.create_error_trace())
    else:
        app.mainloop()
    
    # Send stop signal to thread
    send_q.put('STOP')

    
